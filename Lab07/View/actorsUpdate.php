<!DOCTYPE html>
<html>
    <head>
        <title>Actor Update</title>
    </head>

    <body>

    <section>

        <p>
            <a href="<?php echo $_SERVER['PHP_SELF'] ?>">Back to form</a>
        </p>

        <?php
        if(!empty($result))
        {

        ?>
            <h2><?php echo $result; ?></h2>
        <?php
        } // End If Statement


        if ($actorObj->getID() != NULL)
        {

        ?>

        <form id="updateActor" name="updateActor" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">

            <p>
                <label>Actor ID: <input type="text" name="actorID" id="actorID" value = "<?PHP echo $actorObj->getID(); ?>" readonly/> </label>
            </p>

            <p>
                <label>First Name: <input type="text" name="firstName" id="firstName" value = "<?PHP echo $actorObj->getFirstName(); ?>"/> </label>
            </p>
            <p>
                <label>Last Name:<input type="text" name="lastName" id="lastName" value = "<?PHP echo $actorObj->getLastName(); ?>"/></label>
            </p>
            <p>
                <input type="submit" name="updateActor" id="updateActor" value="Update Actor" />
            </p>
        </form>

        <?php

        }else
        {

        ?>

            <p>Unable to find actor.</p>

        <?php
        } // End If Statement to see if the actorID is null or not.

        ?>

    </section>
    </body>
</html>