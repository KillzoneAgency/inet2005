    <!DOCTYPE html>
    <html>
    <head>
        <title>Actors Main</title>
    </head>
    <body>

    <?php
        if(!empty($result))
        {


    ?>
        <h2><?php echo $result; ?></h2>
    <?php
        } // End If Statement
    ?>

    <?php

        if ($arrayOfActors != null)
        {

    ?>

    <table border=1>

        <thead>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Last Update</th>
        </thead>

        <?php

                foreach($arrayOfActors as $actor):

        ?>

            <tr>
                <td><?php echo $actor->getID(); ?></td>
                <td><?php echo $actor->getFirstName(); ?></td>
                <td><?php echo $actor->getLastName(); ?></td>
                <td><?php echo $actor->getLastUpdate(); ?></td>
            </tr>

        <?php

                endforeach;

        ?>

    </table>

    <br />

    <?php

        }else
        {

    ?>

        <p>Sorry, there were no actors to display.</p>

    <?php
        } // Ending Inner If Statement to determine if anny rows were returned
    ?>

    <section>

        <h2>Insert Actor</h2>

        <form id="insertActor" name="insertActor" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
            <p>
                <label>First Name: <input type="text" name="firstName" id="firstName" /> </label>
            </p>
            <p>
                <label>Last Name:<input type="text" name="lastName" id="lastName" /></label>
            </p>
            <p>
                <input type="submit" name="insertActor" id="insertActor" value="Insert" />
            </p>
        </form>

        <h2>Update Actor</h2>

        <form id="updateActor" name="updateActor" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
            <p>
                <label>ID to Update: <input type="text" name="idToMod" id="idToMod" /> </label>
            </p>
            <p>
                <input type="submit" name="getActorToUpdate" id="getActorToUpdate" value="Update" />
            </p>
        </form>

        <h2>Delete Actor</h2>

        <form id="deleteActor" name="deleteActor" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
            <p>
                <label>ID to Delete: <input type="text" name="idToDel" id="idToDel" /> </label>
            </p>
            <p>
                <input type="submit" name="deleteActor" id="deleteActor" value="Delete" />
            </p>
        </form>

        <h2>Search For Actor</h2>

        <form id="searchActors" name="searchActors" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
            <p>
                <label>Name to Search: <input type="text" name="searchQuery" id="searchQuery" /> </label>
            </p>
            <p>
                <input type="submit" name="searchActors" id="searchActors" value="Search" />
            </p>
        </form>

    </section>

    </body>
    </html>