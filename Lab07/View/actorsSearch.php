<!DOCTYPE html>

<html>

    <head>
        <title>Search Results</title>
    </head>

    <body>

        <section>


            <p>
                <a href="<?php echo $_SERVER['PHP_SELF'] ?>">Back to form</a>
            </p>

            <?php

                if ($arrayOfActors != null)
                {

            ?>

            <table border=1>

                <thead>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Last Update</th>
                </thead>

                <?php

                foreach($arrayOfActors as $actor):

                ?>

                    <tr>
                        <td><?php echo $actor->getID(); ?></td>
                        <td><?php echo $actor->getFirstName(); ?></td>
                        <td><?php echo $actor->getLastName(); ?></td>
                        <td><?php echo $actor->getLastUpdate(); ?></td>
                    </tr>

                <?php

                endforeach;

                ?>

            </table>

            <?php

                 }else
                 {

            ?>

                <p>Sorry, your query returned no results.</p>

            <?php
                 } // Ending Inner If Statement to determine if anny rows were returned

            ?>

        </section>

    </body>

</html>