<?php

require_once('../Model/ActorModel.php');

class ActorController
{
    public $model;

    public function __construct()
    {
        $this->model = new ActorModel();
    } // End Constructor Method

    public function displayActor($actorID)
    {

        $actorObj = $this->model->selectSingle($actorID);

        include '../View/actorsUpdate.php';

    } // End displayActor Method

    public function displayActors($numOfActors)
    {

        $arrayOfActors = $this->model->selectMultiple($numOfActors);

        include '../View/actorsMain.php';

    } // End displayActors Method

    public function addActor($firstName, $lastName)
    {

        $result = $this->model->insert($firstName, $lastName);

        $arrayOfActors = $this->model->selectMultiple(10);

        include '../View/actorsMain.php';

    } // End addActor Method

    public function updateActor($actorID, $fName, $lName)
    {
        $result = "";

        $currentActor = $this->model->selectSingle($actorID);

        $currentActor->setFirstName($fName);
        $currentActor->setLastName($lName);

        $result = $this->model->update($currentActor);

        $arrayOfActors = $this->model->selectMultiple(10);

        include '../View/actorsMain.php';

    } // End updateActor Method

    public function deleteActor($actorID)
    {

        $result = $this->model->delete($actorID);

        $arrayOfActors = $this->model->selectMultiple(10);

        include '../View/actorsMain.php';

    } // End deleteActor Method

    public function searchActors($query)
    {

        $arrayOfActors = $this->model->search($query);

        include '../View/actorsSearch.php';

    } // End searchActors Method

} // End ActorController Class

?>