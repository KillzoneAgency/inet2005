<?php
interface iBusinessObject
{
    public static function selectMultiple($count);
    public static function selectSingle($id);
    public function insert($firstName, $lastName);
    public static function update($actorObject);
    public static function delete($id);
    public static function search($query);
}
?>
