<?php

class Actor
{
    private $actorID;
    private $firstName;
    private $lastName;
    private $lastUpdate;

    public function __construct($id, $firstN, $lastN, $lastUpdate)
    {
        $this->actorID = $id;
        $this->firstName = $firstN;
        $this->lastName = $lastN;
        $this->lastUpdate = $lastUpdate;
    } // End Constructor Method

    public function getID()
    {
        return ($this->actorID);
    } // End getID Method

    public function setFirstName($name)
    {
        $this->firstName = $name;
    } // End setFirstName Method

    public function getFirstName()
    {
        return ($this->firstName);
    } // End getFirstName Method

    public function setLastName($name)
    {
        $this->lastName = $name;
    } // End setLastName Method

    public function getLastName()
    {
        return ($this->lastName);
    } // End getLastName Method

    public function getLastUpdate()
    {
        return ($this->lastUpdate);
    } // End getLastUpdate Method

} // End Actor Class

?>