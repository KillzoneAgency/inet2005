<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'DataAccess.php';
class DataAccessSQLServer extends DataAccess
{
    private $dbCon;
    private $result;

    // aDataAccess methods
    function connectToDB()
    {
           $this->dbCon;

           /* Specify the server and connection string attributes. */
           $serverName = "(local)\SQLEXPRESS";
           $uid = "sa";
           $pwd = "sql";
           $connectionInfo = array( "UID"=>$uid,
                                    "PWD"=>$pwd,
                                    "Database"=>"AdventureWorks2012");

           /* Connect using SQL Server Authentication. */
           $this->dbCon = sqlsrv_connect($serverName, $connectionInfo);
           if($this->dbCon === false )
           {
                die("Could not insert records into the database: " . print_r( sqlsrv_errors(), true));
           }


    }

    function closeDB()
    {
         sqlsrv_close($this->dbCon);
    }

    public function insertActor($firstName,$lastName)
    {

        $insertStatement = "INSERT INTO Actors (first_name, last_name) VALUES ('";
        $insertStatement .= $firstName;
        $insertStatement .= "','";
        $insertStatement .= $lastName;
        $insertStatement .= "');";

        $this->result = sqlsrv_query($this->dbCon,$insertStatement);

        if(!$this->result)
        {
            die("Could not insert records into the database: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_rows_affected($this->result);

    } // End insertActor Method

    public function selectActor($id)
    {

        $selectStatement = "SELECT * FROM Actors WHERE actor_id = '";
        $selectStatement .= $id;
        $selectStatement .= "';";

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectActors Method

    public function selectActors($count)
    {

        $selectStatement = "SELECT top ";
        $selectStatement .= $count;
        $selectStatement .= " * FROM Actors ORDER BY actor_id DESC;";

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectActors Method

    public function updateActor($id, $fName, $lName)
    {

        $updateStatement = "UPDATE Actors SET first_name ='";
        $updateStatement .= $fName;
        $updateStatement .= "', last_name = '";
        $updateStatement .= $lName;
        $updateStatement .= "' WHERE actor_id ='";
        $updateStatement .= $id;
        $updateStatement .= "';";

        $this->result = sqlsrv_query($this->dbCon,$updateStatement);

        if(!$this->result)
        {
            die("Could not update records in the database: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_rows_affected($this->result);

    } // End updateActor Method

    public function deleteActor($id)
    {

        $deleteStatement = "DELETE FROM Actors WHERE actor_id = '";
        $deleteStatement .= $id;
        $deleteStatement .= "';";

        $this->result = sqlsrv_query($this->dbCon,$deleteStatement);

        if(!$this->result)
        {
            die("Could not delete records from the database: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_rows_affected($this->result);

    } // End deleteActor Method

    public function searchActors($query)
    {

        $selectStatement = "SELECT * FROM Actors WHERE ";
        $selectStatement .= "first_name LIKE '%$query%'";
        $selectStatement .= " OR last_name LIKE '%$query%';";

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the database: " . print_r( sqlsrv_errors(), true));
        }

    } // End searchActors Method

    public function retrieveActors()
    {
        if(!$this->result)
        {
            die("No records to display: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_fetch_array($this->result, SQLSRV_FETCH_ASSOC);
    } // End retrieveActors Method

    public function retrieveActorID($row)
    {
        return $row['actor_id'];
    } // End retrieveActorID Method

    public function retrieveActorFirstName($row)
    {
        return $row['first_name'];
    } // End retrieveActorFirstName Method

    public function retrieveActorLastName($row)
    {
        return $row['last_name'];
    } // End retrieveActorLastName Method

    public function retrieveActorLastUpdate($row)
    {
        return $row['last_update'];
    } // End retrieveActorLastUpdate Method
}

?>
