<?php

require_once 'DataAccess.php';
class DataAccessMySQL extends DataAccess
{

    private $dbCon;
    private $result;

    // aDataAccess methods
    public function connectToDB()
    {

        // Creating Connection
        $this->dbCon = mysqli_connect("localhost","root","","sakila");

        // Checking Connection for Errors
        if (!$this->dbCon)
        {
            die('Could not connect to the Sakila Database: ' . $this->dbCon->connect_errno);
        }

    }

    public function closeDB()
    {
        $this->dbCon->close();
    } // End closeDB Method

    public function insertActor($firstName,$lastName)
    {

        $insertStatement = "INSERT INTO actor (first_name, last_name) VALUES ('";
        $insertStatement .= $firstName;
        $insertStatement .= "','";
        $insertStatement .= $lastName;
        $insertStatement .= "');";

        $this->result = mysqli_query($this->dbCon,$insertStatement);

        if(!$this->result)
        {
            die("Could not insert records into the Sakila Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End insertActor Method

    public function selectActor($id)
    {

        $selectStatement = "SELECT * FROM actor WHERE actor_id = '";
        $selectStatement .= $id;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Sakila Database: " . $this->dbCon->error);
        }

    } // End selectActors Method

    public function selectActors($count)
    {

        $selectStatement = "SELECT * FROM actor ORDER BY actor_id DESC LIMIT ";
        $selectStatement .= $count;
        $selectStatement .= ";";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Sakila Database: " . $this->dbCon->error);
        }

    } // End selectActors Method

    public function updateActor($id, $fName, $lName)
    {

        $updateStatement = "UPDATE actor SET first_name ='";
        $updateStatement .= $fName;
        $updateStatement .= "', last_name = '";
        $updateStatement .= $lName;
        $updateStatement .= "' WHERE actor_id ='";
        $updateStatement .= $id;
        $updateStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$updateStatement);

        if(!$this->result)
        {
            die("Could not update records in the Sakila Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End updateActor Method

    public function deleteActor($id)
    {

        $deleteStatement = "DELETE FROM actor WHERE actor_id = '";
        $deleteStatement .= $id;
        $deleteStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$deleteStatement);

        if(!$this->result)
        {
            die("Could not delete records from the Sakila Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End deleteActor Method

    public function searchActors($query)
    {

        $selectStatement = "SELECT * FROM actor WHERE ";
        $selectStatement .= "first_name LIKE '%$query%'";
        $selectStatement .= " OR last_name LIKE '%$query%';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Sakila Database: " . $this->dbCon->error);
        }

    } // End searchActors Method

    public function retrieveActors()
    {
        if(!$this->result)
        {
            die("No records to display: " . $this->dbCon->error);
        }

        return mysqli_fetch_assoc($this->result);
    } // End retrieveActors Method

    public function retrieveActorID($row)
    {
        return $row['actor_id'];
    } // End retrieveActorID Method

    public function retrieveActorFirstName($row)
    {
        return $row['first_name'];
    } // End retrieveActorFirstName Method

    public function retrieveActorLastName($row)
    {
        return $row['last_name'];
    } // End retrieveActorLastName Method

    public function retrieveActorLastUpdate($row)
    {
        return $row['last_update'];
    } // End retrieveActorLastUpdate Method

} // End DataAccessMySQL Class

?>