<?php

require_once 'MySQLActorDataModel.php';
//require_once 'SQLServerActorDataModel.php';

abstract class DataAccess
{
    private static $dataAccess;

    public static function getInstance()
    {
        // If there's no instance of DataAccess then create one.
        if(self::$dataAccess == null)
        {
            self::$dataAccess = new DataAccessMySQL();
            //self::$dataAccess = new DataAccessSQLServer();
        }
        return self::$dataAccess;
    }

    public abstract function connectToDB();

    public abstract function closeDB();

    public abstract function insertActor($firstName,$lastName);

    public abstract function updateActor($id, $fName, $lName);

    public abstract function deleteActor($id);

    public abstract function selectActor($id);

    public abstract function selectActors($count);

    public abstract function searchActors($query);

    public abstract function retrieveActors();

    public abstract function retrieveActorID($row);

    public abstract function retrieveActorFirstName($row);

    public abstract function retrieveActorLastName($row);

    public abstract function retrieveActorLastUpdate($row);

} // End DataAccess Class
?>