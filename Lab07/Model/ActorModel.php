<?php

require_once 'iBusinessObject.php';
require_once 'Data/DataAccess.php';
require_once 'Actor.php';

class ActorModel implements iBusinessObject
{

    public static function selectSingle($id)
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectActor($id);

        $row = $dataAccess->retrieveActors();
        $currentActor = new Actor($dataAccess->retrieveActorID($row),
                                  $dataAccess->retrieveActorFirstName($row),
                                  $dataAccess->retrieveActorLastName($row),
                                  $dataAccess->retrieveActorLastUpdate($row));

        $dataAccess->closeDB();

        return $currentActor;
    } // End selectSingle Method

    public static function selectMultiple($count)
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectActors($count);

        while($row = $dataAccess->retrieveActors())
        {
            $currentActor = new Actor($dataAccess->retrieveActorID($row),
                                      $dataAccess->retrieveActorFirstName($row),
                                      $dataAccess->retrieveActorLastName($row),
                                      $dataAccess->retrieveActorLastUpdate($row));

            $arrayOfActorObjects[] = $currentActor;
        } // End While Loop

        $dataAccess->closeDB();

        return $arrayOfActorObjects;
    } // End selectMultiple Method

    public function insert($firstName, $lastName)
    {

        $dataAccess = DataAccess::getInstance();
        $dataAccess->connectToDB();
        $rowsAffected = $dataAccess->insertActor($firstName, $lastName);

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "Actor successfully inserted into database.";
        }else
        {
            return "Unable to insert actor into database.";
        } // End If Statement

    } // End insert Method

    public static function update($actorObject)
    {

        $dataAccess = DataAccess::getInstance();
        $dataAccess->connectToDB();

        $rowsAffected = $dataAccess->updateActor($actorObject->getID(), $actorObject->getFirstName(), $actorObject->getLastName());

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "Actor successfully updated.";
        }else
        {
            return "Unable to update actor.";
        } // End If Statement

    } // End update Method

    public static function delete($id)
    {

        $dataAccess = DataAccess::getInstance();
        $dataAccess->connectToDB();
        $rowsAffected = $dataAccess->deleteActor($id);

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "Actor successfully deleted from database.";
        }else
        {
            return "Unable to delete actor from database.";
        } // End If Statement

    } // End delete Method

    public static function search($query)
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->searchActors($query);

        $arrayOfActorObjects = NULL;

        while($row = $dataAccess->retrieveActors())
        {
            $currentActor = new Actor($dataAccess->retrieveActorID($row),
                                      $dataAccess->retrieveActorFirstName($row),
                                      $dataAccess->retrieveActorLastName($row),
                                      $dataAccess->retrieveActorLastUpdate($row));

            $arrayOfActorObjects[] = $currentActor;
        }

        $dataAccess->closeDB();

        return $arrayOfActorObjects;
    } // End search Method

} // End Actor Class

?>