<?php

require_once '../Controller/ActorController.php';

$actorController = new ActorController();


if(isset($_POST['insertActor']))
{

    if (!empty($_POST['firstName']) && !empty($_POST['lastName']))
    {
        $result = $actorController->addActor($_POST['firstName'], $_POST['lastName']);
    }else
    {
        $result = "Please ensure both fields are filled out when adding a new actor.";
        $actorController->displayActors(10);
    } // End If Statement

}elseif (isset($_POST['getActorToUpdate']))
{

    if (!empty($_POST['idToMod']))
    {
        $actorController->displayActor($_POST['idToMod']);
    }else
    {
        $result = "Please ensure you've entered an actor ID for updating.";
        $actorController->displayActors(10);
    } // End If Statement

}elseif (isset($_POST['updateActor']))
{

    if (!empty($_POST['firstName']) && !empty($_POST['lastName']))
    {
        $actorController->updateActor($_POST['actorID'], $_POST['firstName'], $_POST['lastName']);
    }else
    {
        $result = "Please ensure both fields are filled out when updating an actor.";
        $actorController->displayActor($_POST['actorID']);
    } // End If Statement

}elseif (isset($_POST['deleteActor']))
{

    if (!empty($_POST['idToDel']))
    {
        $result = $actorController->deleteActor($_POST['idToDel']);
    }else
    {
        $result = "Please ensure you've entered an actor ID for deletion.";
        $actorController->displayActors(10);
    } // End If Statement

}elseif (isset($_POST['searchActors']))
{

    if (!empty($_POST['searchQuery']))
    {
        $actorController->searchActors($_POST['searchQuery']);
    }else
    {
        $result = "Please ensure you've entered a search query.";
        $actorController->displayActors(10);
    } // End If Statement

}else
{
    $actorController->displayActors(10);
} // End If Statement

?>