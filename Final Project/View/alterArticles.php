<!DOCTYPE html>
<html>

    <head>

        <title>Edit Article</title>

        <script type="text/javascript" src="/JS/jquery.js"></script>
        <script type="text/javascript" src="/JS/articleEditFormValid.js"></script>
        <script type="text/javascript" src="/JS/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                plugins: [
                    "code table"
                ]
            });
        </script>
        <style type="text/css">
            <?php echo $styleObject->getStyleSheet(); ?>
        </style>

    </head>

    <body>

    <nav>
        <ul>

            <?php

            foreach ($arrayOfPages as $page)
            {
                ?>

                <li>
                    <a href="index.php?page=<?php echo $page->getAlias(); ?>"><?php echo $page->getName(); ?></a>
                </li>

            <?php
            }
            ?>

        </ul>
    </nav>

    <?php

    if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
    {

        ?>

        <form method="post" id="logOutForm" name="logOutForm" action="<?php $_SERVER['PHP_SELF'] ?>">
            <label>Logged in as <?php echo $userObject->getUsername(); ?></label>
            <input type="submit" id="logOut" name="logOut" value="Logout">
            <input type="hidden" name="pageAlias" value="home">
        </form>

    <?php

        $hasPermission = false;

        // Role ID of 1 is Author
        // Role ID of 2 is Editor
        // Role ID of 3 is Admin
        if ($userObject->isRole(2) == true || $userObject->isRole(3) == true)
        {
            $hasPermission = true;
        } // End If Statement

        if ($hasPermission == true)
        {


    ?>
        <a href="index.php?page=admin">Admin CP</a>

    <?php

        } // End If Statement

    }else
    {

        ?>

        <form id="loginForm" name="loginForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
            <label>Hello Guest, please login to access more features.</label>

            <label for="username">Username: </label>
            <input type="text" id="username" name="username" maxlength="16" value="">
            <label for="password">Password: </label>
            <input type="password" id="password" name="password" maxlength="16" value="">
            <input type="hidden" name="pageAlias" value="home">


            <br />

            <input type="submit" id="loginBtn" name="loginBtn" class="submitBtn" value="Login">

        </form>

    <?php

    } // End If Statement to display logout form or login form.

    ?>

        <br />

    <?php
    if(!empty($result))
    {


        ?>
        <h2><?php echo $result; ?></h2>
    <?php
    } // End If Statement
    ?>

        <br />

        <section>

        <?php
            if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
            {

                $hasPermission = false;

                // Role ID of 1 is Author
                // Role ID of 2 is Editor
                // Role ID of 3 is Admin
                if ($userObject->isRole(1) == true)
                {
                    $hasPermission = true;
                } // End If Statement

                if ($hasPermission == true)
                {

        ?>

             <form method="post" id="editArticleForm" name="editArticleForm" onSubmit="return validateForm();" action="<?php $_SERVER['PHP_SELF'] ?>">

                <input type="hidden" name="articleID" value="<?php echo $articleObject->getArticleID(); ?>">

                <label for="title">Article Title: </label>
                <input type="text" id ="title" name="title" maxlength="60" value="<?php echo $articleObject->getTitle(); ?>" required>
                <span id="titleError" name="titleError"><br /></span>

                <br />

                <label for="content">Article Content: </label

                <br />

                <textarea name="content" id="content"><?php echo $articleObject->getContent(); ?></textarea>
                <span id="contentError" name="contentError"><br /></span>

                <br />

                <label for="description">Article Description: </label>
                <input type="text" id ="description" name="description" maxlength="256" value="<?php echo $articleObject->getDescription(); ?>">

                <br />

                <label for="pageList">Pages: </label>
                <select name="pageList" id="pageList" required>

                    <?php

                        foreach($arrayOfPages as $page)
                        {


                            if ($page->getPageID() == $articleObject->getPageID())
                            {

                    ?>
                        <option selected="selected" value="<?php echo $page->getPageID(); ?>"><?php echo $page->getName(); ?></option>
                    <?php

                            }else
                            {

                    ?>

                        <option value="<?php echo $page->getPageID(); ?>"><?php echo $page->getName(); ?></option>

                    <?php
                            } // End If Statement

                        } // End For Each Loop

                    ?>
                </select>

                <br />


                <label for="contentAreaList">Content Areas: </label>
                <select name="contentAreaList" id="contentAreaList" required>


                <?php

                    foreach($contentAreaArray as $contentArea)
                    {


                        if ($contentArea->getContentAreaID() == $articleObject->getContentAreaID())
                        {

                ?>
                    <option selected="selected" value="<?php echo $contentArea->getContentAreaID(); ?>"><?php echo $contentArea->getName(); ?></option>
                <?php

                        }else
                        {

                ?>

                     <option value="<?php echo $contentArea->getContentAreaID(); ?>"><?php echo $contentArea->getName(); ?></option>

                <?php

                        } // End If Statement

                    } // End For Each Loop

                ?>

                </select>

                 <br />

                <label for="dereferenceArticle">Remove all references to article? </label>
                <input type="checkbox" name="dereferenceArticle">

                <br />

                <input type="submit" id="submitArticleMods" name="submitArticleMods" value="Submit Changes">

             </form>

        <?php

                }else
                {

        ?>
            <p>You don't have permission to modify articles.</p>
        <?php

                }// End If Statement

            } // End If Statement

        ?>


            <form method="post" id="gotoArticle" name="gotoArticle" action="<?php $_SERVER['PHP_SELF'] ?>">

                <?php

                foreach($arrayOfPages as $page)
                {

                    if ($page->getPageID() == $articleObject->getPageID())
                    {

                        $pageAlias = $page->getAlias();

                    } // End If Statement

                } // End For Each Loop

                ?>

                <input type="hidden" name="pageAlias" value="<?php echo $pageAlias; ?>">

                <br />

                <input type="submit" id="gotoArticlePage" name="gotoArticlePage" value="Go to article">
            </form>

        </section>

    </body>

</html>