<!DOCTYPE html>
<html>

    <head>

        <title>Admin CP</title>

        <script type="text/javascript" src="/JS/jquery.js"></script>
        <script type="text/javascript" src="/JS/articleCreateFormValid.js"></script>

        <style type="text/css">
            <?php echo $styleObject->getStyleSheet(); ?>
        </style>

    </head>

    <body>

    <nav>
        <ul>
            <li><a href="admin.php?page=adminMain">Home</a></li>
            <li><a href="admin.php?page=displayUsers">Display Users</a></li>
            <li><a href="index.php">Back to Public Site</a></li>
        </ul>
    </nav>

    <?php

    if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
    {

        ?>

        <form method="post" id="logOutForm" name="logOutForm" action="<?php $_SERVER['PHP_SELF'] ?>">
            <label>Logged in as <?php echo $userObject->getUsername(); ?></label>
            <input type="submit" id="logOut" name="logOut" value="Logout">
            <input type="hidden" name="pageAlias" value="home">
        </form>

        <?php

        $hasPermission = false;

        // Role ID of 1 is Author
        // Role ID of 2 is Editor
        // Role ID of 3 is Admin
        if ($userObject->isRole(2) == true || $userObject->isRole(3) == true)
        {
            $hasPermission = true;
        } // End If Statement

        if ($hasPermission == true)
        {


            ?>


        <?php

        } // End If Statement

    }else
    {

        ?>

        <form id="loginForm" name="loginForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
            <label>Hello Guest, please login to access more features.</label>

            <label for="username">Username: </label>
            <input type="text" id="username" name="username" maxlength="16" value="">
            <label for="password">Password: </label>
            <input type="password" id="password" name="password" maxlength="16" value="">
            <input type="hidden" name="pageAlias" value="home">


            <br />

            <input type="submit" id="loginBtn" name="loginBtn" class="submitBtn" value="Login">

        </form>

    <?php

    } // End If Statement to display logout form or login form.

    ?>

    </body>

</html>