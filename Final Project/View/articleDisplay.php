<!DOCTYPE html>
<html>

    <head>

        <title><?php echo $pageObject->getName();?></title>
        <style type="text/css">
            <?php echo $styleObject->getStyleSheet(); ?>
        </style>

    </head>

    <body>

        <nav>
            <ul>

                <?php

                    foreach ($arrayOfPages as $page)
                    {
                ?>

                <li>
                    <a href="index.php?page=<?php echo $page->getAlias(); ?>"><?php echo $page->getName(); ?></a>
                </li>

                <?php
                    }
                ?>

            </ul>
        </nav>

        <?php

        if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
        {

            ?>

            <form method="post" id="logOutForm" name="logOutForm" action="<?php $_SERVER['PHP_SELF'] ?>">
                <label>Logged in as <?php echo $userObject->getUsername(); ?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
                <input type="hidden" name="pageAlias" value="<?php echo $pageObject->getAlias();?>">
            </form>

            <?php

            $hasPermission = false;

            // Role ID of 1 is Author
            // Role ID of 2 is Editor
            // Role ID of 3 is Admin
            if ($userObject->isRole(2) == true || $userObject->isRole(3) == true)
            {
                $hasPermission = true;
            } // End If Statement

            if ($hasPermission == true)
            {


            ?>

            <a href="index.php?page=admin">Admin CP</a>


            <?php

            } // End If Statement

                $hasPermission = false;

                // Role ID of 1 is Author
                // Role ID of 2 is Editor
                // Role ID of 3 is Admin
                if ($userObject->isRole(1) == true)
                {
                    $hasPermission = true;
                } // End If Statement

                if ($hasPermission == true)
                {

            ?>

            <form id="editArticle" name="editArticle" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

                <input type="submit" id="addArticle" name="addArticle" class="submitBtn" value="Add Article">

            </form>

        <?php

                } // End If Statement

        }else
        {



            ?>

            <form id="loginForm" name="loginForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                <label>Hello Guest, please login to access more features.</label>

                    <label for="username">Username: </label>
                    <input type="text" id="username" name="username" maxlength="16" value="">
                    <label for="password">Password: </label>
                    <input type="password" id="password" name="password" maxlength="16" value="">
                    <input type="hidden" name="pageAlias" value="<?php echo $pageObject->getAlias();?>">

                <br />

                <input type="submit" id="loginBtn" name="loginBtn" class="submitBtn" value="Login">

            </form>

        <?php

        } // End If Statement to display logout form or login link.

        ?>

        <section>

            <?php
            // obtain/receive all content areas IN ORDER ($areaArray)
            foreach ($pageObject->getArrayOfContentAreas() as $area)
            {

            ?>

            <div id="<?php echo $area->getAlias(); ?>">

            <?php

                foreach ($area->getArrayOfArticles() as $article)
                {
            ?>

                <article id="<?php echo $article->getTitle();?>">

                    <h2><?php echo $article->getTitle();?></h2>

                    <?php echo $article->getContent(); ?>

                </article>

                <?php

                    if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
                    {

                        $hasPermission = false;

                        // Role ID of 1 is Author
                        // Role ID of 2 is Editor
                        // Role ID of 3 is Admin
                        if ($userObject->isRole(1) == true)
                        {
                            $hasPermission = true;
                        } // End If Statement

                        if ($hasPermission == true)
                        {

                ?>

                <form id="editArticle" name="editArticle" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

                    <input type="hidden" name="articleID" value="<?php echo $article->getArticleID(); ?>">
                    <input type="submit" id="editArticle" name="editArticle" class="submitBtn" value="Edit Article">

                </form>

                <?php
                        } // End If Statement

                    } // End If Statement

                ?>

            <?php
                 } // End For Each Loop

            ?>

            </div>

            <?php

            } // End For Each Loop

            ?>

        </section>

    </body>

</html>