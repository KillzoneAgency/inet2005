<!DOCTYPE html>
<html>

<head>

    <title>Admin CP - Add User</title>

    <script type="text/javascript" src="/JS/jquery.js"></script>
    <script type="text/javascript" src="/JS/createUserFormValid.js"></script>
    <script type="text/javascript" src="/JS/myCode.js"></script>

    <style type="text/css">
        <?php echo $styleObject->getStyleSheet(); ?>
    </style>

</head>

<body>

<nav>
    <ul>
        <li><a href="admin.php?page=adminMain">Home</a></li>
        <li><a href="admin.php?page=displayUsers">Display Users</a></li>
        <li><a href="index.php">Back to Public Site</a></li>
    </ul>
</nav>

<?php

if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
{

    ?>

    <form method="post" id="logOutForm" name="logOutForm" action="<?php $_SERVER['PHP_SELF'] ?>">
        <label>Logged in as <?php echo $userObject->getUsername(); ?></label>
        <input type="submit" id="logOut" name="logOut" value="Logout">
        <input type="hidden" name="pageAlias" value="home">
    </form>

<?php


}else
{

    ?>

    <form id="loginForm" name="loginForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
        <label>Hello Guest, please login to access more features.</label>

        <label for="username">Username: </label>
        <input type="text" id="username" name="username" maxlength="16" value="">
        <label for="password">Password: </label>
        <input type="password" id="password" name="password" maxlength="16" value="">
        <input type="hidden" name="pageAlias" value="home">


        <br />

        <input type="submit" id="loginBtn" name="loginBtn" class="submitBtn" value="Login">

    </form>

<?php

} // End If Statement to display logout form or login form.

?>

<section>

    <?php
    if(!empty($result))
    {


        ?>
        <h2><?php echo $result; ?></h2>
    <?php
    } // End If Statement
    ?>

    <?php

    if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
    {

        $hasPermission = false;

        // Role ID of 1 is Author
        // Role ID of 2 is Editor
        // Role ID of 3 is Admin
        if ($userObject->isRole(3) == true)
        {
            $hasPermission = true;
        } // End If Statement

        if ($hasPermission == true)
        {


    ?>


            <form id="registrationForm" name="registrationForm" class="inputForm" method="post"  onSubmit="return validateForm();" action="<?php $_SERVER['PHP_SELF'] ?>">
                <label for="username">Username: </label>
                <input required type="text" id="username" name="username" maxlength="25" placeholder="Username" value="" onfocus="checkUsernameStuff();" onblur="checkUsernameStuff() ;">
                <span id="usernameError" name="usernameError"><br /></span>

                <br />

                <label for="firstName">First Name: </label>
                <input required type="text" id="firstName" name="firstName" maxlength="25" placeholder="First Name" value="" onfocus="checkFirstName();" onblur="checkFirstName() ;">
                <span id="firstNameError" name="firstNameError"><br /></span>

                <br />

                <label for="lastName">Last Name: </label>
                <input required type="text" id="lastName" name="lastName" maxlength="25" placeholder="Last Name" value="" onfocus="checkLastName();" onblur="checkLastName() ;">
                <span id="lastNameError" name="lastNameError"><br /></span>

                <br />

                <label for="password">Password: </label>
                <input required type="password" id="password" name="password" maxlength="20" placeholder="Password" value="" onkeyup="checkPassword();" onfocus="checkPassword();" onblur="checkPassword();">
                <span><br /></span>

                <br />

                <label for="confirmPassword">Confirm Password: </label>
                <input required type="password" id="confirmPassword" name="confirmPassword" maxlength="20" placeholder="Password" value="" onkeyup="checkPassword();" onfocus="checkPassword();" onblur="checkPassword();">
                <span id="passwordError" name="passwordError"><br /></span>

                <br />

                <input type="submit" id="registerBtn" name="registerBtn" value="Create User">

            </form>

            <br /> <br />


    <a href="admin.php?page=displayUsers">Back to list of users.</a>


    <?php

        }else
        {

            ?>

            <p>You don't have permission to view this page.</p>

        <?php

        }// End If Statement

    } // End If Statement

    ?>

</section>


</body>

</html>