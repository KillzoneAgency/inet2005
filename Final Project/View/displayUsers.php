<!DOCTYPE html>
<html>

<head>

    <title>Admin CP - Display Users</title>

    <link rel="stylesheet" type="text/css" href="/CSS/jquery.dataTables.css">
    <script type="text/javascript" src="/JS/jquery.js"></script>
    <script type="text/javascript" src="/JS/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/JS/dataTables.js"></script>

    <style type="text/css">
        <?php echo $styleObject->getStyleSheet(); ?>
    </style>

</head>

<body>

<nav>
    <ul>
        <li><a href="admin.php?page=adminMain">Home</a></li>
        <li><a href="admin.php?page=displayUsers">Display Users</a></li>
        <li><a href="index.php">Back to Public Site</a></li>
    </ul>
</nav>

<?php

if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
{

    ?>

    <form method="post" id="logOutForm" name="logOutForm" action="<?php $_SERVER['PHP_SELF'] ?>">
        <label>Logged in as <?php echo $userObject->getUsername(); ?></label>
        <input type="submit" id="logOut" name="logOut" value="Logout">
        <input type="hidden" name="pageAlias" value="home">
    </form>

    <?php


}else
{

    ?>

    <form id="loginForm" name="loginForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
        <label>Hello Guest, please login to access more features.</label>

        <label for="username">Username: </label>
        <input type="text" id="username" name="username" maxlength="16" value="">
        <label for="password">Password: </label>
        <input type="password" id="password" name="password" maxlength="16" value="">
        <input type="hidden" name="pageAlias" value="home">


        <br />

        <input type="submit" id="loginBtn" name="loginBtn" class="submitBtn" value="Login">

    </form>

<?php

} // End If Statement to display logout form or login form.

?>

    <section>

    <?php

        if(!empty($_SESSION['UserID']) && isset($_SESSION['UserID']))
        {

            $hasPermission = false;

            // Role ID of 1 is Author
            // Role ID of 2 is Editor
            // Role ID of 3 is Admin
            if ($userObject->isRole(3) == true)
            {
                $hasPermission = true;
            } // End If Statement

            if ($hasPermission == true)
            {


                if ($arrayOfUsers != null)
                {

                    ?>

                    <table id="grid">

                        <thead>
                        <tr>

                            <th>User ID</th>
                            <th>Username</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Created Date</th>
                            <th>Modified Date</th>
                            <th>Created By</th>
                            <th>Modified By</th>
                            <th>Edit User</th>

                        </tr>
                        </thead>

                        <?php

                        foreach($arrayOfUsers as $user):

                            ?>

                            <tr>
                                <td><?php echo htmlentities($user->getUserID()); ?></td>
                                <td><?php echo htmlentities($user->getUsername()); ?></td>
                                <td><?php echo htmlentities($user->getFirstName()); ?></td>
                                <td><?php echo htmlentities($user->getLastName()); ?></td>
                                <td><?php echo htmlentities($user->getCreatedDate()); ?></td>
                                <td><?php echo htmlentities($user->getModifiedDate()); ?></td>
                                <td><?php echo htmlentities($user->getCreatedBy()); ?></td>
                                <td><?php echo htmlentities($user->getModifiedBy()); ?></td>
                                <td>

                                    <form id="editUserForm" name="editUserForm" method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">

                                        <input type="submit" name="editUser" id="editUser" value="Edit" />
                                        <input type="hidden" name="userID" id="userID" value="<?php echo $user->getUserID(); ?>">

                                    </form>

                                </td>
                            </tr>

                        <?php

                        endforeach;

                        ?>

                    </table>

                    <br /> <br />

                    <form id="addUserForm" name="addUserForm" method="POST" action="admin.php?page=addUser">

                        <input type="submit" name="addUser" id="addUser" value="Add New User" />

                    </form>

                <?php

                }else
                {

                    ?>

                    <p>Sorry, there were no users to display.</p>

                <?php
                } // Ending Inner If Statement to determine if anny rows were returned

            }else
            {

            ?>

                <p>You don't have permission to view this page.</p>

        <?php

            }// End If Statement

        } // End If Statement

    ?>

    </section>


    </body>

</html>