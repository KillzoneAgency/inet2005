function validateForm()
{

    var isValid = false;

    if (checkTitle() == true)
    {

        if (checkName() == true)
        {

            if (checkTinyMCELength() == true)
            {

                isValid = true;

            }else
            {
                isValid = false;
            }// End If Statement

        }else
        {
            isValid = false;
        } // End If Statement

    }else
    {
        isValid = false;
    }// End If Statement

    return isValid;

} // End validateForm Method

function checkName()
{

    var isValid = false;
    var string = $("#name").val();

    if (checkLength(string) == true)
    {
        isValid = true;
        $("#nameError").html("<br />");
    }else
    {
        isValid = false;
        $("#nameError").html("Please ensure your name is between 1 and 60 characters long.<br />");
    } // End If Statement

    return isValid;

} // End checkUserName Method

function checkTitle()
{

    var isValid = false;
    var string = $("#title").val();

    if (checkLength(string) == true)
    {
        isValid = true;
        $("#titleError").html("<br />");
    }else
    {
        isValid = false;
        $("#titleError").html("Please ensure your title is between 1 and 60 characters long.<br />");
    } // End If Statement

    return isValid;

} // End checkUserName Method

function checkTinyMCELength()
{

    var isValid = false;
    var value = tinyMCE.get('content').getContent();

    if (value.length > 0)
    {
        isValid = true;
        $("#contentError").html("<br />");
    }else
    {
        isValid = false;
        $("#contentError").html("Please ensure your body is longer than 1 character.<br />");
    } // End If Statement

    return isValid;

} // End checkTinyMCELength Method

function checkLength(string)
{

    var isValid = false;

    if (string.length < 1 || string.length > 60)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameAndTitleLength Function