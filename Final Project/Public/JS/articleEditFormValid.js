function validateForm()
{

    var isValid = false;

    if (checkTitleLength() == true)
    {

        if (checkTinyMCELength() == true)
        {

            isValid = true;

        }else
        {
            isValid = false;
        }// End If Statement

    }else
    {
        isValid = false;
    }// End If Statement

    return isValid;

} // End validateForm Method

function checkTitleLength()
{

    var isValid = false;
    var string = $("#title").val();

    if (checkNameAndTitleLength(string) == true)
    {
        isValid = true;
        $("#titleError").html("<br />");
    }else
    {
        isValid = false;
        $("#titleError").html("Please ensure your title is between 1 and 60 characters long.<br />");
    } // End If Statement

    return isValid;

} // End checkUserName Method

function checkTinyMCELength()
{

    var isValid = false;
    var value = tinyMCE.get('content').getContent();

    if (value.length > 0)
    {
        isValid = true;
        $("#contentError").html("<br />");
    }else
    {
        isValid = false;
        $("#contentError").html("Please ensure your body is longer than 1 character.<br />");
    } // End If Statement

    return isValid;

} // End checkTinyMCELength Method

function checkNameAndTitleLength(string)
{

    var isValid = false;

    if (string.length < 1 || string.length > 60)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameAndTitleLength Function