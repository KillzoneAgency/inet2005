function validateForm()
{

    var isValid = false;

    if (checkUserName() == true)
    {

        if(checkFirstName() == true)
        {

            if(checkLastName() == true)
            {

                if (checkPassword() == true)
                {

                    isValid = true;

                }else
                {
                    isValid = false;
                }// End If Statement

            }else
            {
                isValid = false;
            }// End If Statement

        }else
        {
            isValid = false;
        }// End If Statement

    }else
    {
        isValid = false;
    }// End If Statement

    return isValid;

} // End validateForm Method

function checkFirstName()
{
    var isValid = false;
    var string = $("#firstName").val();

    if (checkNameLength(string) == true)
    {
        isValid = true;
        $("#firstNameError").html("<br />");
    }else
    {
        isValid = false;
        $("#firstNameError").html("Please ensure your name is at least two characters long.<br />");
    } // End If Statement

    return isValid;

} // End checkFirstName Method

function checkLastName()
{
    var isValid = false;
    var string = $("#lastName").val();

    if (checkNameLength(string) == true)
    {
        isValid = true;
        $("#lastNameError").html("<br />");
    }else
    {
        isValid = false;
        $("#lastNameError").html("Please ensure your name is at least two characters long.<br />");
    } // End If Statement

    return isValid;

} // End checkLastName Method

function checkUserName()
{

    var isValid = false;
    var isValidOne = false;
    var isValidTwo = false;
    var string = $("#username").val();

    if (checkUserNameLength(string) == true)
    {
        isValidOne = true;
        $("#usernameError").html("<br />");
    }else
    {
        isValidOne = false;
        $("#usernameError").html("Please ensure your name is at least eight characters long.");
        return isValidOne;
    } // End If Statement

    if (checkNameFormat(string) == true)
    {
        isValidTwo = true;
        $("#usernameError").html("<br />");
    }else
    {
        isValidTwo = false;
        $("#usernameError").html("Please ensure your name only contains alphanumeric characters");
        return isValidTwo;
    } // End If Statement

    if (isValidOne != true && isValidTwo != true)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkUserName Method

function checkPassword()
{

    var isValid = false;
    var passOne = $("#password").val();
    var passTwo = $("#confirmPassword").val();

    if (passOne != passTwo)
    {
        isValid = false;
        $("#passwordError").html("Please ensure both passwords match.");
    }else if (checkPasswordLength(passOne) == false)
    {
        isValid = false;
        $("#passwordError").html("Please ensure your password is 8 or more characters long.");
    }else
    {
        isValid = true;
        $("#passwordError").html("<br />");
    }// End If Statement

    return isValid;

} // End checkPassword Method

function checkNameFormat(string)
{

    var regExpression = new RegExp(/^[a-zA-Z]+$/);
    var isValid = false;

    if (regExpression.test(string) == false)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameFormat Method

function checkNameLength(string)
{

    var isValid = false;

    if (string.length < 2)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameLength Function

function checkUserNameLength(string)
{

    var isValid = false;

    if (string.length < 8)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkUserNameLength Function

function checkPasswordLength(string)
{

    var isValid = false;

    if (string.length < 8)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkPasswordLength Function