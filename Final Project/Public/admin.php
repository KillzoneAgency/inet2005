<?php

// State the Session for all pages
session_start();

require_once '../Controller/AdminController.php';

$controller = new AdminController();

if (isset($_POST['editUser']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage("editUser", $_SESSION['UserID'], $_POST['userID']);
    }else
    {
        header("location:index.php");
    } // End If Statement

}else if (isset($_POST['registerBtn']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->createUser($_POST['username'], $_POST['firstName'], $_POST['lastName'], $_POST['password'], $_SESSION['UserID']);
    }else
    {
        header("location:index.php");
    } // End If Statement

}else if (isset($_POST['modifyUser']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->updateUser($_POST['userToEditID'], $_POST['username'], $_POST['firstName'], $_POST['lastName'], $_POST['password'], $_SESSION['UserID']);
    }else
    {
        header("location:index.php");
    } // End If Statement

}else if (isset($_POST['logOut']))
{

    session_destroy();
    header("location:index.php");

}else if(isset($_GET['page']))
{

    if (!empty($_SESSION['UserID']) && $_GET['page'] == "index")
    {
        header("location:index.php");
    }else if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage($_GET['page'], $_SESSION['UserID'], null);
    }// End If Statement

}else
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage(null, $_SESSION['UserID'], null);
    }else
    {
        header("location:index.php");
    } // End If Statement

} // End If Statement


?>