<?php

// State the Session for all pages
session_start();

require_once '../Controller/PublicController.php';

$controller = new PublicController();

if (isset($_POST['editArticle']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->getArticleToEdit($_POST['articleID'], $_SESSION['UserID']);
    }else
    {
        $controller->getArticleToEdit($_POST['articleID'], null);
    } // End If Statement

}else if (isset($_POST['submitArticleMods']))
{

    $description = $_POST['description'];

    if ($_POST['description'] == null || strlen($_POST['description']) < 1)
    {
        $description = "";
    } // End If Statement

    $pageID =  $_POST['pageList'];
    $contentAreaID = $_POST['contentAreaList'];

    if (isset($_POST['dereferenceArticle']))
    {
        $hidePage = 1;
    }else
    {
        $hidePage = 0;
    }// End If Statement

    if (!empty($_SESSION['UserID']))
    {
        $controller->updateArticle($_POST['articleID'], $_SESSION['UserID'], $_POST['title'], $_POST['content'], $description, $pageID, $contentAreaID, $hidePage);
    }else
    {
        $controller->getArticleToEdit($_POST['articleID'], null);
    } // End If Statement

}else if (isset($_POST['gotoArticlePage']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage($_POST['pageAlias'], $_SESSION['UserID']);
    }else
    {
        $controller->displayPage($_POST['pageAlias'], null);
    } // End If Statement

}else if (isset($_POST['addArticle']))
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->gotoAddArticle($_SESSION['UserID']);
    }else
    {
        $controller->gotoAddArticle(null);
    } // End If Statement

}else if (isset($_POST['createArticle']))
{

    if (isset($_POST['setArticleToAllPages']))
    {
        $allPage = 1;
    }else
    {
        $allPage = 0;
    } // End If Statement

    if (!empty($_SESSION['UserID']))
    {
        $controller->createArticle($_SESSION['UserID'],
                                   $_POST['title'],
                                   $_POST['name'],
                                   $_POST['description'],
                                   $_POST['content'],
                                   $allPage,
                                   $_POST['contentAreaList'],
                                   $_POST['pageList']);
    } // End If Statement

}else if (isset($_POST['loginBtn']))
{

    if (!empty($_POST['username']) && !empty($_POST['password']))
    {
        $result = $controller->login($_POST['username'], $_POST['password']);

        if ($result != null)
        {
            $_SESSION['UserID'] =  $result;
        }else
        {
            $result = "Invalid username or password.";
        } // End If Statement

        if (!empty($_SESSION['UserID']))
        {
            $controller->displayPage($_POST['pageAlias'], $_SESSION['UserID']);
        }else
        {
            $controller->displayPage($_POST['pageAlias'], null);
        } // End If Statement

    }else
    {
        $result = "Please ensure you've filled in both the username and password fields";

        if (!empty($_SESSION['UserID']))
        {
            $controller->displayPage($_POST['pageAlias'], $_SESSION['UserID']);
        }else
        {
            $controller->displayPage($_POST['pageAlias'], null);
        } // End If Statement

    } // End If Statement

}else if (isset($_POST['logOut']))
{

    session_destroy();
    session_start();
    $controller->displayPage($_POST['pageAlias'], null);

}else if(isset($_GET['page']))
{

    if($_GET['page'] == "admin")
    {
        header("location:admin.php");
    }else if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage($_GET['page'], $_SESSION['UserID']);
    }else
    {
        $controller->displayPage($_GET['page'], null);
    } // End If Statement

}else
{

    if (!empty($_SESSION['UserID']))
    {
        $controller->displayPage(null, $_SESSION['UserID']);
    }else
    {
        $controller->displayPage(null, null);
    } // End If Statement
} // End If Statement


?>