<?php

require_once '../Controller/AdminController.php';

$controller = new AdminController();

$searchValue = "";

if(!empty($_GET['searchExpr']))
{
    $searchValue = $_GET['searchExpr'];
    $validName = false;

    $userObj = $controller->checkUserNameExists($searchValue);

    if ($userObj->getUsername() != NULL)
    {
        $validName = false;
    }else
    {
        $validName = true;
    } // End If Statement

    echo $validName;

} // End If Statement
?>
