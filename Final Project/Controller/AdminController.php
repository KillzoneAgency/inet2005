<?php

require_once('../Model/UserModel.php');
require_once('../Model/RoleModel.php');
require_once('../Model/StyleModel.php');

class AdminController
{

    private $userModel;
    private $roleModel;
    private $styleModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->roleModel = new RoleModel();
        $this->styleModel = new StyleModel();
    } // End Constructor Method

    public function checkUserNameExists($name)
    {

        return $this->userModel->checkUserNameExists($name);

    } // End checkUserNameExists Method

    public function updateUser($userToEdit, $username, $firstName, $lastName, $password, $userID)
    {

        $styleObject = $this->styleModel->selectStyle();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        $result = $this->userModel->updateUser($userToEdit, $username, $firstName, $lastName, $password, $userID);

        $userToEdit = $this->userModel->selectUserFromNameOrID($userToEdit);

        include '../View/editUser.php';

    } // End updateUser Method

    public function createUser($username, $firstName, $lastName, $password, $userID)
    {

        $styleObject = $this->styleModel->selectStyle();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        $result = $this->userModel->createUser($username, $firstName, $lastName, $password, $userID);

        include '../View/addUser.php';

    } // End createUser Method

    public function displayPage($pageAlias, $userID, $extraUserID)
    {

        if ($pageAlias == null)
        {
            $pageAlias = "adminMain";
        } // End If Statement

        $styleObject = $this->styleModel->selectStyle();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        if ($pageAlias == "adminMain")
        {
            include '../View/adminMain.php';
        }else if ($pageAlias == "displayUsers")
        {
            $arrayOfUsers = $this->userModel->selectUsers();
            include '../View/displayUsers.php';
        }else if ($pageAlias == "editUser")
        {
            $userToEdit = $this->userModel->selectUserFromNameOrID($extraUserID);
            include '../View/editUser.php';
        }else if ($pageAlias == "addUser")
        {
            include '../View/addUser.php';
        }// End If Statement

    } // End displayPage Method

} // End PublicController Class

?>