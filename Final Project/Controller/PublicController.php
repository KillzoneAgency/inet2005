<?php

require_once('../Model/PageModel.php');
require_once('../Model/ArticleModel.php');
require_once('../Model/UserModel.php');
require_once('../Model/RoleModel.php');
require_once('../Model/ContentAreaModel.php');
require_once('../Model/StyleModel.php');

class PublicController
{
    private $pageModel;
    private $articleModel;
    private $userModel;
    private $roleModel;
    private $contentAreaModel;
    private $styleModel;

    public function __construct()
    {
        $this->pageModel = new PageModel();
        $this->articleModel = new ArticleModel();
        $this->userModel = new UserModel();
        $this->roleModel = new RoleModel();
        $this->contentAreaModel = new ContentAreaModel();
        $this->styleModel = new StyleModel();
    } // End Constructor Method

    public function updateArticle($articleID, $userID, $title, $content, $description, $pageId, $contentAreaID, $hidePage)
    {

        $styleObject = $this->styleModel->selectStyle();

        $result = $this->articleModel->updateArticle($articleID, $userID, $title, $content, $description, $pageId, $contentAreaID, $hidePage);

        $arrayOfPages = $this->pageModel->selectPages();

        $contentAreaArray = $this->contentAreaModel->getContentAreas();

        $articleObject = $this->articleModel->selectArticleByID($articleID);

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        include '../View/alterArticles.php';

    } // End updateArticle Method

    public function gotoAddArticle($userID)
    {

        $styleObject = $this->styleModel->selectStyle();

        $arrayOfPages = $this->pageModel->selectPages();

        $contentAreaArray = $this->contentAreaModel->getContentAreas();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        include '../View/addArticles.php';

    } // End gotoAddArticle Method

    public function createArticle($userID, $title, $name, $description, $content, $allPage, $contentAreaID, $pageID)
    {

        $styleObject = $this->styleModel->selectStyle();

        $result = $this->articleModel->createArticle($userID, $title, $name, $description, $content, $allPage, $contentAreaID, $pageID);

        $articleObject = $this->articleModel->selectLastCreatedArticle();

        $arrayOfPages = $this->pageModel->selectPages();

        $contentAreaArray = $this->contentAreaModel->getContentAreas();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        include '../View/addArticles.php';

    } // End createArticle Method

    public function getArticleToEdit($articleID, $userID)
    {

        $styleObject = $this->styleModel->selectStyle();

        $arrayOfPages = $this->pageModel->selectPages();

        $contentAreaArray = $this->contentAreaModel->getContentAreas();

        $articleObject = $this->articleModel->selectArticleByID($articleID);

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        include '../View/alterArticles.php';

    } // End If Statement

    public function displayPage($pageAlias, $userID)
    {

        if ($pageAlias == null)
        {
            $pageAlias = "home";
        } // End If Statement

        $styleObject = $this->styleModel->selectStyle();

        $pageObject = $this->pageModel->selectPage($pageAlias);

        $arrayOfPages = $this->pageModel->selectPages();

        if ($userID != null)
        {
            $userObject = $this->userModel->selectUserFromNameOrID($userID);
        } // End If Statement

        include '../View/articleDisplay.php';

    } // End displayPage Method

    public function login($username, $password)
    {

        $loginResult = $this->userModel->attemptLogin($username, $password);

        return $loginResult;

    } // End login Function

} // End PublicController Class

?>