<?php

require_once 'Data/DataAccess.php';
require_once 'ContentArea.php';

class ContentAreaModel
{

    public static function getContentAreas()
    {

        // Get an instance of dataAccess
        $dataAccess = dataAccess::getInstance();

        // Connect to the database
        $dataAccess->connectToDB();

        $dataAccess->selectContentAreas();

        while($row = $dataAccess->retrieveRecords())
        {

            // Create a new content area
            $contentArea = new ContentArea($dataAccess->retrieveContentAreaID($row),
                                           $dataAccess->retrieveContentAreaName($row),
                                           $dataAccess->retrieveContentAreaAlias($row),
                                           $dataAccess->retrieveContentAreaOrder($row),
                                           $dataAccess->retrieveContentAreaDescription($row),
                                           $dataAccess->retrieveContentAreaCreatedDate($row),
                                           $dataAccess->retrieveContentAreaModifiedDate($row),
                                           $dataAccess->retrieveContentAreaCreatedByID($row),
                                           $dataAccess->retrieveContentAreaModifiedByID($row));

            // Put it into an array
            $arrayOfContentAreas[] = $contentArea;
        } // End While Loop

        // Close the database
        $dataAccess->closeDB();

        return $arrayOfContentAreas;

    } // End getContentAreas Method

} // End ContentAreaModel Class

?>