<?php

require_once 'DataAccess.php';
class DataAccessMySQL extends DataAccess
{

    private $dbCon;
    private $result;

    // aDataAccess methods
    public function connectToDB()
    {

        if ($this->dbCon == null)
        {

            // Creating Connection
            $this->dbCon = mysqli_connect("localhost","super","Password-01","custom_cms");

            // Checking Connection for Errors
            if (!$this->dbCon)
            {
                die('Could not connect to the Chinook Database: ' . $this->dbCon->connect_errno);
            }

        } // End If Statement

    } //  End connectToDB Method

    public function closeDB()
    {
        $this->dbCon->close();
        $this->dbCon = null;
    } // End closeDB Method

    public function selectStyle()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM styles ";
        $selectStatement .= "WHERE s_activeState = '1';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectStyle

    public function selectPage($alias)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM pages ";
        $selectStatement .= "WHERE p_alias = '";
        $selectStatement .= $alias;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectTrack Method

    public function selectPages()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM pages;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectPages Method

    public function selectContentAreasByPageID($id)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatementHead = "SELECT c_id, c_name, c_alias, c_order, c_description, c_createdDate, c_modifiedDate, c_createdBy, c_modifedBy ";
        $selectStatementHead .= "FROM content_areas ";
        $selectStatementHead .= "JOIN articles ON (c_id = a_contentArea) ";

        $selectStatement = $selectStatementHead;
        $selectStatement .= "JOIN pages ON (p_id = a_page) ";
        $selectStatement .= "WHERE p_id = ";
        $selectStatement .= $id;
        $selectStatement .= " AND a_hidePage = 0 ";
        $selectStatement .= "UNION ";
        $selectStatement .= $selectStatementHead;
        $selectStatement .= "WHERE a_allPage = 1 ";
        $selectStatement .= "AND a_hidePage = 0 ";
        $selectStatement .= "ORDER BY c_order;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectContentAreasByPageID Method

    public function selectContentAreas()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM content_areas;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectContentAreas Method

    public function selectArticlesByPageAndCAID($c_id, $p_id)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatementHead = "SELECT a_id, a_name, a_title, a_description, a_content, a_createdDate, a_modifiedDate,
                            a_allPage, a_contentArea, a_page, a_createdBy, a_modifiedBy ";
        $selectStatementHead .= "FROM articles ";
        $selectStatementHead .= "JOIN content_areas ON (a_contentArea  = c_id) ";

        $selectStatement = $selectStatementHead;
        $selectStatement .= "JOIN pages ON (p_id = a_page) ";
        $selectStatement .= "WHERE c_id = ";
        $selectStatement .= $c_id;
        $selectStatement .= " AND p_id = ";
        $selectStatement .= $p_id;
        $selectStatement .= " AND a_hidePage = 0 ";
        $selectStatement .="UNION ";
        $selectStatement .= $selectStatementHead;
        $selectStatement .= "WHERE a_allPage = 1 ";
        $selectStatement .= "AND c_id = ";
        $selectStatement .= $c_id;
        $selectStatement .= " AND a_hidePage = 0 ";
        $selectStatement .="ORDER BY a_createdDate DESC";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectArticlesByContentAreaID Method

    public function selectLastCreatedArticle()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM articles ";
        $selectStatement .= "ORDER BY a_id DESC ";
        $selectStatement .= "LIMIT 1;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectCreatedObject Method

    public function selectArticleByID($id)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM articles ";
        $selectStatement .= "WHERE a_id = '";
        $selectStatement .= $id;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectArticleByID

    public function selectUsers()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM user;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectUsers Method

    public function selectUser($username, $hashedPassword)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $username = stripslashes($username);
        $hashedPassword = stripslashes($hashedPassword);

        $username = mysqli_real_escape_string($this->dbCon, $username);
        $hashedPassword = mysqli_real_escape_string($this->dbCon, $hashedPassword);

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM user ";
        $selectStatement .= "WHERE u_username = '";
        $selectStatement .= $username;
        $selectStatement .= "' AND u_password = '";
        $selectStatement .= $hashedPassword;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectUser Method

    public function selectUserByID($id)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM user ";
        $selectStatement .= "WHERE u_id = '";
        $selectStatement .= $id;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectUserByID Method

    public function selectUserFromName($username)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $username = stripslashes($username);
        $username = mysqli_real_escape_string($this->dbCon, $username);

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM user ";
        $selectStatement .= "WHERE u_username = '";
        $selectStatement .= $username;
        $selectStatement .= "';";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectUserFromName Method

    public function selectRoles()
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT * ";
        $selectStatement .= "FROM roles;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectRoles Method

    public function selectRolesByUserID($id)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        $selectStatement = "SELECT r_id, r_name ";
        $selectStatement .= "FROM roles ";
        $selectStatement .= "JOIN user_roles ON (ur_r_id = r_id) ";
        $selectStatement .= "JOIN user ON (u_id = ur_u_id) ";
        $selectStatement .= "WHERE u_id = ";
        $selectStatement .= $id;
        $selectStatement .= ";";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

    } // End selectRolesByUserID Method

    public function insertArticle($userID, $title, $name, $description, $content, $allPage, $contentAreaID, $pageID)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $title = stripslashes($title);
        $title = mysqli_real_escape_string($this->dbCon, $title);

        $description = stripslashes($description);
        $description = mysqli_real_escape_string($this->dbCon, $description);

        $content = stripslashes($content);
        $content = mysqli_real_escape_string($this->dbCon, $content);


        $insertStatement = "INSERT INTO articles (";
        $insertStatement .= "a_name, ";
        $insertStatement .= "a_title, ";
        $insertStatement .= "a_description, ";
        $insertStatement .= "a_content, ";
        $insertStatement .= "a_allPage, ";
        $insertStatement .= "a_contentArea, ";
        $insertStatement .= "a_page, ";
        $insertStatement .= "a_createdDate, ";
        $insertStatement .= "a_createdBy, ";
        $insertStatement .= "a_modifiedBy";
        $insertStatement .= ") VALUES(";
        $insertStatement .= "'$name', ";
        $insertStatement .= "'$title', ";
        $insertStatement .= "'$description', ";
        $insertStatement .= "'$content', ";
        $insertStatement .= "$allPage, ";
        $insertStatement .= "$contentAreaID, ";
        $insertStatement .= "$pageID, ";
        $insertStatement .= "null, ";
        $insertStatement .= "$userID, ";
        $insertStatement .= "$userID";
        $insertStatement .= ");";

        $this->result = mysqli_query($this->dbCon, $insertStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End insertArticle Method

    public function insertUser($username, $firstName, $lastName, $password, $salt, $userID)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $username = stripslashes($username);
        $username = mysqli_real_escape_string($this->dbCon, $username);

        $firstName = stripslashes($firstName);
        $firstName = mysqli_real_escape_string($this->dbCon, $firstName);

        $lastName = stripslashes($lastName);
        $lastName = mysqli_real_escape_string($this->dbCon, $lastName);

        $password = stripslashes($password);
        $password = mysqli_real_escape_string($this->dbCon, $password);


        $insertStatement = "INSERT INTO user (";
        $insertStatement .= "u_username, ";
        $insertStatement .= "u_fName, ";
        $insertStatement .= "u_lName, ";
        $insertStatement .= "u_password, ";
        $insertStatement .= "u_salt, ";
        $insertStatement .= "u_createdDate, ";
        $insertStatement .= "u_createdBy, ";
        $insertStatement .= "u_modifiedBy";
        $insertStatement .= ") VALUES(";
        $insertStatement .= "'$username', ";
        $insertStatement .= "'$firstName', ";
        $insertStatement .= "'$lastName', ";
        $insertStatement .= "'$password', ";
        $insertStatement .= "'$salt', ";
        $insertStatement .= "null, ";
        $insertStatement .= "$userID, ";
        $insertStatement .= "$userID";
        $insertStatement .= ");";

        $this->result = mysqli_query($this->dbCon, $insertStatement);

        if(!$this->result)
        {
            die("Could not insert record into the Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End insertUser Method

    public function updateUser($userToEditID, $username, $firstName, $lastName, $password, $salt, $userID)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $username = stripslashes($username);
        $username = mysqli_real_escape_string($this->dbCon, $username);

        $firstName = stripslashes($firstName);
        $firstName = mysqli_real_escape_string($this->dbCon, $firstName);

        $lastName = stripslashes($lastName);
        $lastName = mysqli_real_escape_string($this->dbCon, $lastName);

        $password = stripslashes($password);
        $password = mysqli_real_escape_string($this->dbCon, $password);

        $updateStatement = "UPDATE user ";
        $updateStatement .= "SET u_username =\"";
        $updateStatement .= $username;
        $updateStatement .= "\", u_fName = \"";
        $updateStatement .= $firstName;
        $updateStatement .= "\", u_lName = \"";
        $updateStatement .= $lastName;
        $updateStatement .= "\", u_password = \"";
        $updateStatement .= $password;
        $updateStatement .= "\", u_salt = \"";
        $updateStatement .= $salt;
        $updateStatement .= "\", u_modifiedBy = ";
        $updateStatement .= $userID;
        $updateStatement .= " WHERE u_id =";
        $updateStatement .= $userToEditID;
        $updateStatement .= ";";

        $this->result = mysqli_query($this->dbCon, $updateStatement);

        if(!$this->result)
        {
            die("Could not update record in the Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End updateUser Method

    public function updateArticleByID($articleID, $userID, $title, $content, $description, $pageId, $contentAreaID, $hidePage)
    {

        // Connect to the in case it was closed; otherwise, this does nothing.
        $this->connectToDB();

        // To protect from SQL injection
        $title = stripslashes($title);
        $title = mysqli_real_escape_string($this->dbCon, $title);

        $description = stripslashes($description);
        $description = mysqli_real_escape_string($this->dbCon, $description);

        $content = stripslashes($content);
        $content = mysqli_real_escape_string($this->dbCon, $content);

        $updateStatement = "UPDATE articles ";
        $updateStatement .= "SET a_title =\"";
        $updateStatement .= $title;
        $updateStatement .= "\", a_content = \"";
        $updateStatement .= $content;
        $updateStatement .= "\", a_description = \"";
        $updateStatement .= $description;
        $updateStatement .= "\", a_page = ";
        $updateStatement .= $pageId;
        $updateStatement .= ", a_contentArea = ";
        $updateStatement .= $contentAreaID;
        $updateStatement .= ", a_modifiedBy = ";
        $updateStatement .= $userID;
        $updateStatement .= ", a_hidePage = ";
        $updateStatement .= $hidePage;
        $updateStatement .= " WHERE a_id =";
        $updateStatement .= $articleID;
        $updateStatement .= ";";

        $this->result = mysqli_query($this->dbCon, $updateStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End updateArticleByID Method

    public function retrieveRecords()
    {
        if(!$this->result)
        {
            die("No records to display: " . $this->dbCon->error);
        }

        return mysqli_fetch_assoc($this->result);
    } // End retrieveRecords Method

    // ************************* PAGE RETRIEVE METHODS *************************

    public function retrievePageID($row)
    {
        return $row['p_id'];
    } // End retrievePageID Method

    public function retrievePageName($row)
    {
        return $row['p_name'];
    } // End retrievePageName Method

    public function retrievePageAlias($row)
    {
        return $row['p_alias'];
    } // End retrievePageAlias Method

    public function retrievePageDescription($row)
    {
        return $row['p_description'];
    } // End retrievePageDescription Method

    public function retrievePageCreatedByID($row)
    {
        return $row['p_createdBy'];
    } // End retrievePageCreatedByID Method

    public function retrievePageModifiedByID($row)
    {
        return $row['p_modifiedBy'];
    } // End retrievePageModifiedByID Method

    public function retrievePageCreatedDate($row)
    {
        return $row['p_createdDate'];
    } // End retrievePageCreatedDate Method

    public function retrievePageModifiedDate($row)
    {
        return $row['p_modifiedDate'];
    } // End retrievePageModifiedDate Method


    // ************************* CONTENT AREA RETRIEVE METHODS *************************

    public function retrieveContentAreaID($row)
    {
        return $row['c_id'];
    } // End retrieveContentAreaID Method

    public function retrieveContentAreaName($row)
    {
        return $row['c_name'];
    } // End retrieveContentAreaName Method

    public function retrieveContentAreaAlias($row)
    {
        return $row['c_alias'];
    } // End retrieveContentAreaAlias Method

    public function retrieveContentAreaOrder($row)
    {
        return $row['c_order'];
    } // End retrieveContentAreaOrder Method

    public function retrieveContentAreaDescription($row)
    {
        return $row['c_description'];
    } // End retrieveContentAreaDescription Method

    public function retrieveContentAreaCreatedByID($row)
    {
        return $row['c_createdBy'];
    } // End retrieveContentAreaCreatedByID Method

    public function retrieveContentAreaModifiedByID($row)
    {
        return $row['c_modifedBy'];
    } // End retrieveUserModifiedByID Method

    public function retrieveContentAreaCreatedDate($row)
    {
        return $row['c_createdDate'];
    } // End retrieveContentAreaCreatedDate Method

    public function retrieveContentAreaModifiedDate($row)
    {
        return $row['c_modifiedDate'];
    } // End retrieveContentAreaModifiedDate Method

    // ************************* ARTICLE RETRIEVE METHODS *************************

    public function retrieveArticleID($row)
    {
        return $row['a_id'];
    } // End retrieveArticleID Method

    public function retrieveArticleName($row)
    {
        return $row['a_name'];
    } // End retrieveArticleName Method

    public function retrieveArticleTitle($row)
    {
        return $row['a_title'];
    } // End retrieveArticleTitle Method

    public function retrieveArticleDescription($row)
    {
        return $row['a_description'];
    } // End retrieveArticleDescription Method

    public function retrieveArticleContent($row)
    {
        return $row['a_content'];
    } // End retrieveArticleContent Method

    public function retrieveArticleCreatedByID($row)
    {
        return $row['a_createdBy'];
    } // End retrieveArticleCreatedByID Method

    public function retrieveArticleModifiedByID($row)
    {
        return $row['a_modifiedBy'];
    } // End retrieveArticleModifiedByID Method

    public function retrieveArticleAllPagesBit($row)
    {
        return $row['a_allPage'];
    } // End retrieveArticleAllPagesBit Method

    public function retrieveArticlePageID($row)
    {
        return $row['a_page'];
    } // End retrieveArticlePageID Method

    public function retrieveArticleContentAreaID($row)
    {
        return $row['a_contentArea'];
    } // End retrieveArticleContentAreaID Method

    public function retrieveArticleCreatedDate($row)
    {
        return $row['a_createdDate'];
    } // End retrieveArticleCreatedDate Method

    public function retrieveArticleModifiedDate($row)
    {
        return $row['a_modifiedDate'];
    } // End retrieveArticleModifiedDate Method

    // ************************* USER RETRIEVE METHODS *************************

    public function retrieveUserID($row)
    {
        return $row['u_id'];
    } // End retrieveUserID Method

    public function retrieveUserUsername($row)
    {
        return $row['u_username'];
    } // End retrieveUserUsername Method

    public function retrieveUserFirstName($row)
    {
        return $row['u_fName'];
    } // End retrieveUserFirstName Method

    public function retrieveUserLastName($row)
    {
        return $row['u_lName'];
    } // End retrieveUserLastName Method

    public function retrieveUserPassword($row)
    {
        return $row['u_password'];
    } // End retrieveUserPassword Method

    public function retrieveUserSalt($row)
    {
        return $row['u_salt'];
    } // End retrieveUserSalt Method

    public function retrieveUserCreatedByID($row)
    {
        return $row['u_createdBy'];
    } // End retrieveUserCreatedByID Method

    public function retrieveUserModifiedByID($row)
    {
        return $row['u_modifiedBy'];
    } // End retrieveUserModifiedByID Method

    public function retrieveUserCreatedDate($row)
    {
        return $row['u_createdDate'];
    } // End retrieveUserCreatedDate Method

    public function retrieveUserModifiedDate($row)
    {
        return $row['u_modifiedDate'];
    } // End retrieveUserModifiedDate Method

    // ************************* ROLE RETRIEVE METHODS *************************

    public function retrieveRoleID($row)
    {
        return $row['r_id'];
    } // End retrieveRoleID Method

    public function retrieveRoleName($row)
    {
        return $row['r_name'];
    } // End retrieveRoleName Method

    // ************************* USER ROLE RETRIEVE METHODS *************************

    public function retrieveUserRoleID($row)
    {
        return $row['ur_id'];
    } // End retrieveUserRoleID Method

    public function retrieveUserRoleUserID($row)
    {
        return $row['ur_u_id'];
    } // End retrieveUserID Method

    public function retrieveUserRoleRoleID($row)
    {
        return $row['ur_r_id'];
    } // End retrieveRoleID Method

    // ************************* STYLE RETRIEVE METHODS *************************

    public function retrieveStyleID($row)
    {
        return $row['s_id'];
    } // End retrieveStyleID Method

    public function retrieveStyleName($row)
    {
        return $row['s_name'];
    } // End retrieveStyleName Method

    public function retrieveStyleDescription($row)
    {
        return $row['s_description'];
    } // End retrieveStyleDescription Method

    public function retrieveStyleActiveState($row)
    {
        return $row['s_activeState'];
    } // End retrieveStyleActiveState Method

    public function retrieveStyleStyleSheet($row)
    {
        return $row['s_stylesheet'];
    } // End retrieveStyleStyleSheet Method

    public function retrieveStyleCreatedByID($row)
    {
        return $row['s_createdBy'];
    } // End retrieveStyleCreatedByID Method

    public function retrieveStyleModifiedByID($row)
    {
        return $row['s_modifiedBy'];
    } // End retrieveStyleModifiedByID Method

    public function retrieveStyleCreatedDate($row)
    {
        return $row['s_createdDate'];
    } // End retrieveStyleCreatedDate Method

    public function retrieveStyleModifiedDate($row)
    {
        return $row['s_modifiedDate'];
    } // End retrieveStyleModifiedDate Method

} // End DataAccessMySQL Class

?>