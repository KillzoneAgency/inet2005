<?php

require_once 'DataAccessMySQL.php';

abstract class DataAccess
{
    private static $dataAccess;

    public static function getInstance()
    {
        // If there's no instance of DataAccess then create one.
        if(self::$dataAccess == null)
        {
            self::$dataAccess = new DataAccessMySQL();
        }
        return self::$dataAccess;
    }

    public abstract function connectToDB();

    public abstract function closeDB();

} // End DataAccess Class
?>