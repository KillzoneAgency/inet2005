<?php
Class Article
{

    private $articleID;
    private $name;
    private $title;
    private $description;
    private $content;
    private $createdDate;
    private $modifiedDate;
    private $allPages;
    private $createdBy;
    private $modifiedBy;
    private $pageID;
    private $contentAreaID;
    //private $contentAreaObject;


    public function __construct($id, $name, $title, $description, $content, $createdDate, $modifiedDate, $allPages, $createdBy, $modifiedBy, $pageID, $contentAreaID)
    {

        $this->articleID = $id;
        $this->name = $name;
        $this->title = $title;
        $this->description = $description;
        $this->content = $content;
        $this->createdDate = $createdDate;
        $this->modifiedDate = $modifiedDate;
        $this->allPages = $allPages;
        $this->createdBy = $createdBy;
        $this->modifiedBy = $modifiedBy;
        $this->pageID = $pageID;
        $this->contentAreaID = $contentAreaID;

    } // End Constructor Method

    public function setAllPages($allPages)
    {
        $this->allPages = $allPages;
    } // End setAllPages Method

    public function getAllPages()
    {
        return $this->allPages;
    } // End getAllPages Method

    public function setArticleID($articleID)
    {
        $this->articleID = $articleID;
    } // End setArticleID Method

    public function getArticleID()
    {
        return $this->articleID;
    } // End getArticleID Method

    public function setContent($content)
    {
        $this->content = $content;
    } // End setContent Method

    public function getContent()
    {
        return $this->content;
    } // End getContent Method

    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    } // End setCreatedDate Method

    public function getCreatedDate()
    {
        return $this->createdDate;
    } // End getCreatedDate Method

    public function setDescription($description)
    {
        $this->description = $description;
    } // End setDescription Method

    public function getDescription()
    {
        return $this->description;
    } // End getDescription Method

    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    } // End setModifiedDate Method

    public function getModifiedDate()
    {
        return $this->modifiedDate;
    } // End getModifiedDate Method

    public function setName($name)
    {
        $this->name = $name;
    } // End setName Method

    public function getName()
    {
        return $this->name;
    } // End getName Method

    public function setTitle($title)
    {
        $this->title = $title;
    } // End setTitle Method

    public function getTitle()
    {
        return $this->title;
    } // End getTitle Method

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    } // End setModifiedBy Method

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    } // End getModifiedBy Method

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    } // End setCreatedBy Method

    public function getCreatedBy()
    {
        return $this->createdBy;
    } // End getCreatedBy Method

    public function setPageID($pageID)
    {
        $this->pageID = $pageID;
    } // End setPageID Method

    public function getPageID()
    {
        return $this->pageID;
    } // End getPageID Method

    public function setContentAreaID($contentAreaID)
    {
        $this->contentAreaID = $contentAreaID;
    } // End setContentAreaID Method

    public function getContentAreaID()
    {
        return $this->contentAreaID;
    } // End getContentAreaID Method

    /*    public function setContentAreaObject($contentAreaObject)
    {
        $this->contentAreaObject = $contentAreaObject;
    } // End setContentAreaObject Method

    public function getContentAreaObject()
    {
        return $this->contentAreaObject;
    } // End getContentAreaObject Method*/

} // End Article Class