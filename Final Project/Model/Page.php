<?php

Class Page
{

    private $pageID;
    private $name;
    private $alias;
    private $description;
    private $createdDate;
    private $modifiedDate;
    private $createdBy;
    private $modifiedBy;
    private $arrayOfContentAreas;

    public function __construct($id, $name, $alias, $description, $createdDate, $modifiedDate, $createdBy, $modifiedBy)
    {

        $this->pageID = $id;
        $this->name = $name;
        $this->alias = $alias;
        $this->description = $description;
        $this->createdDate = $createdDate;
        $this->modifiedDate = $modifiedDate;
        $this->createdBy = $createdBy;
        $this->modifiedBy = $modifiedBy;

    } // End Constructor Method

    public function getAlias()
    {
        return $this->alias;
    } // End getAlias Method

    public function getCreatedDate()
    {
        return $this->createdDate;
    } // End getCreatedDate Method

    public function getDescription()
    {
        return $this->description;
    } // End getDescription Method

    public function getName()
    {
        return $this->name;
    } // End getName Method

    public function getPageID()
    {
        return $this->pageID;
    } // End getPageID Method

    public function setArrayOfContentAreas($arrayOfContentAreas)
    {
        $this->arrayOfContentAreas = $arrayOfContentAreas;
    } // End setArrayOfContentAreas Method

    public function getArrayOfContentAreas()
    {
        return $this->arrayOfContentAreas;
    } // End getArrayOfContentAreas Method

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    } // End getModifiedBy Method

    public function getCreatedBy()
    {
        return $this->createdBy;
    } // End getCreatedBy Method

} // End Page Class