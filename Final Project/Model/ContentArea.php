<?php

Class ContentArea
{

    private $contentAreaID;
    private $name;
    private $alias;
    private $order;
    private $description;
    private $createdDate;
    private $modifiedDate;
    private $createdBy;
    private $modifiedBy;
    private $arrayOfArticles;

    public function __construct($id, $name, $alias, $order, $description, $createdDate, $modifiedDate, $createdBy, $modifiedBy)
    {

        $this->contentAreaID = $id;
        $this->name = $name;
        $this->alias = $alias;
        $this->order = $order;
        $this->description = $description;
        $this->createdDate = $createdDate;
        $this->modifiedDate = $modifiedDate;
        $this->createdBy = $createdBy;
        $this->modifiedBy = $modifiedBy;

    } // End Constructor Method

    public function getAlias()
    {
        return $this->alias;
    } // End getAlias Method

    public function setArrayOfArticles($arrayOfArticles)
    {
        $this->arrayOfArticles = $arrayOfArticles;
    } // End setArrayOfArticles Method

    public function getArrayOfArticles()
    {
        return $this->arrayOfArticles;
    } // End getArrayOfArticles Method

    public function setContentAreaID($contentAreaID)
    {
        $this->contentAreaID = $contentAreaID;
    } // End setContentAreaID Method

    public function getContentAreaID()
    {
        return $this->contentAreaID;
    } // End getContentAreaID Method

    public function getCreatedDate()
    {
        return $this->createdDate;
    } // End getCreatedDate Method

    public function getDescription()
    {
        return $this->description;
    } // End getDescription Method

    public function getModifiedDate()
    {
        return $this->modifiedDate;
    } // End getModifiedDate Method

    public function getName()
    {
        return $this->name;
    } // End getName Method

    public function getOrder()
    {
        return $this->order;
    } // End getOrder Method

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    } // End getModifiedBy Method

    public function getCreatedBy()
    {
        return $this->createdBy;
    } // End getCreatedBy Method

} // End ContentArea Class