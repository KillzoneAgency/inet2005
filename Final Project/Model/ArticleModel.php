<?php

require_once 'Data/DataAccess.php';
require_once 'Article.php';

class ArticleModel
{

    public static function createArticle($userID, $title, $name, $description, $content, $allPage, $contentAreaID, $pageID)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $content = htmlspecialchars($content);

        if ($userID == null)
        {
            $rowsAffected = -1;
        }else
        {
            $rowsAffected = $dataAccess->insertArticle($userID, $title, $name, $description, $content, $allPage, $contentAreaID, $pageID);
        }

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "Article successfully created.";
        }else
        {
            return "Unable to create article.";
        } // End If Statement

    } // End createArticle Method

    public static function selectLastCreatedArticle()
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectLastCreatedArticle();

        $row = $dataAccess->retrieveRecords();

        // Create an article
        $article = new Article($dataAccess->retrieveArticleID($row),
                               $dataAccess->retrieveArticleName($row),
                               $dataAccess->retrieveArticleTitle($row),
                               $dataAccess->retrieveArticleDescription($row),
                               $dataAccess->retrieveArticleContent($row),
                               $dataAccess->retrieveArticleCreatedDate($row),
                               $dataAccess->retrieveArticleModifiedDate($row),
                               $dataAccess->retrieveArticleAllPagesBit($row),
                               $dataAccess->retrieveArticleCreatedByID($row),
                               $dataAccess->retrieveArticleModifiedByID($row),
                               $dataAccess->retrieveArticlePageID($row),
                               $dataAccess->retrieveArticleContentAreaID($row));

        $dataAccess->closeDB();

        $article->setContent(htmlspecialchars_decode($article->getContent()));

        return $article;

    } // End selectLastCreatedArticle Method

    public static function selectArticleByID($articleId)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectArticleByID($articleId);

        $row = $dataAccess->retrieveRecords();

        // Create an article
        $article = new Article($dataAccess->retrieveArticleID($row),
                               $dataAccess->retrieveArticleName($row),
                               $dataAccess->retrieveArticleTitle($row),
                               $dataAccess->retrieveArticleDescription($row),
                               $dataAccess->retrieveArticleContent($row),
                               $dataAccess->retrieveArticleCreatedDate($row),
                               $dataAccess->retrieveArticleModifiedDate($row),
                               $dataAccess->retrieveArticleAllPagesBit($row),
                               $dataAccess->retrieveArticleCreatedByID($row),
                               $dataAccess->retrieveArticleModifiedByID($row),
                               $dataAccess->retrieveArticlePageID($row),
                               $dataAccess->retrieveArticleContentAreaID($row));

        $dataAccess->closeDB();

        $article->setContent(htmlspecialchars_decode($article->getContent()));

        return $article;

    } // End selectArticleByID Method

    public static function updateArticle($articleID, $userID, $title, $content, $description, $pageId, $contentAreaID, $hidePage)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $content = htmlspecialchars($content);

        $rowsAffected = $dataAccess->updateArticleByID($articleID, $userID, $title, $content, $description, $pageId, $contentAreaID, $hidePage);

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "Article successfully updated.";
        }else
        {
            return "The article was not updated.";
        } // End If Statement

    } // End updateArticle Method

} // End PageModel Class

?>