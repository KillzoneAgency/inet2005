<?php

require_once 'Data/DataAccess.php';
require_once 'Style.php';

class StyleModel
{

    public static function selectStyle()
    {

        // Get an instance of dataAccess
        $dataAccess = dataAccess::getInstance();

        // Connect to the database
        $dataAccess->connectToDB();

        $dataAccess->selectStyle();

        $row = $dataAccess->retrieveRecords();

        $style = new Style($dataAccess->retrieveStyleID($row),
                           $dataAccess->retrieveStyleName($row),
                           $dataAccess->retrieveStyleDescription($row),
                           $dataAccess->retrieveStyleActiveState($row),
                           $dataAccess->retrieveStyleStyleSheet($row),
                           $dataAccess->retrieveStyleCreatedDate($row),
                           $dataAccess->retrieveStyleModifiedDate($row),
                           $dataAccess->retrieveStyleCreatedByID($row),
                           $dataAccess->retrieveStyleModifiedByID($row));

        // Close the database
        $dataAccess->closeDB();

        return $style;

    } // End selectRoles Method

} // End RoleModel Class

?>