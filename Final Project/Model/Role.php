<?php

Class Role
{

    private $roleID;
    private $name;

    public function __construct($id, $name)
    {

        $this->roleID = $id;
        $this->name = $name;

    } // End Constructor Method

    public function getName()
    {
        return $this->name;
    } // End getName Method

    public function getRoleID()
    {
        return $this->roleID;
    } // End getRolesID Method

} // End Role Class