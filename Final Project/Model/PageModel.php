<?php

require_once 'Data/DataAccess.php';
require_once 'Page.php';
require_once 'ContentArea.php';
require_once 'Article.php';

class PageModel
{

    public static function selectPage($alias)
    {

        // Get an instance of dataAccess
        $dataAccess = dataAccess::getInstance();

        // Connect to the database
        $dataAccess->connectToDB();

        // Get the selected page
        $dataAccess->selectPage($alias);

        // Get the returned records
        $row = $dataAccess->retrieveRecords();

        //Create a new page.
        $currentPage = new Page($dataAccess->retrievePageID($row),
                                $dataAccess->retrievePageName($row),
                                $dataAccess->retrievePageAlias($row),
                                $dataAccess->retrievePageDescription($row),
                                $dataAccess->retrievePageCreatedDate($row),
                                $dataAccess->retrievePageModifiedDate($row),
                                $dataAccess->retrievePageCreatedByID($row),
                                $dataAccess->retrievePageModifiedByID($row));


        // Get all the content areas contained within the page.
        $dataAccess->selectContentAreasByPageID($currentPage->getPageID());

        // Add all content areas to a temporary array.
        while($row = $dataAccess->retrieveRecords())
        {

            // Create a new content area
            $contentArea = new ContentArea($dataAccess->retrieveContentAreaID($row),
                                           $dataAccess->retrieveContentAreaName($row),
                                           $dataAccess->retrieveContentAreaAlias($row),
                                           $dataAccess->retrieveContentAreaOrder($row),
                                           $dataAccess->retrieveContentAreaDescription($row),
                                           $dataAccess->retrieveContentAreaCreatedDate($row),
                                           $dataAccess->retrieveContentAreaModifiedDate($row),
                                           $dataAccess->retrieveContentAreaCreatedByID($row),
                                           $dataAccess->retrieveContentAreaModifiedByID($row));

            // Put it into an array
            $tempContentAreaArray[] = $contentArea;
        } // End While Loop

        // For each content area, get all of its articles and put content areas into an array.
        foreach($tempContentAreaArray as $contentArea)
        {

            $dataAccess->selectArticlesByPageAndCAID($contentArea->getContentAreaID(), $currentPage->getPageID());

            while($row = $dataAccess->retrieveRecords())
            {

                // Create an article
                $article = new Article($dataAccess->retrieveArticleID($row),
                                       $dataAccess->retrieveArticleName($row),
                                       $dataAccess->retrieveArticleTitle($row),
                                       $dataAccess->retrieveArticleDescription($row),
                                       $dataAccess->retrieveArticleContent($row),
                                       $dataAccess->retrieveArticleCreatedDate($row),
                                       $dataAccess->retrieveArticleModifiedDate($row),
                                       $dataAccess->retrieveArticleAllPagesBit($row),
                                       $dataAccess->retrieveArticleCreatedByID($row),
                                       $dataAccess->retrieveArticleModifiedByID($row),
                                       $dataAccess->retrieveArticlePageID($row),
                                       $dataAccess->retrieveArticleContentAreaID($row));

                $article->setContent(htmlspecialchars_decode($article->getContent()));

                // Put it into an array
                $arrayOfArticles[] = $article;
            } // End While Loop

            // Put all of the articles into an array within each content array.
            $contentArea->setArrayOfArticles($arrayOfArticles);

            // Unset the array of articles to building up of articles for future content areas.
            unset($arrayOfArticles);

            // Put the content area into an array.
            $arrayOfContentAreas[] = $contentArea;

        } // End For Each Loop

        // Close the database
        $dataAccess->closeDB();

        // Put the array of content areas into an array within the page.
        $currentPage->setArrayOfContentAreas($arrayOfContentAreas);

        return $currentPage;
    } // End selectPage Method

    public static function selectPages()
    {

        // Get an instance of dataAccess
        $dataAccess = dataAccess::getInstance();

        // Connect to the database
        $dataAccess->connectToDB();

        $dataAccess->selectPages();

        while($row = $dataAccess->retrieveRecords())
        {

            //Create a new page.
            $tempPage = new Page($dataAccess->retrievePageID($row),
                                 $dataAccess->retrievePageName($row),
                                 $dataAccess->retrievePageAlias($row),
                                 $dataAccess->retrievePageDescription($row),
                                 $dataAccess->retrievePageCreatedDate($row),
                                 $dataAccess->retrievePageModifiedDate($row),
                                 $dataAccess->retrievePageCreatedByID($row),
                                 $dataAccess->retrievePageModifiedByID($row));

            // Put it into an array
            $arrayOfPages[] = $tempPage;
        } // End While Loop

        // Close the database
        $dataAccess->closeDB();

        return $arrayOfPages;

    } // End selectPages Method

} // End PageModel Class

?>