<?php

Class Style
{

    private $stylesID;
    private $name;
    private $description;
    private $activeState;
    private $styleSheet;
    private $createdDate;
    private $modifiedDate;
    private $createdBy;
    private $modifiedBy;

    public function __construct($id, $name, $description, $activeState, $styleSheet, $createdDate, $modifiedDate, $createdBy, $modifiedBy)
    {

        $this->stylesID = $id;
        $this->name = $name;
        $this->description = $description;
        $this->activeState = $activeState;
        $this->styleSheet = $styleSheet;
        $this->createdDate = $createdDate;
        $this->modifiedDate = $modifiedDate;
        $this->createdBy = $createdBy;
        $this->modifiedBy = $modifiedBy;

    } // End Constructor Method

    public function getActiveState()
    {
        return $this->activeState;
    } // End getActiveState Method

    public function getCreatedDate()
    {
        return $this->createdDate;
    } // End getCreatedDate Method

    public function getDescription()
    {
        return $this->description;
    } // End getDescription Method

    public function getModifiedDate()
    {
        return $this->modifiedDate;
    } // End getModifiedDate Method

    public function getName()
    {
        return $this->name;
    } // End getName Method

    public function getStyleSheet()
    {
        return $this->styleSheet;
    } // End getStyleSheet Method

    public function getStylesID()
    {
        return $this->stylesID;
    } // End getStylesID Method

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    } // End getModifiedBy Method

    public function getCreatedBy()
    {
        return $this->createdBy;
    } // End getCreatedBy Method

} // End Style Class