<?php

require_once 'Data/DataAccess.php';
require_once 'Role.php';

class RoleModel
{

    public static function selectRoles()
    {

        // Get an instance of dataAccess
        $dataAccess = dataAccess::getInstance();

        // Connect to the database
        $dataAccess->connectToDB();

        $dataAccess->selectRoles();

        // Add all roles to an array.
        while($row = $dataAccess->retrieveRecords())
        {

            // Create a new role
            $currentRole = new Role($dataAccess->retrieveRoleID($row),
                                    $dataAccess->retrieveRoleName($row));

            // Put it into an array
            $arrayOfRoles[] = $currentRole;
        } // End While Loop

        // Close the database
        $dataAccess->closeDB();

        return $arrayOfRoles;

    } // End selectRoles Method

} // End RoleModel Class

?>