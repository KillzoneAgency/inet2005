<?php

Class User
{

    private $userID;
    private $username;
    private $firstName;
    private $lastName;
    private $password;
    private $salt;
    private $createdDate;
    private $modifiedDate;
    private $createdBy;
    private $modifiedBy;
    private $arrayOfRoles;

    public function __construct($id, $username, $firstName, $lastName, $password, $salt, $createdDate, $modifiedDate, $createdBy, $modifiedBy)
    {

        $this->userID = $id;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->password = $password;
        $this->salt = $salt;
        $this->createdDate = $createdDate;
        $this->modifiedDate = $modifiedDate;
        $this->createdBy = $createdBy;
        $this->modifiedBy = $modifiedBy;

    } // End Constructor Method

    public function isRole($roleID)
    {

        foreach($this->arrayOfRoles as $userRole)
        {

            // Role ID of 1 is Author
            // Role ID of 2 is Editor
            // Role ID of 3 is Admin
            if ($userRole->getRoleID() == $roleID)
            {
                return true;
            } // End If Statement

        } // End For Each Loop

    } // End isRole Method

    public function getModifiedDate()
    {
        return $this->modifiedDate;
    } // End getModifiedDate Method

    public function getCreatedDate()
    {
        return $this->createdDate;
    } // End getCreatedDate Method

    public function getSalt()
    {
        return $this->salt;
    } // End getSalt Method

    public function getUsername()
    {
        return $this->username;
    } // End getUsername Method

    public function getPassword()
    {
        return $this->password;
    } // End getPassword Method

    public function getLastName()
    {
        return $this->lastName;
    } // End getLastName Method

    public function getFirstName()
    {
        return $this->firstName;
    } // End getFirstName Method

    public function getUserID()
    {
        return $this->userID;
    } // End getUserID Method

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    } // End getModifiedBy Method

    public function getCreatedBy()
    {
        return $this->createdBy;
    } // End getCreatedBy Methods

    public function setArrayOfRoles($arrayOfRoles)
    {
        $this->arrayOfRoles = $arrayOfRoles;
    } // End setArrayOfRoles Method

    public function getArrayOfRoles()
    {
        return $this->arrayOfRoles;
    } // End getArrayOfRoles Method

} // End User Class