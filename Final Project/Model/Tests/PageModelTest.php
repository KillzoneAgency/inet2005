<?php

require('PageModel.php');

class PageModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSelectPage()
    {

        $page = PageModel::selectPage("home");

        $this->assertNotEquals(null, $page);

    } // End testGetContentAreas Method

    public function testSelectPages()
    {

        $arrayOfPages = PageModel::selectPages();

        $this->assertNotEquals(null, $arrayOfPages);

    } // End testGetContentAreas Method

} // End PageModelTest Class