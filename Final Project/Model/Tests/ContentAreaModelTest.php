<?php

require('ContentAreaModel.php');

class ContentAreaModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testGetContentAreas()
    {

        $arrayOfArticles = ContentAreaModel::getContentAreas();

        $this->assertNotEquals(null, $arrayOfArticles);

    } // End testGetContentAreas Method

} // End ContentAreaModelTest Method
