<?php

require('RoleModel.php');

class RoleModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSelectRoles()
    {

        $arrayOfRoles = RoleModel::selectRoles();

        $this->assertNotEquals(null, $arrayOfRoles);

    } // End testSelectRoles Method

} // End RoleModelTest Class
