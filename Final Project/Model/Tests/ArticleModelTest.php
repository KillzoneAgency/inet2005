<?php

require('ArticleModel.php');

class ArticleModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSelectArticleByID()
    {

        $article = ArticleModel::selectArticleByID(1);

        $this->assertEquals("Header Article", $article->getName());

    } // End testSelectArticleByID Method

    public function testLastSelectedArticle()
    {

        $lastArticle = ArticleModel::selectLastCreatedArticle();

        $tempArticle = ArticleModel::selectArticleByID($lastArticle->getArticleID());

        $this->assertEquals($lastArticle, $tempArticle);

    } // End testLastSelectedArticle Method

    public function testInsertArticleSuccess()
    {

        $result = ArticleModel::createArticle(1, time(), time(), time(), "<p>" . time() . "</p>", 0, 1, 3);

        $this->assertEquals("Article successfully created.", $result);

    } // End testInsertArticleSuccess Method

    public function testInsertArticleFailed()
    {

        $result = ArticleModel::createArticle(null, time(), time(), time(), "<p>" . time() . "</p>", 0, 1, 3);

        $this->assertEquals("Unable to create article.", $result);

    } // End testInsertArticleFailed Method

    public function testUpdateArticleSuccess()
    {
        $result = ArticleModel::updateArticle(22, 1, time(), "<p>" . time() . "</p>", time(), 3, 1, 0);

        $this->assertEquals("Article successfully updated.", $result);

    } // End testUpdateArticle Method

    public function testUpdateArticleFailed()
    {

        $result = ArticleModel::updateArticle(22, 1, time(), "<p>" . time() . "</p>", time(), 3, 1, 0);

        $this->assertEquals("The article was not updated.", $result);

    } // End testUpdateArticleFailed Method

} // End ArticleModelTest Class
