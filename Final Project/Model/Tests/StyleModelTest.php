<?php

require('StyleModel.php');

class StyleModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSelectStyle()
    {

        $style = StyleModel::selectStyle();

        $this->assertEquals(2, $style->getStylesID());

    } // End selectStyle Method

} // End StyleModelTest Class
