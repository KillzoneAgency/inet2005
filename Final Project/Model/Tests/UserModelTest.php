<?php

require('UserModel.php');

class UserModelTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testCreateUser()
    {

        //$username, $firstName, $lastName, $password, $userID
        $result = UserModel::createUser(time(), time(), time(), time(), 1);

        $this->assertEquals("User created successfully.", $result);

    } // End testCreateUser Method

    public function testSelectUsers()
    {

        $arrayOfUsers = UserModel::selectUsers();

        $this->assertNotEquals(null, $arrayOfUsers);

    } // End testSelectUsers Method

    public function testLogin()
    {

        $userID = UserModel::attemptLogin("DanielMB", "Password-01");

        $this->assertEquals(1, $userID);

    } // End testLogin Method

    public function testSelectUserFromID()
    {

        $userObject = UserModel::selectUserFromNameOrID(1);

        $this->assertEquals("DanielMB", $userObject->getUsername());

    } // End testSelectUserFromID Method

    public function testSelectUserFromName()
    {

        $userObject = UserModel::selectUserFromNameOrID("DanielMB");

        $this->assertEquals(1, $userObject->getUserID());

    } // End testSelectUserFromName Method

    public function testCheckUserNameExists()
    {

        $userObject = UserModel::checkUserNameExists("NOTREALY");

        $this->assertEquals(null, $userObject->getUserID());

    } // End testCheckUserNameExists Method

    public function testUpdateUserSuccess()
    {

        $result = UserModel::updateUser(6, time() + time(), time(), time(), UserModel::hashPassword(time(), time()), 1);

        $this->assertEquals("User updated successfully.", $result);

    } // End testUpdateUser Method

    public function testUpdateUserFailed()
    {

        $result = UserModel::updateUser(null, time() + time(), time(), time(), UserModel::hashPassword(time(), time()), 1);

        $this->assertEquals("An error occurred while updating a user.", $result);

    } // End testUpdateUserFailed Method

} // End UserModelTest Class