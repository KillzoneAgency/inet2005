<?php

require_once 'Data/DataAccess.php';
require_once 'User.php';
require_once 'Role.php';

class UserModel
{

    public static function updateUser($userToEdit, $username, $firstName, $lastName, $password, $userID)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $salt =  UserModel::genSalt();

        $password = UserModel::hashPassword($password, $salt);

        if ($userToEdit == null)
        {
            $rowsAffected = -1;
        }else
        {
            $rowsAffected = $dataAccess->updateUser($userToEdit, $username, $firstName, $lastName, $password, $salt, $userID);
        }

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "User updated successfully.";
        }else
        {
            return "An error occurred while updating a user.";
        } // End If Statement

    } // End updateUser Method

    public static function createUser($username, $firstName, $lastName, $password, $userID)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $salt =  UserModel::genSalt();

        $password = UserModel::hashPassword($password, $salt);

        $rowsAffected = $dataAccess->insertUser($username, $firstName, $lastName, $password, $salt, $userID);

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            return "User created successfully.";
        }else
        {
            return "An error occurred while creating a user.";
        } // End If Statement

    } // End createUser Method

    public static function checkUserNameExists($name)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        // Getting roles for a user.
        $dataAccess->selectUsers();

        $dataAccess->selectUserFromName($name);

        $row = $dataAccess->retrieveRecords();

        // Create a new user
        $currentUser =  new User($dataAccess->retrieveUserID($row), $dataAccess->retrieveUserUsername($row), null, null, null, null, null, null, null, null);

        $dataAccess->closeDB();

        return $currentUser;

    } // End checkUserNameExists Method

    public static function selectUsers()
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        // Getting roles for a user.
        $dataAccess->selectUsers();

        // Add all roles to a temporary array.
        while($row = $dataAccess->retrieveRecords())
        {

            // Create a new content user
            $currentUser =  new User($dataAccess->retrieveUserID($row),
                $dataAccess->retrieveUserUsername($row),
                $dataAccess->retrieveUserFirstName($row),
                $dataAccess->retrieveUserLastName($row),
                $dataAccess->retrieveUserPassword($row),
                $dataAccess->retrieveUserSalt($row),
                $dataAccess->retrieveUserCreatedDate($row),
                $dataAccess->retrieveUserModifiedDate($row),
                $dataAccess->retrieveUserCreatedByID($row),
                $dataAccess->retrieveUserModifiedByID($row));

            // Put it into an array
            $arrayOfUsers[] = $currentUser;
        } // End While Loop

        $dataAccess->closeDB();

        return $arrayOfUsers;

    } // End selectUsers Method

    public static function attemptLogin($username, $password)
    {

        // Creating a instance of user in order to get the salt.
        $userObj = UserModel::selectUserFromNameOrID($username);

        if ($userObj->getUsername() != NULL)
        {

            // Getting Hashed Version of the password
            $password = UserModel::hashPassword($password, $userObj->getSalt());

            if ($userObj->getPassword() == $password)
            {

                return $userObj->getUserID();
            }else
            {
                return null;
            } // If Statement to check if the credentials matched or not.

        }else
        {
            //return "Username doesn't exist.";
            return null;
        }// End If Statement

    } // End attemptLogin Method

    public static function selectUserFromNameOrID($idOrName)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        if (is_numeric($idOrName))
        {
            // Selecting a User by their ID
            $dataAccess->selectUserByID($idOrName);
        }else
        {
            // Selecting a User Based on their name (Unique)
            $dataAccess->selectUserFromName($idOrName);
        }

        // Getting the results.
        $row = $dataAccess->retrieveRecords();

        //$id, $username, $firstName, $lastName, $password, $salt, $createdDate, $modifiedDate, $createdBy, $modifiedBy

        // Creating a new user.
        $currentUser =  new User($dataAccess->retrieveUserID($row),
                                 $dataAccess->retrieveUserUsername($row),
                                 $dataAccess->retrieveUserFirstName($row),
                                 $dataAccess->retrieveUserLastName($row),
                                 $dataAccess->retrieveUserPassword($row),
                                 $dataAccess->retrieveUserSalt($row),
                                 $dataAccess->retrieveUserCreatedDate($row),
                                 $dataAccess->retrieveUserModifiedDate($row),
                                 $dataAccess->retrieveUserCreatedByID($row),
                                 $dataAccess->retrieveUserModifiedByID($row));


        if ($currentUser->getUserID() != null)
        {

            // Getting roles for a user.
            $dataAccess->selectRolesByUserID($currentUser->getUserID());

            // Add all roles to a temporary array.
            while($row = $dataAccess->retrieveRecords())
            {

                // Create a new content area
                $currentRole = new Role($dataAccess->retrieveRoleID($row),
                    $dataAccess->retrieveRoleName($row));

                // Put it into an array
                $tempArrayOfRoles[] = $currentRole;
            } // End While Loop

        }

        $dataAccess->closeDB();

        if (isset($tempArrayOfRoles))
        {
            $currentUser->setArrayOfRoles($tempArrayOfRoles);
        } // End If Statement

        return $currentUser;

    } // End selectUserFromName Method

    public static function hashPassword($password, $salt)
    {

        $hashInstructions = '$6$rounds=3000$' . $salt . '$';
        $hashedPasswordTemp = crypt($password, $hashInstructions);
        $hashedPassword = str_replace ($hashInstructions, "", $hashedPasswordTemp);

        return $hashedPassword;

    } // End hashPassword Method

    public static function genSalt()
    {
        return time();
    } // End genSalt Method

} // End UserModel Class

?>