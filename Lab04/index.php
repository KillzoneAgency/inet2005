<?php
    require("Circle.php");
    require("Rectangle.php");
    require("Triangle.php");


    if (empty($_POST['radius']) == false)
    {
        $circle = new Circle("Circle", $_POST['radius']);
    }else
    {
        $circle = new Circle("Circle", 1);
    } // End If Statement to create a circle

    if (empty($_POST['length'])  == false && empty($_POST['width']) == false)
    {
        $rectangle = new Rectangle("Rectangle", $_POST['length'], $_POST['width']);
    }else
    {
        $rectangle = new Rectangle("Rectangle", 1, 1);
    } // End If Statement to create a rectangle

    if (empty($_POST['base']) == false && empty($_POST['height'])  == false )
    {
        $triangle = new Triangle("Triangle", $_POST['base'], $_POST['height']);
    }else
    {
        $triangle = new Triangle("Triangle", 1, 1);
    } // End If Statement to create a triangle

    if (isset($_POST['submitBtn']))
    {

        if (empty($_POST['radius']) == false)
        {
            $circle = new Circle("Circle", $_POST['radius']);
        }else
        {
            $circle = new Circle("Circle", 1);
        } // End If Statement to create a circle

        if (empty($_POST['length'])  == false && empty($_POST['width']) == false)
        {
            $rectangle = new Rectangle("Rectangle", $_POST['length'], $_POST['width']);
        }else
        {
            $rectangle = new Rectangle("Rectangle", 1, 1);
        } // End If Statement to create a rectangle

        if (empty($_POST['base']) == false && empty($_POST['height'])  == false )
        {
            $triangle = new Triangle("Triangle", $_POST['base'], $_POST['height']);
        }else
        {
            $triangle = new Triangle("Triangle", 1, 1);
        } // End If Statement to create a triangle

    }else if (isset($_POST['resizeCircle']))
    {
        $circle->resize($_POST['circlePercent']);
    }else if (isset($_POST['resizeTriangle']))
    {
        $triangle->resize($_POST['trianglePercent']);
    } // End If Statement

?>

<!DOCTYPE html>

<html>

    <head>
        <title>Area Calculator</title>
    </head>

    <body>

        <section>

            <form id="calculationForm" name="calculationForm" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <fieldset>
                    <legend>Circle</legend>

                    <label for="radius">Radius: </label>
                    <input type="text" id="radius" name="radius" value="<?php echo $circle->getRadius(); ?>">
                    <input type="hidden" id="radiusHidden" name="radiusHidden" value="<?php echo $circle->getRadius(); ?>">

                    <label for="circlePercent">Resize: </label>
                    <input type="text" id="circlePercent" name="circlePercent">%
                    <input type="submit" id="resizeCircle" name="resizeCircle" value="Resize"/>
                </fieldset>

                <fieldset>
                    <legend>Rectangle</legend>

                    <label for="length">Length: </label>
                    <input type="text" id="length" name="length" value="<?php echo $rectangle->getLength(); ?>">
                    <input type="hidden" id="lengthHidden" name="lengthHidden" value="<?php echo $rectangle->getLength(); ?>">

                    <label for="width">Width: </label>
                    <input type="text" id="width" name="width" value="<?php echo $rectangle->getWidth(); ?>">
                    <input type="hidden" id="widthHidden" name="widthHidden" value="<?php echo $rectangle->getWidth(); ?>">
                </fieldset>

                <fieldset>
                    <legend>Triangle</legend>

                    <label for="base">Base: </label>
                    <input type="text" id="base" name="base" value="<?php echo $triangle->getBase(); ?>">
                    <input type="hidden" id="baseHidden" name="baseHidden" value="<?php echo $triangle->getBase(); ?>">

                    <label for="height">Height: </label>
                    <input type="text" id="height" name="height" value="<?php echo $triangle->getHeight(); ?>">
                    <input type="hidden" id="heightHidden" name="heightHidden" value="<?php echo $triangle->getHeight(); ?>">

                    <label for="trianglePercent">Resize: </label>
                    <input type="text" id="trianglePercent" name="trianglePercent">%
                    <input type="submit" id="resizeTriangle" name="resizeTriangle" value="Resize"/>
                </fieldset>

                <input type="submit" id="submitBtn" name="submitBtn" value="Calculate"/>

            </form>

            <?php

            if (isset($_POST['submitBtn']) || isset($_POST['resizeCircle']) || isset($_POST['resizeTriangle']))
            {

            ?>

            <p>

                <strong>Results:</strong> <br /> <br />

                <strong>Shape: <?php echo $circle->getShapeName()?> </strong> <br />
                Area: <?php echo $circle->calculateSize()?> <br /> <br />

                <strong>Shape: <?php echo $rectangle->getShapeName()?> </strong> <br />
                Area: <?php echo $rectangle->calculateSize()?> <br /> <br />

                <strong>Shape: <?php echo $triangle->getShapeName()?> </strong> <br />
                Area: <?php echo $triangle->calculateSize()?> <br /> <br />

            </p>

            <?php

            } // End If Statement

            ?>

        </section>

    </body>

</html>