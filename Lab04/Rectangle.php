<?php

require_once("Shape.php");

class Rectangle extends Shape
{

    protected $length;
    protected $width;

    public function __construct($shapeName, $length, $width)
    {
        parent::__construct($shapeName, $length, $width);
        $this->shapeName  = $shapeName;
        $this->length  = $length;
        $this->width = $width;
    } // End Constructor Method

    // Methods

    public function calculateSize()
    {
        $this->area = ($this->length * $this->width);
        return $this->area;
    } // End calculateSize Method

    // Getters and Setters
    public function getLength()
    {
        return $this->length;
    } // End getLength Method

    public function setLength($length)
    {
        $this->length = $length;
    } // End setLength Method

    public function getWidth()
    {
        return $this->width;
    } // End getWidth Method

    public function setWidth($width)
    {
        $this->width = $width;
    } // End setWidth Method

} // End Circle Class

?>