<?php

require_once("Shape.php");
require_once("iResizeable.php");

class Circle extends Shape
{

    protected $radius;
    protected $area;

    public function __construct($shapeName, $radius)
    {
        parent::__construct($shapeName, $radius);
        $this->shapeName  = $shapeName;
        $this->radius  = $radius;
    } // End Constructor Method

    // Methods

    public function calculateSize()
    {
        $this->area = pi() * pow($this->radius, 2);
        return $this->area;
    } // End calculateSize Method

    public function resize($percentage)
    {

        $this->radius *= ($percentage / 100);

    } // End resize Method

    // Getters and Setters

    public function getRadius()
    {
        return $this->radius;
    } // End getRadius Method

    public function setRadius($radius)
    {
        $this->radius = $radius;
    } // End setRadius Method

    public function getArea()
    {
        return $this->area;
    } // End getArea Method

    public function setArea($area)
    {
        $this->area = $area;
    } // End setArea Method

} // End Circle Class

?>