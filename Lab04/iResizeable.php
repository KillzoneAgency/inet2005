<?php

interface iResizeable
{
    public function resize($percentage);
} // End iResizeable interface

?>