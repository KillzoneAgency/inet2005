<?php

abstract class Shape
{
    protected $shapeName;
    protected $area;

    public function __construct($shapeName)
    {
        $this->shapeName  = $shapeName;
    } // End Constructor

    // Abstract methods
    abstract public function calculateSize();

    public function getShapeName()
    {
        return $this->shapeName;
    } // End getShapeName Method

    public function setShapeName($name)
    {
        $this->shapeName = $name;
    } // End setShapeName Method

} // End Shape Class

?>