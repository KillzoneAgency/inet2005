<?php

require_once("Shape.php");
require_once("iResizeable.php");

class Triangle extends Shape
{

    protected $base;
    protected $height;

    public function __construct($shapeName, $base, $height)
    {
        parent::__construct($shapeName, $base, $height);
        $this->shapeName  = $shapeName;
        $this->base  = $base;
        $this->height = $height;
    } // End Constructor Method

    // Methods

    public function resize($percentage)
    {

        $this->height *= ($percentage / 100);

    } // End resize Method

    public function calculateSize()
    {
        $this->area = (($this->base * $this->height) / 2);
        return $this->area;
    } // End calculateSize Method

    // Getters and Setters
    public function getBase()
    {
        return $this->base;
    } // End getBase Method

    public function setBase($base)
    {
        $this->base = $base;
    } // End setBase Method

    public function getHeight()
    {
        return $this->height;
    } // End getHeight Method

    public function setHeight($height)
    {
        $this->height = $height;
    } // End setHeight Method

    public function getArea()
    {
        return $this->area;
    } // End getArea Method

    public function setArea($area)
    {
        $this->area = $area;
    } // End setArea Method

} // End Circle Class

?>