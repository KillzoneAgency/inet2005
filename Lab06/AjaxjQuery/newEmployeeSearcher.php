<?php
header("Cache-Control: no-cache");

$results = "";
$searchExpr = "";


if(!empty($_GET['searchExpr']))
{
    $searchExpr = $_GET['searchExpr'];

    include("dbConn.php");

    connectToDB();

    selectEmployeesWithFirstNameStartingWith($searchExpr);

    while ($row = fetchEmployees())
    {

        $results .= "<div class='tooltip' title='Employee Number: " . $row['emp_no'] . " Birth Date: " . $row['birth_date'] . " Hire Date: " . $row['hire_date'] . "'>";
        $results .= $row['first_name'] . " " . $row['last_name'] . "<br/>";
        $results .= "</div>";

    }

    closeDB();
}

echo $results;


?>


