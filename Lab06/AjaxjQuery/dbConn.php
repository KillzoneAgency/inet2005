<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 $dbConnection;
 $result;

 function connectToDB()
 {
      global $dbConnection;
      $dbConnection = mysqli_connect("localhost","root", "","employees");
      if (!$dbConnection)
      {
            die('Could not connect to the employees Database: ' .
                    mysqli_error($dbConnection));
      }
 }

 function closeDB()
 {
      global $dbConnection;
      mysqli_close($dbConnection);
 }

 function selectEmployeesWithFirstNameStartingWith($searchString)
 {
    global $dbConnection;
    global $result;
    $sqlStatement = "SELECT * FROM employees WHERE first_name LIKE '%";
    $sqlStatement .= $searchString;
    $sqlStatement .= "%' LIMIT 50;";
    $result = mysqli_query($dbConnection,$sqlStatement);
    if(!$result)
    {
            die('Could not retrieve records from the employees Database: ' .
                    mysqli_error($dbConnection));
    }

 }

 function fetchEmployees()
 {
    global $dbConnection;
    global $result;
    if(!$result)
    {
            die('No records in the result set: ' .
                    mysqli_error($dbConnection));
    }
    return mysqli_fetch_assoc($result);
 }
?>
