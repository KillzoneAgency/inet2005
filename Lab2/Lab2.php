<?php

/**
 * Created by JetBrains PhpStorm.
 * User: inet2005
 * Date: 9/12/13
 * Time: 8:48 AM
 * To change this template use File | Settings | File Templates.
 */

  $myName = "Daniel Brown";
  $welcomeMessage = "Hello! My name is " . $myName . ". This is a test using concatenation in PHP.";

  /* Math variables */
  $calcA = (32 * 14) + 83;
  $calcB = (1024 / 128) - 7;
  $calcC = (769 % 6);

  $headingNumber;
  $hello = "Hello, World";
  $age = 19;

  $colours = array('Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet');
  $colourArrayLength = count($colours);

  function headings($string, $number)
  {

    if ($number == 1)
    {
        return "<h1>  $string$number </h1>";
    }else if ($number == 2)
    {
        return "<h2> $string$number </h2>";
    }else if ($number == 3)
    {
        return "<h3> $string$number </h3>";
    }else if ($number == 4)
    {
        return "<h4> $string$number </h4>";
    }else if ($number == 5)
    {
        return "<h5> $string$number </h5>";
    }else if ($number == 6)
    {
        return "<h6> $string$number </h6>";
    }else
    {
        return "<p>Not a valid heading level.</p>";
    } // End of If Statement

  } // End headings Function

  function byValue($string)
  {

    $string = $string . '...blah';

  } // End byValue Function

  function byReference(&$string)
  {
    $string = $string . '...blah';
  } // End byReference Function

  function ageWithGlobal()
  {

      global $age;

      return 'You are ' . $age . ' years old';

  } // End ageWithGlobal Function

?>

<!DOCTYPE html>

<html>

  <head>
    <title>Lab 2</title>
  </head>
  <body>

  <?php echo "<h1>Greetings from Lab2.</h1>" ?>

    <p>
        This is a paragraph that the browser can see in view source. The H1/H3 tabs can be seen by the browser;
        however, it doesn't see the PHP around it.
    </p>

  <?php echo "<h3>This is an H3 Tag</h3>" ?>

  <?php echo "<h2>" . $myName . "</h2>" ?>

  <?php echo "<p>" . $welcomeMessage . "</p>" ?>

  <?php echo "<p>" . "a. (32 * 14) + 83 = " . $calcA . "<br />"?>
  <?php echo "b. (1024 / 128) - 7 = " . $calcB . "<br />"?>
  <?php echo "c. the remainder of 769 divided by 6 = " . $calcC . "</p>" . "<br />"?>

  <?php

  echo "<p>";

  for ($i = 10; $i >= 0; $i--)
    {

        if ($i == 0)
        {
            echo "Blast Off!";
        }else
        {
          echo $i . "...";
        } // End of If Statement

    } // End For Loop

    echo "</p>";

  ?>

  <?php


  for ($i = 0; $i < 7; $i++)
  {

      $headingNumber = "Heading";
      echo headings($headingNumber, $i+1 );

  } // End For Loop

  ?>

  <?php

    echo "<p>";

    print($hello);
    echo '<br />';

    byValue($hello);
    print($hello);
    echo '<br />';

    byReference($hello);
    print($hello);
    echo '<br />';

    echo "</p>";
  ?>

  <?php

    echo '<h1>' . ageWithGlobal() . '</h1>';

  ?>

  <?php

    echo '<p>';

    for ($index = 0; $index < $colourArrayLength; $index++)
    {
            echo $colours[$index] . ' from For Loop <br />';
    } // End of For Loop

    echo '<br />';

    foreach ($colours as $index)
    {
            echo $index  . ' from For Each loop<br />';
    } // End of ForEach Loop

    echo '<br />';

    $counter = 0;

    while ($counter < $colourArrayLength)
    {
        echo $colours[$counter] . ' from While Loop <br />';
        $counter++;
    } // End While Loop

    echo '</p>';

  ?>

  </body>

</html>