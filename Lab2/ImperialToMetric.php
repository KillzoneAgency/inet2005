<!DOCTYPE html>

<html>

<body>

    <?php

    $heightFeet = $_POST['heightFeet'];
    $heightInches = $_POST['heightInches'];

    $fName = $_POST['fName'];
    $lName = $_POST['lName'];

    $metres = ($heightFeet * .3) + ($heightInches * .025);
    $heightMessage;
    $fNameMessage = 'Your first name is: ' . $fName . '<br />';
    $lNameMessage = 'Your last name is: ' . $lName . '<br />';


    if ($metres == 0)
    {
        $heightMessage = "Your height in metres is: No height was entered, or it was invalid";
    }else
    {
        $heightMessage = 'Your height in metres is: ' . $metres;
    } // End If Statement

    echo '<p>' . $fNameMessage . $lNameMessage . $heightMessage .'</p>';

    $fileTmpName = $_FILES['fileUpload']['tmp_name'];
    $fileOrigName = $_FILES['fileUpload']['name'];
    $fileSize = $_FILES['fileUpload']['size'];
    $fileUploadError = $_FILES['fileUpload']['error']; // 0 means success
    $result = move_uploaded_file($fileTmpName,"images/".$fileOrigName);

    echo '<p>';

    if ($_FILES["fileUpload"]["error"] > 0)
    {
        echo "Error: " . $_FILES["fileUpload"]["error"] . " Failed to upload. <br />";
    }
    else
    {
        echo "Tmp: " . $_FILES["fileUpload"]["tmp_name"] . "<br />";
        echo "Orig: " . $_FILES["fileUpload"]["name"] . "<br />";
        echo "Size: " . round(($_FILES["fileUpload"]["size"] / 1024), 0, PHP_ROUND_HALF_UP)  . " KB <br />";
        echo "Error: " . $_FILES["fileUpload"]["error"] . "<br /> <br />";

        echo "File Uploaded Successfully!";
    }

    echo '</p>';

    ?>

    <a href="Lab2Form.html">Go Back to form</a>

</body>

</html>
