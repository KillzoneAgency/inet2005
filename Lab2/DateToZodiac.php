<!DOCTYPE html>

<html>

<body>

<?php


$fName = $_GET['fName'];
$lName = $_GET['lName'];
$month = $_GET['birthMonth'];
$day = $_GET['birthDay'];
$zodiacSign = "";
$welcomeMessage = "";

if ($month == 1)
{

    if ($day >= 1 && $day <= 19)
    {
        $zodiacSign = 'Capricorn';
    }else if ($day >= 20 && $day <= 31)
    {
        $zodiacSign = 'Aquarius';
    } // End January Day If Statement

}else if ($month == 2)
{

    if ($day >= 1 && $day<= 1)
    {
        $zodiacSign = 'Aquarius';
    }else if ($day >= 19 && $day<= 29)
    {
        $zodiacSign = 'Pisces';
    } // End February Day If Statement

}else if ($month == 3)
{

    if ($day >= 1 && $day<= 20)
    {
        $zodiacSign = 'Pisces';
    }else if ($day >= 21 && $day<= 31)
    {
        $zodiacSign = 'Aries';
    } // End March Day If Statement

}else if ($month == 4)
{

    if ($day >= 1 && $day<= 19)
    {
        $zodiacSign = 'Capricorn';
    }else if ($day >= 20 && $day<= 30)
    {
        $zodiacSign = 'Taurus';
    } // End April Day If Statement

}else if ($month == 5)
{

    if ($day >= 1 && $day<= 20)
    {
        $zodiacSign = 'Taurus';
    }else if ($day >= 21 && $day<= 31)
    {
        $zodiacSign = 'Gemini';
    } // End May Day If Statement

}else if ($month == 6)
{

    if ($day >= 1 && $day<= 20)
    {
        $zodiacSign = 'Gemini';
    }else if ($day >= 21 && $day<= 30)
    {
        $zodiacSign = 'Cancer';
    } // End June Day If Statement

}else if ($month == 7)
{

    if ($day >= 1 && $day<= 22)
    {
        $zodiacSign = 'Cancer';
    }else if ($day >= 23 && $day<= 31)
    {
        $zodiacSign = 'Leo';
    } // End July Day If Statement

}else if ($month == 8)
{

    if ($day >= 1 && $day<= 22)
    {
        $zodiacSign = 'Leo';
    }else if ($day >= 23 && $day<= 31)
    {
        $zodiacSign = 'Virgo';
    } // End August Day If Statement

}else if ($month == 9)
{

    if ($day >= 1 && $day<= 22)
    {
        $zodiacSign = 'Virgo';
    }else if ($day >= 23 && $day<= 30)
    {
        $zodiacSign = 'Libra';
    } // End September Day If Statement

}else if ($month == 10)
{

    if ($day >= 1 && $day<= 22)
    {
        $zodiacSign = 'Libra';
    }else if ($day >= 23 && $day<= 31)
    {
        $zodiacSign = 'Scorpio';
    } // End October Day If Statement

}else if ($month == 11)
{

    if ($day >= 1 && $day<= 21)
    {
        $zodiacSign = 'Scorpio';
    }else if ($day >= 22 && $day<= 30)
    {
        $zodiacSign = 'Sagittarius';
    } // End November Day If Statement

}else if ($month == 12)
{

    if ($day >= 1 && $day<= 21)
    {
        $zodiacSign = 'Sagittarius';
    }else if ($day >= 22 && $day<= 31)
    {
        $zodiacSign = 'Capricorn';
    } // End December Day If Statement

}else
{
    $zodiacSign = 'You entered an invalid date.';
} // End Zodiac Sign If Statement

$welcomeMessage = "Hello, $fName $lName! <br /> Your zodiac sign is: $zodiacSign";
echo '<p>' .  $welcomeMessage . '</p>';

?>

<a href="Lab2Form2.html">Go Back to form</a>

</body>

</html>