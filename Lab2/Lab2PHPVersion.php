<!DOCTYPE html>
<html>
<head>
    <title>Other PHP Information</title>
</head>

<body>

<?php

echo '<h1>This page was rendered in PHP version ' . phpversion() . '</h1>';

echo '<h1>This page was rendered in Zend version ' . zend_version() . '</h1>';

echo '<h1>The value for default_mimetype from the php.ini file is  ' . ini_get('default_mimetype') . '</h1>';

?>

</body>

</html>