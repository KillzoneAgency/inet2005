<!DOCTYPE html>
<html>

<head>
    <title>Fahrenheit to Celsius Conversion</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>

    <!--<p>
        <?php
/*
        $temperature;

        for ($index = 0; $index <= 100; $index++)
        {
            $temperature = ($index - 32) * (5 / 9);
            echo $index . ' degrees fahrenheit equals ' . round($temperature, 0, PHP_ROUND_HALF_UP) . ' degrees Celsius<br />';
        } // End of For Loop

        */?>
    </p>-->

    <p>
        <a href="TemperatureConversions.html">Back to main page.</a>
    </p>

    <?php

      $temperature;


      echo '<table>';
      echo '<tr> <th>Fahrenheit</th> <th>Celsius</th> </tr>';

      for ($index = 0; $index <= 100; $index++)
      {
          $temperature = ($index - 32) * (5 / 9);
          echo '<tr> <td>' . $index . '</td> <td>' . round($temperature, 0, PHP_ROUND_HALF_UP) . '</td></tr>';
      } // End of For Loop

      echo '</table>';

    ?>

</body>

</html>