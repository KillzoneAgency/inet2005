<!DOCTYPE html>
<html>

<head>
    <title>Celsius to Fahrenheit Conversion</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>

    <p>
        <a href="TemperatureConversions.html">Back to main page.</a>
    </p>

    <?php

        $temperature;

        echo '<table>';
        echo '<tr> <th>Celsius</th> <th>Fahrenheit</th> </tr>';

        for ($index = 0; $index <= 100; $index++)
        {
            $temperature = ($index * (9 / 5)) + 32;
            echo '<tr> <td>' . $index . '</td> <td>' . round($temperature, 0, PHP_ROUND_HALF_UP) . '</td></tr>';
        } // End of For Loop

        echo '</table>';

    ?>

</body>

</html>