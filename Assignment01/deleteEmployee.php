<?php

require_once("isLoggedIn.php");
checkIfLoggedIn();

include_once('dbConnectAndClose.php');
include('utilityFunctions.php');

$db = connectToDB();

if (isset($_POST['delEmp']))
{

    $deleteStatement = "DELETE FROM employees WHERE emp_no ='";
    $deleteStatement .= $_POST['employeeID'];
    $deleteStatement .="';";

    $sqlQuery = mysqli_query($db, $deleteStatement);

}

?>

<!DOCTYPE html>

<html>

    <head>
        <title>Delete Employee</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Delete Employee</h1>

            <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                <label>Logged in as <?php echo $_SESSION['LoginUser']?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
            </form>

            <?php

            if(isset($_POST['delBtn']))
            {

            ?>

                <form id="delEmpForm" name="delEmpForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

                    <input type="hidden" name="employeeID" value="<?php echo $_POST['employeeID']; ?>" />
                    <label>Are you sure you wish to delete this employee?</label> <br />
                    <input type="submit" id="delEmp" name="delEmp" value="Yes" />
                    <input type="submit" value="No, Go Back to Employee Database." formaction="employee.php" />

                </form>

            <?php

            } // End If Statement if the updateBtn was set.

            ?>

            <br />

            <?php

            if (isset($_POST['delEmp']))
            {
                if(!$sqlQuery)
                {
                    die('Could not delete record in the database: ' . mysqli_error($db));
                }else
                {

                    $temp = mysqli_affected_rows($db);

            ?>

                <p>Successfully deleted <?php echo $temp; ?> record(s)</p> <br />

            <?php

                } // End If Statement to determine if the rows were deleted.

            } // End If Statement to determine if the POST was set.

            ?>

            <p>
                <a href="employee.php">Back to Employee Database</a>
            </p>

        </section>

    </body>

</html>

<?php

closeDBCon($db);

?>