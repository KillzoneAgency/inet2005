<?php

include_once('dbConnectAndClose.php');
include('utilityFunctions.php');

$db = connectToDB();

if (isset($_POST['registerBtn']))
{

    $insertStatement = "INSERT INTO webusers (username, password) VALUES ('";
    $insertStatement .= $_POST['username'];
    $insertStatement .= "', ";
    $insertStatement .= "SHA2('";
    $insertStatement .= $_POST['password'];
    $insertStatement .= "', 512)";
    $insertStatement .= ");";

    $sqlQuery = mysqli_query($db, $insertStatement);

}

?>

<!DOCTYPE html>
<HTML>


    <head>
        <title>Registration</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Register</h1>

            <form id="registrationForm" name="registrationForm" class="inputForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                <label for="userName">Username: </label>
                <input type="text" id="username" name="username" maxlength="16" value="">
                <label for="password">Password: </label>
                <input type="password" id="password" name="password" maxlength="16" value="">

                <br />

                <input type="submit" id="registerBtn" name="registerBtn" class="submitBtn" value="Register">

            </form>

            <br /><br />
            <p>

            <?php
            if(isset($_POST['registerBtn']))
            {

                if(!$sqlQuery)
                {

                ?>

                    Could not add record to the database: <?php echo mysqli_error($db)?>

                <?php

                }else
                {

            ?>

                Successfully registered <?php echo $_POST['username']; ?> <br />

            <?php

                } // End If Statement to determine if the query succeeded

            } // End If Statement to determine if the registerBtn was pressed.

            ?>

            <br /><br />
            Already have an account? <a href="mainLogin.html">Login here</a></p>

        </section>

    </body>

</HTML>