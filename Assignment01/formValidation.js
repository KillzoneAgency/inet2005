function validateForm()
{

    var isValid = false

    if (checkBirthDate() == true)
    {

        if (checkFirstName() == true)
        {

            if (checkLastName() == true)
            {

                if (checkGender() == true)
                {

                    if (checkHireDate() == true)
                    {
                        isValid = true;
                    }else
                    {
                        isValid = false;
                    } // End If Statement to checkHireDate

                }else
                {
                    isValid = false;
                } // End If Statement to checkGender

            }else
            {
                isValid = false;
            } // End If Statement to checkLastName

        }else
        {
            isValid = false;
        } // End If statement to checkFirstName

    }else
    {
        isValid = false;
    } // End If Statement to checkBirthDate

    return isValid;

} // End validateForm Function

function checkBirthDate()
{

    var isValid = false;
    var string = document.getElementById("birthDate");

    if (checkDateLength(string) == true)
    {

        if (checkDateFormat(string) == true)
        {
            isValid = true;
            document.getElementById("birthDateError").innerHTML = "<br />";
        }else
        {
            isValid = false;
            document.getElementById("birthDateError").innerHTML = "The date needs to be in a YYYY-MM-DD format.";
        } //  End If Statement to check the date format

    }else
    {
        isValid = false;
        document.getElementById("birthDateError").innerHTML = "The date entered needs to be 10 characters long.";
    } // End If Statement to check date length

    return isValid;

} // End checkBirthDate Function

function checkHireDate()
{

    var isValid = false;
    var string = document.getElementById("hireDate");

    if (checkDateLength(string) == true)
    {

        if (checkDateFormat(string) == true)
        {
            isValid = true;
            document.getElementById("hireDateError").innerHTML = "<br />";
        }else
        {
            isValid = false;
            document.getElementById("hireDateError").innerHTML = "The date needs to be in a YYYY-MM-DD format.";
        } //  End If Statement to check the date format

    }else
    {
        isValid = false;
        document.getElementById("hireDateError").innerHTML = "The date entered needs to be 10 characters long.";
    } // End If Statement to check the date length

    return isValid;

} // End checkHireDate

function checkDateLength(string)
{

    var isValid = false;

    if (string.value.length != 10)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkDateLength Function

function checkDateFormat(string)
{

    var isValid = false;
    var regExpression = new RegExp(/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/);

    if (regExpression.test(string.value) == true)
    {
        isValid = true;
    }else
    {
        isValid = false;
    } // End If Statement to check the format of the date against a regular expression

    return isValid;

} // End checkDateFormat Function

function checkFirstName()
{

    var isValid = false;
    var string = document.getElementById("firstName");

    if (checkNameLength(string) == true)
    {

        if (checkNameFormat(string) == true)
        {
            isValid = true;
            document.getElementById("firstNameError").innerHTML = "<br />";
        }else
        {
            isValid = false;
            document.getElementById("firstNameError").innerHTML = "Please ensure your name starts with a capital letter followed by lowercase characters.";
        } //  End If Statement to check the date format

    }else
    {
        isValid = false;
        document.getElementById("firstNameError").innerHTML = "Please ensure your name is at least two characters long.";
    } // End If Statement to check the date length

    return isValid;

} // End checkFirstName Function

function checkLastName()
{

    var isValid = false;
    var string = document.getElementById("lastName");

    if (checkNameLength(string) == true)
    {

        if (checkNameFormat(string) == true)
        {
            isValid = true;
            document.getElementById("lastNameError").innerHTML = "<br />";
        }else
        {
            isValid = false;
            document.getElementById("lastNameError").innerHTML = "Please ensure your name starts with a capital letter followed by lowercase characters.";
        } //  End If Statement to check the date format

    }else
    {
        isValid = false;
        document.getElementById("lastNameError").innerHTML = "Please ensure your name is at least two characters long.";
    } // End If Statement to check the date length

    return isValid;

} // End checkLastName Function

function checkNameLength(string)
{

    var isValid = false;

    if (string.value.length < 2)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameLength Function

function checkNameFormat(string)
{

    var isValid = false;
    var regExpression = new RegExp(/^[A-Z][a-z]+$/);

    if (regExpression.test(string.value) == true)
    {
        isValid = true;
    }else
    {
        isValid = false;
    } // End If Statement to check the name format against a regular expression

    return isValid;

} // End checkNameFormat Function

function checkGender()
{

    var isValid = false;
    var maleRadio = document.getElementById("genderSelect_0")
    var femaleRadio = document.getElementById("genderSelect_1")

    if (maleRadio.checked == false && femaleRadio.checked == false)
    {
        isValid = false;
        document.getElementById("genderError").innerHTML = "Please ensure you've selected a gender.";
    }else
    {
        isValid = true;
        document.getElementById("genderError").innerHTML = "<br />";
    } // End of If Statement

    return isValid;

} // End checkRadioButtons Function