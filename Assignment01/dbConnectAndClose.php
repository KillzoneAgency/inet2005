<?php

$dbCon;

function connectToDB()
{

    // Creating Connection
    $dbCon = mysqli_connect("localhost","root","","employees");

    // Checking Connection for Errors
    if (!$dbCon)
    {
        die('Could not connect to the Employees Database: ' . mysqli_error($dbCon));
    }

    return $dbCon;

} // End connectToDB Function

function closeDBCon($db)
{
    mysqli_close($db);
} // End closeDBCon Function

?>