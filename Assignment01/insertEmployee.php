<?php

require_once("isLoggedIn.php");
checkIfLoggedIn();

include_once('dbConnectAndClose.php');
include('utilityFunctions.php');

$db = connectToDB();


if (isset($_POST['addEmp']))
{

    $insertStatement = "INSERT INTO employees (emp_no, birth_date, first_name, last_name, gender, hire_date) VALUES ('";
    $insertStatement .= $_POST['empNum'];
    $insertStatement .= "','";
    $insertStatement .= $_POST['birthDate'];
    $insertStatement .= "','";
    $insertStatement .= $_POST['firstName'];
    $insertStatement .= "','";
    $insertStatement .= $_POST['lastName'];
    $insertStatement .= "','";
    $insertStatement .= $_POST['genderSelect'];
    $insertStatement .= "','";
    $insertStatement .= $_POST['hireDate'];
    $insertStatement .= "');";

    $sqlQuery = mysqli_query($db, $insertStatement);

}

?>

<!DOCTYPE html>

<html>

    <head>
        <title>Add New Employee</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <script src="formValidation.js" type="text/javascript"></script>
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Insert New Employee</h1>

            <div class="overflowContainer">

            <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                <label>Logged in as <?php echo $_SESSION['LoginUser']?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
            </form>

            <form id="addNewEmp" name="addNewEmp" class="inputForm" method="post" onSubmit="return validateForm();" action="<?php $_SERVER['PHP_SELF'] ?>">

                <label>Employee Number</label>
                <input type="text" id="empNum" name="empNum" value="<?php echo getNewEmpID($db); ?>" readonly>
                <span><br /></span>

                <label>Birth Date</label>
                <input type="text" id="birthDate" name="birthDate" value="" maxlength="10" onfocus="checkBirthDate();" onblur="checkBirthDate();">
                <span id="birthDateError" name="birthDateError"><br /></span>

                <label>First Name</label>
                <input type="text" id="firstName" name="firstName" value="" onfocus="checkFirstName();" onblur="checkFirstName();">
                <span id="firstNameError" name="firstNameError"><br /></span>

                <label>Last Name</label>
                <input type="text" id="lastName" name="lastName" value="" onfocus="checkLastName();" onblur="checkLastName();">
                <span id="lastNameError" name="lastNameError"><br /></span>

                <label>Gender:</label>
                <label id="lblMale" for="genderSelect_0">
                    <input type="radio" name="genderSelect" value="M" id="genderSelect_0" onfocus="checkGender();" onblur="checkGender();"/>Male
                </label>
                <label id="lblFemale" for="genderSelect_1">
                    <input type="radio" name="genderSelect" value="F" id="genderSelect_1" onfocus="checkGender();" onblur="checkGender();"/>Female
                </label>

                <span id="genderError" name="genderError"><br /></span>

                <label>Hire Date</label>
                <input type="text" id="hireDate" name="hireDate" value="" maxlength="10" onfocus="checkHireDate();" onblur="checkHireDate();">
                <span id="hireDateError" name="hireDateError"><br /></span>

                <br />

                <input type="submit" id="addEmp" name="addEmp" class="submitBtn" value="Submit">

            </form>

            <br />

            <?php

                if (isset($_POST['addEmp']))
                {
                    if(!$sqlQuery)
                    {
                        die('Could not add record to the database: ' . mysqli_error($db));
                    }else
                    {

                        $temp = mysqli_affected_rows($db);

            ?>

            <p>Successfully added <?php echo $temp; ?> record(s)</p> <br />

            <?php

                    } // End If Statement to determine if the rows were added.

                } // End If Statement to determine if the POST was set.

            ?>
            <p>
            <a href="employee.php">Back to Employee Database</a>
            </p>

            </div>

        </section>

    </body>

</html>

<?php

closeDBCon($db);

?>