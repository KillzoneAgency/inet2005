<?php

    function getNumPages($db, $rowsPerPage)
    {

        mysqli_query($db,"SELECT * FROM employees");
        $numRows = mysqli_affected_rows($db);
        $rowsPerPage = 25;

        $results = ceil($numRows / $rowsPerPage);

        return $results;

    } // End getNumPages Method

    function getNewEmpID($db)
    {

        $selectStatement = "SELECT emp_no FROM employees ORDER BY emp_no DESC LIMIT 1";
        $sqlQuery = mysqli_query($db, $selectStatement);

        if (mysqli_num_rows($sqlQuery) > 0)
        {
            $row = mysqli_fetch_assoc($sqlQuery);
            $result = ($row['emp_no'] + 1);

        }else
        {
            $result = "";
        } // End If Statement

        return $result;

    } // End getNewEmpID Method

    function pageForward(&$pageNum, &$lowerRange, &$selectStatement, $totalPages, $rowsPerPage, $searchQuery)
    {

        if (strlen($searchQuery) > 1)
        {
            $selectStatement = "SELECT * FROM employees WHERE ";
            $selectStatement .= "first_name LIKE '%$searchQuery%'";
            $selectStatement .= " OR last_name LIKE '%$searchQuery%'";
            $selectStatement .= " LIMIT ";
        }else
        {
            $selectStatement = "SELECT * FROM employees LIMIT ";
        } // End If Statement determining whether a search was entered or not.

        if ($pageNum < $totalPages)
        {

            $lowerRange += ($pageNum * $rowsPerPage);
            $pageNum += 1;

            $selectStatement .= ($lowerRange - 1);
            $selectStatement .= ",";
            $selectStatement .= $rowsPerPage;
            $selectStatement .=";";

        }else
        {

            $lowerRange += ($pageNum * $rowsPerPage);

            $selectStatement .= ($lowerRange - 1);
            $selectStatement .= ",";
            $selectStatement .= $rowsPerPage;
            $selectStatement .=";";

        } // End If Statement

    } // End pageForward Function

    function pageBackwards(&$pageNum, &$lowerRange, &$selectStatement, $rowsPerPage, $searchQuery)
    {

        if (strlen($searchQuery) > 1)
        {
            $selectStatement = "SELECT * FROM employees WHERE ";
            $selectStatement .= "first_name LIKE '%$searchQuery%'";
            $selectStatement .= " OR last_name LIKE '%$searchQuery%'";
            $selectStatement .= " LIMIT ";
        }else
        {
            $selectStatement = "SELECT * FROM employees LIMIT ";
        } // End If Statement determining whether a search was entered or not.

        if ($pageNum > 1)
        {

            $lowerRange += ($pageNum * $rowsPerPage);
            $lowerRange -= ($rowsPerPage * 2);
            $pageNum -= 1;

            $selectStatement .= ($lowerRange - 1);
            $selectStatement .= ",";
            $selectStatement .= $rowsPerPage;
            $selectStatement .=";";

        }else
        {

            $selectStatement .= ($lowerRange - 1);
            $selectStatement .= ",";
            $selectStatement .= $rowsPerPage;
            $selectStatement .=";";

        } // End If Statement

    } // End pageBackwards Function