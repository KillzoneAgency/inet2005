<?php

session_start();
ob_start();
include_once('dbConnectAndClose.php');

$db = connectToDB();

// Define $userName and $password
$userName = $_POST['username'];
$password = $_POST['password'];

// To protect MySQL injection (more detail about MySQL injection)
$userName = stripslashes($userName);
$password = stripslashes($password);
$userName = mysqli_real_escape_string($db, $userName);
$password = mysqli_real_escape_string($db, $password);

$hashedPass = hash("sha512", $password);

$selectStatement = "SELECT * FROM webusers WHERE username='$userName' and password='$hashedPass'";
$sqlQuery = mysqli_query($db,$selectStatement);


if(!$sqlQuery)
{
    die('Could find records in the database: ' . mysqli_error($db));
}else
{

    // Mysql_num_row is counting table row
    $count=mysqli_num_rows($sqlQuery);

    closeDBCon($db);

    // If result matched $userName and $password, table row must be 1 row
    if($count==1){

        $_SESSION['LoginUser'] = $userName;
        header("location:employee.php");
    }
    else {

        ?>

<html>

    <head>
        <title>Check Login</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Invalid Credentials</h1>

            <p>Wrong Username or Password. <br />
                <a href="mainLogin.html" >Try Again</a>
            </p>

        </section>

    </body>

</html>

<?php
    }
    ob_end_flush();

}
?>