<?php

require_once("isLoggedIn.php");
checkIfLoggedIn();

include_once('dbConnectAndClose.php');
include('utilityFunctions.php');

$db = connectToDB();


if (isset($_POST['modifyEmp']))
{

    $updateStatement = "UPDATE employees SET birth_date = '";
    $updateStatement .= $_POST['birthDate'];
    $updateStatement .= "', first_name = '";
    $updateStatement .= $_POST['firstName'];
    $updateStatement .= "', last_name = '";
    $updateStatement .= $_POST['lastName'];
    $updateStatement .= "', gender = '";
    $updateStatement .= $_POST['genderSelect'];
    $updateStatement .= "', hire_date = '";
    $updateStatement .= $_POST['hireDate'];
    $updateStatement .= "' WHERE emp_no = '";
    $updateStatement .= $_POST['empNum'];
    $updateStatement .= "';";

    $sqlQuery = mysqli_query($db, $updateStatement);

}

?>

    <!DOCTYPE html>

<html>

    <head>
        <title>Update Employee</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <script src="formValidation.js" type="text/javascript"></script>
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Update Current Employee</h1>

            <div class="overflowContainer">

                <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                    <label>Logged in as <?php echo $_SESSION['LoginUser']?></label>
                    <input type="submit" id="logOut" name="logOut" value="Logout">
                </form>

                <?php

                    if(isset($_POST['updateBtn']))
                    {

                        $selectStatement = "SELECT * FROM employees WHERE emp_no = '";
                        $selectStatement .= $_POST['employeeID'];
                        $selectStatement .= "';";

                        $selectQuery = mysqli_query($db, $selectStatement);

                        if(!$selectQuery)
                        {
                            die('Could not find record in the database: ' . mysqli_error($db));
                        }else
                        {
                            $results = mysqli_fetch_assoc($selectQuery);

                ?>

                <form id="updateEmp" name="updateEmp"method="post" class="inputForm" onSubmit="return validateForm();" action="<?php $_SERVER['PHP_SELF'] ?>">

                    <label>Employee Number</label>
                    <input type="text" id="empNum" name="empNum" value="<?php echo $results['emp_no'] ?>" readonly>
                    <span><br /></span>

                    <label>Birth Date</label>
                    <input type="text" id="birthDate" name="birthDate" value="<?php echo $results['birth_date'] ?>" maxlength="10" onfocus="checkBirthDate();" onblur="checkBirthDate();">
                    <span id="birthDateError" name="birthDateError"><br /></span>

                    <label>First Name</label>
                    <input type="text" id="firstName" name="firstName" value="<?php echo $results['first_name'] ?>" onfocus="checkFirstName();" onblur="checkFirstName();">
                    <span id="firstNameError" name="firstNameError"><br /></span>

                    <label>Last Name</label>
                    <input type="text" id="lastName" name="lastName" value="<?php echo $results['last_name'] ?>" onfocus="checkLastName();" onblur="checkLastName();">
                    <span id="lastNameError" name="lastNameError"><br /></span>

                    <?php

                        $isMale = true;

                        if ($results['gender'] == "M")
                        {
                            $isMale = true;
                        }else
                        {
                            $isMale = false;
                        }

                    ?>
                    <label>Gender:</label>
                    <label id="lblMale" for="genderSelect_0">
                        <input type="radio" name="genderSelect" value="M" id="genderSelect_0" onfocus="checkGender();" onblur="checkGender();" <?php if ($isMale == true) { ?> checked <?php } // End If ?> />Male
                    </label>
                    <label id="lblFemale" for="genderSelect_1">
                        <input type="radio" name="genderSelect" value="F" id="genderSelect_1" onfocus="checkGender();" onblur="checkGender();" <?php if ($isMale == false) { ?> checked <?php } // End If ?> />Female
                    </label>

                    <span id="genderError" name="genderError"><br /></span>

                    <label>Hire Date</label>
                    <input type="text" id="hireDate" name="hireDate" value="<?php echo $results['hire_date'] ?>" maxlength="10" onfocus="checkHireDate();" onblur="checkHireDate();">
                    <span id="hireDateError" name="hireDateError"><br /></span>

                    <br />

                    <input type="submit" id="modifyEmp" name="modifyEmp" class="submitBtn" value="Submit">

                </form>

                <?php

                        } // End If Statement to determine if the rows were added.

                    } // End If Statement if the updateBtn was set.

                ?>

                <br />

                <?php

                if (isset($_POST['modifyEmp']))
                {
                    if(!$sqlQuery)
                    {
                        die('Could not update record in the database: ' . mysqli_error($db));
                    }else
                    {

                        $temp = mysqli_affected_rows($db);

                        ?>

                        <p>Successfully updated <?php echo $temp; ?> record(s)</p> <br />

                    <?php

                    } // End If Statement to determine if the rows were added.

                } // End If Statement to determine if the POST was set.

                ?>

                <p>
                    <a href="employee.php">Back to Employee Database</a>
                </p>

            </div>

        </section>

    </body>

 </html>

<?php

closeDBCon($db);

?>