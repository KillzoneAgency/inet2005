<?php

require_once("isLoggedIn.php");
checkIfLoggedIn();

include_once('dbConnectAndClose.php');
include('utilityFunctions.php');

$db = connectToDB();

//Declaring Variables
$searchQuery = "";
$pageNum = 1;
$rowsPerPage = 25;
$lowerRange = 1;
//$totalPages = 12001;

    if (isset($_POST['selectNumRows']))
    {
        $rowsPerPage = $_POST['numRows'];
        $totalPages = getNumPages($db, $rowsPerPage);
    }else if (isset($_POST['rowsPerPage']))
    {
        $rowsPerPage = $_POST['rowsPerPage'];
    }else
    {
        $rowsPerPage = 25;
    } // End If Statement


    if (isset($_POST['searchBtn']))
    {
        $searchQuery = $_POST['searchBox'];
        $pageNum = 1;

        $selectStatement = "SELECT * FROM employees WHERE ";
        $selectStatement .= "first_name LIKE '%$searchQuery%'";
        $selectStatement .= " OR last_name LIKE '%$searchQuery%'";
        $selectStatement .= " LIMIT ";

    }else
    {

        if (isset($_POST['searchQuery']))
        {
            $searchQuery = $_POST['searchQuery'];
        }else
        {
            $searchQuery = "";
        } // End If Statement to check searchBox for null.

    } // End of If Statement to assign the search query if any.

    if (isset($_POST['prevBtn']))
    {

        $pageNum = $_POST['pageNumber'];

        if (isset($_POST['totalPages']))
        {
            $totalPages = $_POST['totalPages'];
        } // End If Statement

        pageBackwards($pageNum, $lowerRange, $selectStatement, $rowsPerPage, $searchQuery);

    }else if (isset($_POST['nextBtn']))
    {

        $pageNum = $_POST['pageNumber'];

        if (isset($_POST['totalPages']))
        {
            $totalPages = $_POST['totalPages'];
        } // End If Statement

        pageForward($pageNum, $lowerRange, $selectStatement, $totalPages, $rowsPerPage, $searchQuery);

    }else if(strlen($searchQuery) > 1)
    {

        if ($pageNum == 1)
        {
            $totalPages = getNumPages($db, $rowsPerPage);
        }else if (isset($_POST['totalPages']))
        {
            $totalPages = $_POST['totalPages'];
        } // End If Statement

        $selectStatement = "SELECT * FROM employees WHERE ";
        $selectStatement .= "first_name LIKE '%$searchQuery%'";
        $selectStatement .= " OR last_name LIKE '%$searchQuery%'";
        $selectStatement .= " LIMIT ";
        $selectStatement .= 0;
        $selectStatement .= ",";
        $selectStatement .= $rowsPerPage;
        $selectStatement .=";";
    }else
    {

        if ($pageNum == 1)
        {
            $totalPages = getNumPages($db, $rowsPerPage);
        }else if (isset($_POST['totalPages']))
        {
            $totalPages = $_POST['totalPages'];
        } // End If Statement

        $selectStatement = "SELECT * FROM employees LIMIT ";
        $selectStatement .= 0;
        $selectStatement .= ",";
        $selectStatement .= $rowsPerPage;
        $selectStatement .=";";

    } // End of If Statement

    $sqlQuery = mysqli_query($db,$selectStatement);


/*form action="test.php" method="post">
<input type="checkbox" name="check_list[]" value="value 1">
<input type="checkbox" name="check_list[]" value="value 2">
<input type="checkbox" name="check_list[]" value="value 3">
<input type="checkbox" name="check_list[]" value="value 4">
<input type="checkbox" name="check_list[]" value="value 5">
<input type="submit" />
</form>
<?php
if(!empty($_POST['check_list'])) {
    foreach($_POST['check_list'] as $check) {
        echo $check; //echoes the value set in the HTML form for each checked checkbox.
        //so, if I were to check 1, 3, and 5 it would echo value 1, value 3, value 5.
        //in your case, it would echo whatever $row['Report ID'] is equivalent to.
    }
}
?>*/

?>

<html>

    <head>
        <title>Employees Database</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Employee Database</h1>

            <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                <label>Logged in as <?php echo $_SESSION['LoginUser']?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
            </form>

            <form id="searchForm" name="searchForm" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

                <input type="text" id="searchBox" name="searchBox" value="<?php echo $searchQuery ?>">
                <input type="submit" id="searchBtn" name="searchBtn" value="Search">

            </form>

            <form id="changeListing" name="changeListing" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">


                <input type="hidden" id="pageNumber" name="pageNumber" value="<?php echo $pageNum ?>">
                <input type="hidden" id="searchQuery" name="searchQuery" value="<?php echo $searchQuery ?>">

                <label>Number of Rows to display: </label>
                <select id = "numRows" name = "numRows">
                    <option value="25" <?php if ($rowsPerPage == 25){ ?> selected <?php } // End If ?>>25</option>
                    <option value="50" <?php if ($rowsPerPage == 50){ ?> selected <?php } // End If ?>>50</option>
                    <option value="75" <?php if ($rowsPerPage == 75){ ?> selected <?php } // End If ?>>75</option>
                    <option value="100" <?php if ($rowsPerPage == 100){ ?> selected <?php } // End If ?>>100</option>
                    <option value="200" <?php if ($rowsPerPage == 200){ ?> selected <?php } // End If ?>>200</option>
                </select>

                <input type="submit" id="selectNumRows" name="selectNumRows" value="Submit">

            </form>

            <?php

            if (mysqli_num_rows($sqlQuery) > 0)
            {

            ?>

            <br />

            <div class="tableContainer">
                <table>

                    <thead>

                        <tr>
                            <!--<th> <input type="checkbox" id="checkAll" name="checkAll"> </th>-->
                            <th>Emp. Number</th>
                            <th>Birth Date</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Gender</th>
                            <th>Hire Date</th>
                            <th colspan="2"><a href="insertEmployee.php">Add New Employee</a></th>
                        </tr>

                    </thead>

                    <tfoot>

                        <tr>
                            <td colspan="8">

                                <form id="navigation" name="navigation" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">

                                    <input type="hidden" id="pageNumber" name="pageNumber" value="<?php echo $pageNum ?>">
                                    <input type="hidden" id="searchQuery" name="searchQuery" value="<?php echo $searchQuery ?>">
                                    <input type="hidden" id="rowsPerPage" name="rowsPerPage" value="<?php echo $rowsPerPage ?>">
                                    <input type="hidden" id="totalPages" name="totalPages" value="<?php echo $totalPages ?>">
                                    <input type="submit" id="prevBtn" name="prevBtn" value="Previous">
                                    <input type="submit" id="nextBtn" name="nextBtn" value="Next">

                                </form>

                            </td>
                        </tr>

                    </tfoot>

                    <?php

                    while ($row = mysqli_fetch_assoc($sqlQuery))
                    { // beginning While Loop to get data from table.
                        ?>

                        <tbody>
                            <tr>

                                <!--<td> <input type="checkbox" name="checkList[]" value="<?php /*echo $row['emp_no'] */?>"> </td>-->

                                <td> <?php echo $row['emp_no'] ?> </td>

                                <td> <?php echo $row['birth_date'] ?> </td>

                                <td> <?php echo $row['first_name'] ?> </td>

                                <td> <?php echo $row['last_name'] ?> </td>

                                <td> <?php echo $row['gender'] ?> </td>

                                <td> <?php echo $row['hire_date'] ?> </td>

                                <td>
                                    <form name="editForm" action="updateEmployee.php" method="post">
                                        <input type="hidden" name="employeeID" value="<?php echo $row['emp_no']; ?>" />
                                        <input type="submit" name="updateBtn" value="Update" />
                                    </form>
                                </td>

                                <td>
                                    <form name="delForm" action="deleteEmployee.php" method="post">
                                        <input type="hidden" name="employeeID" value="<?php echo $row['emp_no']; ?>" />
                                        <input type="submit" name="delBtn" value="Delete" />
                                    </form>
                                </td>

                            </tr>
                        </tbody>

                    <?php
                    }// Ending While Loop from earlier.
                    ?>

                </table>
            </div>

            <?php

            }else
            {

            ?>

                <p>Sorry, there were no results to display.</p>

            <?php

            } // Ending If Statement to determine if any rows were returned.

            ?>

        </section>

    </body>

</html>

<?php

closeDBCon($db);

?>