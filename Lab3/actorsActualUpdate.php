<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();
?>

    <!DOCTYPE html>
    <html>
    <head>
        <title>Actor Update</title>
    </head>

    <body>

    <section>

        <?php

        if (isset($_POST['updateBtn']))
        {

            $updateStatement = "UPDATE actor SET first_name ='";
            $updateStatement .= $_POST['firstName'];
            $updateStatement .= "', last_name = '";
            $updateStatement .= $_POST['lastName'];
            $updateStatement .= "' WHERE actor_id ='";
            $updateStatement .= $_POST['actorID'];
            $updateStatement .= "';";
            $updateResult = mysqli_query($db,$updateStatement);

            $temp = mysqli_affected_rows($db);

            if(!$updateResult)
            {
                die('Could not update record from the Sakila Database: ' . mysqli_error($db));
            }else
            {

                ?>

                <p>
                    Successfully updated <?php echo $temp; ?> record(s) <br />

                    <a href="actorsCRUD.php">Back to Actor List</a>
                </p>

            <?php

            }

        } // End of If Statement

        ?>

    </section>
    </body>
    </html>

<?php

closeDBCon($db);

?>