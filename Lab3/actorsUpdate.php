<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();
?>

    <!DOCTYPE html>
    <html>
    <head>
        <title>Actor Update</title>
    </head>

    <body>

    <section>

        <p>
            <a href="actorsCRUD.php">Back to form</a>
        </p>

        <?php

        if (isset($_POST['modBtn']))
        {

            $selectStatement = "SELECT actor_id, first_name, last_name FROM actor WHERE actor_id = '";
            $selectStatement .= $_POST['idToMod'];
            $selectStatement .= "';";
            $selectResult = mysqli_query($db,$selectStatement);


            if(!$selectResult)
            {
                die('Could not find record in the Sakila Database: ' . mysqli_error($db));
            }else
            {

                $names = mysqli_fetch_assoc($selectResult)
        ?>

                <form id="updateActor" name="updateActor" method="post" action="actorsActualUpdate.php">

                    <input type="hidden" name="actorID" id="actorID" value = "<?PHP echo $names['actor_id']; ?>"/>

                    <p>
                        <label>First Name: <input type="text" name="firstName" id="firstName" value = "<?PHP echo $names['first_name']; ?>"/> </label>
                    </p>
                    <p>
                        <label>Last Name:<input type="text" name="lastName" id="lastName" value = "<?PHP echo $names['last_name']; ?>"/></label>
                    </p>
                    <p>
                        <input type="submit" name="updateBtn" id="updateBtn" value="Update" />
                    </p>
                </form>

        <?php
            } // End If Statement to determine if the select failed or not.

        }// End of If Statement to determine if the Update Button was pressed or not.

        ?>

    </section>
    </body>
    </html>

<?php

closeDBCon($db);

?>