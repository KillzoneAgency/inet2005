<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Films Search</title>
</head>
<body>


    <?php

        if(isset($_POST['submitBtn']) == true)
        {
            $searchQuery = $_POST['searchBox'];
            $sqlQuery = mysqli_query($db,"SELECT * FROM film WHERE description LIKE '%$searchQuery%'");

            if (mysqli_num_rows($sqlQuery) > 0)
            {

    ?>

    <table border=1>

        <thead>
            <th>Films</th>
            <th>Descriptions</th>
        </thead>

        <?php
            while ($filmRow = mysqli_fetch_assoc($sqlQuery))
            {
        ?>

        <tr>
            <td>
                <?php echo $filmRow['title'] ?>
            </td>

            <td>
                <?php echo $filmRow['description'] ?>
            </td>
        </tr>

        <?php
            }//End While Loop to display all relevant rows from the table.
        ?>

    </table>

    <br /> <br />

    <?php

            }else
            {

    ?>

    <p>Sorry, your search query returned no results.</p>

     <?php
            } // Ending Inner If Statement to determine if anny rows were returned
        }else
        {

     ?>

    <p>Please enter a search term and press submit</p>

    <?php
        }//End outer If Statement to determine if the submit button was pressed
    ?>

    <form id="searchForm" name="searchForm" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
        <label id="searchLbl" name="searchLbl" for="searchBox" >Search: </label>
        <input type="text" id="searchBox" name="searchBox"> <br />
        <input type="submit" id="submitBtn" name="submitBtn" value="Submit">
    </form>

</body>
</html>

<?php

    closeDBCon($db);

?>