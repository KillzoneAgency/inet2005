<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Actor Deleted</title>
</head>

    <body>

        <section>

            <?php

                if (isset($_POST['delBtn']))
                {

                    $deleteStatement = "DELETE FROM actor WHERE actor_id = '";
                    $deleteStatement .= $_POST['idToDel'];
                    $deleteStatement .= "';";
                    $deleteResult = mysqli_query($db,$deleteStatement);

                    $temp = mysqli_affected_rows($db);

                    if(!$deleteResult)
                    {
                        die('Could not delete record from the Sakila Database: ' . mysqli_error($db));
                    }else
                    {

            ?>

            <p>
                Successfully deleted <?php echo $temp; ?> record(s) <br />

                <a href="actorsCRUD.php">Back to Actor List</a>
            </p>

            <?php

                    }

                } // End of If Statement

            ?>

        </section>
    </body>
</html>

<?php

closeDBCon($db);

?>