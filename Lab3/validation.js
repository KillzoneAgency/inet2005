function addMarkers(labelID)
{

    var activeElement = document.activeElement;
    var activeLbl = document.getElementById(labelID);

    activeElement.style.background = "LightYellow";
    activeElement.style.fontStyle="italic";
    activeLbl.style.textDecoration="underline";

} // End addMarkers Method

function removeMarkers(textID, labelID)
{

    var oldText = document.getElementById(textID);
    var oldLbl = document.getElementById(labelID);

    oldText.style.background = "White";
    oldText.style.fontStyle = "normal";
    oldLbl.style.textDecoration="none";

} // End removeMarkers Method

function validateForm()
{

    var valid = 0;
    var length = 0;
    var firstName = document.getElementById("firstName");
    var lastName = document.getElementById("lastName");
    var addressOne = document.getElementById("addressOne");
    var addressTwo = document.getElementById("addressTwo");
    var email = document.getElementById("email");
    var termsAndCon = document.getElementById("termsAndCon");
    var checkBoxError = document.getElementById("checkBoxError");

    length = firstName.value.length;

    if (length < 1)
    {
        firstName.style.borderColor="Red";
    }else
    {
        firstName.style.borderColor="#7F9DB9";
        valid += 1;
    } // End If Statement

    length = lastName.value.length;

    if (length < 1)
    {
        lastName.style.borderColor="Red";
    }else
    {
        lastName.style.borderColor="#7F9DB9";
        valid += 1;
    } // End If Statement

    length = addressOne.value.length;

    if (length < 1)
    {
        addressOne.style.borderColor="Red";
    }else
    {
        addressOne.style.borderColor="#7F9DB9";
        valid += 1;
    } // End If Statement

    length = addressTwo.value.length;

    if (length < 1)
    {
        addressTwo.style.borderColor="Red";
    }else
    {
        addressTwo.style.borderColor="#7F9DB9";
        valid += 1;
    } // End If Statement

    length = email.value.length;

    if (length < 1)
    {
        email.style.borderColor="Red";
    }else
    {
        email.style.borderColor="#7F9DB9";
        valid += 1;
    } // End If Statement

    if (termsAndCon.checked == false)
    {
        checkBoxError.style.visibility = "visible";
    }else
    {
        checkBoxError.style.visibility = "hidden";
        valid += 1;
    } // End If Statement

    if (valid != 6)
    {
        return false;
    }else
    {
        return true;
    }

} // validateForm Method