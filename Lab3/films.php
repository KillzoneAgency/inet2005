<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();

//Declaring Variables
$sqlQuery = mysqli_query($db,"SELECT * FROM film LIMIT 0,10");

?>

<html>

<head>
    <title>Films</title>
</head>

<body>

<table border=1>

    <tr>
        <th>Film</th>
        <th>Description</th>
    </tr>

    <?php

        while ($filmRow = mysqli_fetch_assoc($sqlQuery))
        { // beginning While Loop to get data from table.
    ?>

    <tr>
        <td>
            <?php echo $filmRow['title'] ?>
        </td>

        <td>
            <?php echo $filmRow['description'] ?>
        </td>
    </tr>

    <?php
        }// Ending While Loop from earlier.
    ?>

</table>

</body>

</html>

<?php

closeDBCon($db);

?>