<?php

include_once('dbConnectAndClose.php');

$db = connectToDB();
?>

    <!DOCTYPE html>
    <html>
    <head>
        <title>Actors CRUD</title>
    </head>
    <body>

    <?php

    if(isset($_POST['submitBtn']) == true)
    {

        $insertStatement = "INSERT INTO actor (first_name, last_name) VALUES ('";
        $insertStatement .= $_POST['firstName'];
        $insertStatement .= "','";
        $insertStatement .= $_POST['lastName'];
        $insertStatement .= "');";
        $insertResult = mysqli_query($db,$insertStatement);

        if (!$insertResult)
        {
            die('Could not insert record into the Sakila Database: ' . mysqli_error($db));
        }else
        {

    ?>

            <p>Actor inserted successfully! <br />
            </p>

        <?php
        } // Ending If Statement to determine if the actor was inserted or not.

    }//End outer If Statement to determine if the submit button was pressed
    ?>

    <p>
    <a href="actorsCRUD.html">Back to form</a>
    </p>

    <?php

    $selectQuery = mysqli_query($db,"SELECT * FROM actor ORDER BY actor_id DESC LIMIT 10");

    if (mysqli_num_rows($selectQuery) > 0)
    {

    ?>

    <table border=1>

        <thead>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Last Update</th>
        </thead>

        <?php
        while ($actorRow = mysqli_fetch_assoc($selectQuery))
        {
            ?>

            <tr>
                <td>
                    <?php echo $actorRow['actor_id'] ?>
                </td>

                <td>
                    <?php echo $actorRow['first_name'] ?>
                </td>

                <td>
                    <?php echo $actorRow['last_name'] ?>
                </td>

                <td>
                    <?php echo $actorRow['last_update'] ?>
                </td>
            </tr>

        <?php
        }//End While Loop to display all relevant rows from the table.
        ?>

    </table>

    <br /> <br />

    <?php

    }else
    {

        ?>

        <p>Sorry, there were no actors to display.</p>

    <?php
    } // Ending Inner If Statement to determine if anny rows were returned
    ?>

    <section>

        <form id="deleteActor" name="deleteActor" method="post" action="actorsDelete.php">
            <p>
                <label>ID to Delete: <input type="text" name="idToDel" id="idToDel" /> </label>
            </p>
            <p>
                <input type="submit" name="delBtn" id="delBtn" value="Delete" />
            </p>
        </form>


        <form id="updateActor" name="updateActor" method="post" action="actorsUpdate.php">
            <p>
                <label>ID to Update: <input type="text" name="idToMod" id="idToMod" /> </label>
            </p>
            <p>
                <input type="submit" name="modBtn" id="modBtn" value="Update" />
            </p>
        </form>

    </section>

    </body>
    </html>

<?php

closeDBCon($db);

?>