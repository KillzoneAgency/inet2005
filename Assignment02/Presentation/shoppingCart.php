<?php

require_once("isLoggedIn.php");
checkIfLoggedIn();

$message = NULL;

if (isset($_POST['addTrack']))
{
    require_once("../Business/Tracks.php");

    $trackObj = Tracks::selectTrack($_POST['trackID']);
    $tempArray = array($trackObj->getTrackID(), $trackObj->getTrackName(), $trackObj->getUnitPrice());

    if (empty($_SESSION['shoppingCart']))
    {

        $_SESSION['shoppingCart'] = array(implode("|", $tempArray));

    }else
    {

        if (in_array(implode("|", $tempArray), $_SESSION['shoppingCart']))
        {
            $message = "Item already in your cart.";
        }else
        {
            $_SESSION['shoppingCart'][] = implode("|", $tempArray);
        } // End If Statement to determine if the ID is already present.

    } // End If Statement to add/create session variable for shopping cart.

} // End If Statement to determine whether the page was hit through POST or not.

if (isset($_POST['clearCart']))
{
    unset($_SESSION['shoppingCart']);
} // End If Statement to determine whether the page was hit through a different POST or not.

?>



<!DOCTYPE html>
<html>

    <head>
        <title>Shopping Cart</title>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
    </head>

    <body>

        <section class="mainContent">

        <h1 class="contentHeading">Current Items in Cart:</h1>

            <?php

            if(!empty($_SESSION['LoginUser']))
            {



                ?>

                <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                    <label>Logged in as <?php echo $_SESSION['LoginUser']; ?></label>
                    <input type="submit" id="logOut" name="logOut" value="Logout">
                </form>

            <?php

            }else
            {



                ?>

                <form method="post" id="logOutForm" name="logOutForm" action="">
                    <label>Hello Guest,
                        <input type="submit" formaction="login.html" value="Login">
                        or
                        <input type="submit" formaction="registration.php" value="Register">
                    </label>
                </form>

                <!--                <p>Hello Guest, <a href="login.html">Login</a> or <a href="registration.php">Register</a> to create your shopping cart.</p>-->

            <?php

            } // End If Statement to display logout form or login link.

            ?>


            <?php

            if ($message != NULL)
            {

                ?>

                <p><strong><?php echo $message;?></strong></p>

            <?php

            }

            ?>

        <?php

        if (empty($_SESSION['shoppingCart']) == false)
        {

            ?>

            <div class="tableContainer" id="shoppingCart">

                <table>

                    <thead>
                        <tr>
                            <th>Track ID</th>
                            <th>Name</th>
                            <th>Unit Price</th>
                        </tr>
                    </thead>

                    <tfoot>

                    <tr>
                        <td colspan="3">


                        </td>
                    </tr>

                    </tfoot>

                    <tbody>

                    <?php

                    $subTotal = 0.0;
                    $taxes = 0.15;

                    foreach ($_SESSION['shoppingCart'] as $key => $cartItem)
                    {

                        $tempArray = explode("|", $cartItem);

                        $subTotal += $tempArray[2];

                        ?>

                        <tr>
                            <td><?php echo $tempArray[0]; ?></td>
                            <td><?php echo $tempArray[1]; ?></td>
                            <td><?php echo $tempArray[2]; ?></td>
                        </tr>

                    <?php

                    } // End For Each Loop

                    ?>

                        <tr>
                            <td>Subtotal</td>
                            <td colspan=2><?php echo $subTotal; ?></td>
                        </tr>

                        <tr>
                            <td>HST (15%)</td>
                            <td colspan=2><?php echo $taxes; ?></td>
                        </tr>

                        <tr>
                            <td>Total</td>
                            <td colspan=2>
                                <?php
                                    echo round(($subTotal * $taxes) + $subTotal, 2, PHP_ROUND_HALF_UP);
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <form id="cartControls" name="cartControls" method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">

                <input type="submit" name="clearCart" id="clearCart" value="Clear Cart" />

            </form>

            <br /> <br />

        <?php

        }else
        {

            ?>

            <p>You currently have no items in your shopping cart.</p>

        <?php
        } // Ending Inner If Statement to determine if the session variable is set.
        ?>

            <p><a href="audioTracks.php">Continue Shopping</a></p>

        </section>

    </body>

</html>