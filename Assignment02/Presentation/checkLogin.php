<?php

session_start();
ob_start();

if (isset($_POST['loginBtn']))
{

    if (!empty($_POST['username']) && !empty($_POST['password']))
    {

        require_once("../Business/User.php");

        // Define $userName and $password
        $userName = $_POST['username'];
        $password = $_POST['password'];

        // Creating a instance of user in order to get the salt.
        $userObj = User::selectUserFromName($userName);

        if ($userObj->getUserName() != NULL)
        {

            // Getting Hashed Version of the password
            $password = User::hashPassword($password, $userObj->getSalt());

            // Checking if credentials match.
            $tempUser = User::selectUser($userName, $password);

            if ($userObj->getPassword() == $tempUser->getPassword())
            {
                $_SESSION['LoginUser'] = $userName;
                header("location:audioTracks.php");
            }else
            {
                // Otherwise Display the page.
            } // If Statement to check if the credentials matched or not.

        }else
        {
            // Otherwise Display the page.
        } // End If Statement to see if the user was found.

    }else
    {
        // Otherwise Display the page.
    } // End If Statement to check is username/password is set.

}else
{
    // Otherwise Display the page.
} // End If Statement to check post.
?>

<html>

    <head>
        <title>Check Login</title>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Invalid Credentials</h1>

            <p>Wrong Username and/or Password. <br />
                <a href="login.html" >Try Again</a>
            </p>

        </section>

    </body>

</html>

<?php
ob_end_flush();
?>