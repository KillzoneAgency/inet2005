<?php

require_once("../Business/User.php");

$searchValue = "";

if(!empty($_GET['searchExpr']))
{
    $searchValue = $_GET['searchExpr'];
    $result = "";

    $userObj = User::selectUserFromName($searchValue);

    if ($userObj->getUserName() != NULL)
    {
        $result = "That username is unavailable";
    }else
    {
        $result = "That username is available!";
    } // End If Statement

    echo $result;

} // End If Statement
?>
