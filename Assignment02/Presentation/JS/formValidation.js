function validateForm()
{

    var isValid = false;

    if (checkUserName() == true)
    {

            isValid = checkPassword();

    }else
    {
        isValid = false;
    } // End If Statement to checkBirthDate

    return isValid;

} // End validateForm Function

function checkUserName()
{

    var isValid = false;
    var string = document.getElementById("username");

    if (checkNameLength(string) == true)
    {
        isValid = true;
        document.getElementById("usernameError").innerHTML = "<br />";
    }else
    {
        isValid = false;
        document.getElementById("usernameError").innerHTML = "Please ensure your name is at least two characters long.";
    } // End If Statement

    return isValid;

} // End checkUserName Method

function checkPassword()
{

    var isValid = false;
    var passOne = document.getElementById("password");
    var passTwo = document.getElementById("confirmPassword");

    if (passOne.value != passTwo.value)
    {
        isValid = false;
        document.getElementById("passwordError").innerHTML = "Please ensure both passwords match.";
    }else if (checkPasswordLength(passOne) == false)
    {
        isValid = false;
        document.getElementById("passwordError").innerHTML = "Please your password is 8 or more characters long.";
    }else
    {
        isValid = true;
        document.getElementById("passwordError").innerHTML = "<br />";
    }// End If Statement

    return isValid;

} // End checkPassword Method

function checkNameLength(string)
{

    var isValid = false;

    if (string.value.length < 2)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameLength Function

function checkPasswordLength(string)
{

    var isValid = false;

    if (string.value.length < 8)
    {
        isValid = false;
    }else
    {
        isValid = true;
    } // End If Statement

    return isValid;

} // End checkNameLength Function