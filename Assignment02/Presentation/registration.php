<?php

require('process.php');

$result = NULL;

$username = "";

if (isset($_POST['registerBtn']))
{

    require_once('recaptchalib.php');
    $privateKey = "6LeBAOoSAAAAAAm-o7fya2sS2HEhMDEZe8QPRk_Z";
    $resp = recaptcha_check_answer ($privateKey,
    $_SERVER["REMOTE_ADDR"],
    $_POST["recaptcha_challenge_field"],
    $_POST["recaptcha_response_field"]);

    if (!$resp->is_valid)
    {
        $result = "The reCAPTCHA wasn't entered correctly. Please try again. (reCAPTCHA said: " . $resp->error . ")";
    } else
    {


        if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['password']))
        {
            // For the purposes of sticky forms.
            $username = $_POST['username'];

            require_once("../Business/User.php");

            // Define $userName and $password
            $userName = $_POST['username'];
            $password = $_POST['password'];

            // Creating a unique salt.
            $salt = User::genSalt();

            // Getting Hashed Version of the password
            $password = User::hashPassword($password, $salt);

            // Register User
            $temp = User::insertUser($userName, $password, $salt);

            if ($temp == true)
            {
                header("location:login.html");
            }else
            {
                $result = "There was an error registering your account, please try again.";
            } // If Statement to check if the credentials matched or not.

        }else
        {
            $result = "Please ensure both the username and password fields are completed.";
        }// End If Statement to ensure post for username/password wasn't empty.

    } // End If to check if reCAPTCHA was valid or not.

} // End If Statement to see if register was pressed.

?>

<!DOCTYPE html>
<HTML>


    <head>
        <title>Registration</title>
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <script type="text/javascript" src="JS/jquery.js"></script>
        <script type="text/javascript" src="JS/myScript.js"></script>
        <script type="text/javascript" src="JS/ajax.js"></script>
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Register</h1>

                <div class="overflowContainer">

                    <?php

                        if ($result != NULL)
                        {



                    ?>

                    <p><?php echo $result; ?></p>

                    <?php

                        } // End If Statement to display result or not.

                    ?>

                    <form id="registrationForm" name="registrationForm" class="inputForm" method="post"  onSubmit="return validateForm();" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <label for="username">Username: </label>
                        <input type="text" id="username" name="username" maxlength="16" value="<?php echo $username; ?>" onfocus="checkUserName();" onblur="checkUserName();">
                        <span id="usernameError" name="usernameError"><br /></span>

                        <label for="password">Password: </label>
                        <input type="password" id="password" name="password" maxlength="16" value="" onkeyup="checkPassword();" onfocus="checkPassword();" onblur="checkPassword();">
                        <span><br /></span>

                        <label for="confirmPassword">Confirm Password: </label>
                        <input type="password" id="confirmPassword" name="confirmPassword" maxlength="16" value="" onkeyup="checkPassword();" onfocus="checkPassword();" onblur="checkPassword();">
                        <span id="passwordError" name="passwordError"><br /></span>

                        <br />

                        <div id = "reCaptcha">

                            <script type="text/javascript"
                                    src="http://www.google.com/recaptcha/api/challenge?k=6LeBAOoSAAAAAJzxD4fuSmpycTL0_-UUsQ1o8fAA">
                            </script>
                            <noscript>
                                <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LeBAOoSAAAAAJzxD4fuSmpycTL0_-UUsQ1o8fAA"
                                        height="300" width="500" frameborder="0"></iframe><br>
                                <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                                </textarea>
                                <input type="hidden" name="recaptcha_response_field"
                                       value="manual_challenge">
                            </noscript>

                        </div>

                        <input type="submit" id="registerBtn" name="registerBtn" class="submitBtn" value="Register">

                    </form>

                    <br /><br />
                    <p>Already have an account? <a href="login.html">Login here</a>
                    <br /> <a href="audioTracks.php">Back to track listings.</a></p>

            </div>

        </section>

    </body>

</HTML>