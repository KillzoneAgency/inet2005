<?php
/*form action="test.php" method="post">
<input type="checkbox" name="check_list[]" value="value 1">
<input type="checkbox" name="check_list[]" value="value 2">
<input type="checkbox" name="check_list[]" value="value 3">
<input type="checkbox" name="check_list[]" value="value 4">
<input type="checkbox" name="check_list[]" value="value 5">
<input type="submit" />
</form>
<?php
if(!empty($_POST['check_list'])) {
    foreach($_POST['check_list'] as $check) {
        echo $check; //echoes the value set in the HTML form for each checked checkbox.
        //so, if I were to check 1, 3, and 5 it would echo value 1, value 3, value 5.
        //in your case, it would echo whatever $row['Report ID'] is equivalent to.
    }
}
?>*/

session_start();

?>

<!DOCTYPE html>
<html>

    <head>
        <title>Chinook Database</title>
        <link href="CSS/jquery.dataTables.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <script src="JS/jquery.js" type="text/javascript"></script>
        <script src="JS/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="JS/myCode.js" type="text/javascript"></script>
    </head>

    <body>

        <section class="mainContent">

            <h1 class="contentHeading">Chinook Database</h1>

            <!--<form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                <label>Logged in as <?php /*echo $_SESSION['LoginUser']*/?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
            </form>-->

        <?php

            if(!empty($_SESSION['LoginUser']))
            {



        ?>

            <form method="post" id="logOutForm" name="logOutForm" action="logout.php">
                <label>Logged in as <?php echo $_SESSION['LoginUser']; ?></label>
                <input type="submit" id="logOut" name="logOut" value="Logout">
            </form>

        <?php

            }else
            {



        ?>

                <form method="post" id="logOutForm" name="logOutForm" action="">
                    <label>Hello Guest,
                    <input type="submit" formaction="login.html" value="Login">
                        or
                    <input type="submit" formaction="registration.php" value="Register">
                    </label>
                </form>

        <?php

            } // End If Statement to display logout form or login link.

        ?>

        <p><a href="shoppingCart.php">View Shopping Cart</a></p>



            <?php

            require_once("../Business/Tracks.php");

            $arrayOfTracks = Tracks::selectTracks();

            if ($arrayOfTracks != null)
            {

                ?>

                <table id="grid">

                    <thead>
                        <tr>

                            <th>Track ID</th>
                            <th>Name</th>
                            <th>Album</th>
                            <th>Artist</th>
                            <th>Media Type</th>
                            <th>Genre</th>
                            <th>Composer(s)</th>
                            <th>Milliseconds</th>
                            <th>Bytes</th>
                            <th>Unit Price</th>
                            <th>Add to Cart</th>

                        </tr>
                    </thead>

                    <?php

                    foreach($arrayOfTracks as $track):

                        ?>

                        <tr>
                            <td><?php echo htmlentities($track->getTrackID()); ?></td>
                            <td><?php echo htmlentities($track->getTrackName()); ?></td>
                            <td><?php echo htmlentities($track->getAlbum()->getAlbumName()); ?></td>
                            <td><?php echo htmlentities($track->getAlbum()->getArtist()->getArtistName()); ?></td>
                            <td><?php echo htmlentities($track->getMediaType()->getMediaType()); ?></td>
                            <td><?php echo htmlentities($track->getGenre()->getGenreName()); ?></td>
                            <td><?php echo htmlentities($track->getComposer()); ?></td>
                            <td><?php echo htmlentities($track->getMilliseconds()); ?></td>
                            <td><?php echo htmlentities($track->getBytes()); ?></td>
                            <td><?php echo htmlentities($track->getUnitPrice()); ?></td>
                            <td>

                                <form id="addTrackToCart" name="addTrackToCart" method="POST" action="shoppingCart.php">

                                    <input type="submit" name="addTrack" id="addTrack" value="Add" />
                                    <input type="hidden" name="trackID" id="trackID"value="<?php echo $track->getTrackID(); ?>">

                                </form>

                            </td>
                        </tr>

                    <?php

                    endforeach;

                    ?>

                </table>

                <br /> <br />

            <?php

            }else
            {

                ?>

                <p>Sorry, there were no tracks to display.</p>

            <?php
            } // Ending Inner If Statement to determine if anny rows were returned
            ?>

        </section>

    </body>

</html>