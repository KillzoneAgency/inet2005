<?php

class Artist
{
    private $artistName;

    public function __construct($artistName)
    {
        $this->artistName = $artistName;
    } // End Constructor Method

    public function getArtistName()
    {
        return $this->artistName;
    } // End getArtistName Method

} // End Artist Class

?>