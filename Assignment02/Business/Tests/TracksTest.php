<?php

require("Tracks.php");

class TracksTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSelectTracks()
    {

        $tracks = Tracks::selectTracks();
        $track = $tracks[0];

        $this->assertEquals(1, $track->getTrackID());
        $this->assertEquals("For Those About To Rock (We Salute You) MySQL", $track->getTrackName());
        $this->assertEquals("For Those About To Rock We Salute You", $track->getAlbum()->getAlbumName());
        $this->assertEquals("AC/DC", $track->getAlbum()->getArtist()->getArtistName());
        $this->assertEquals("MPEG audio file", $track->getMediaType()->getMediaType());
        $this->assertEquals("Rock", $track->getGenre()->getGenreName());
        $this->assertEquals("Angus Young, Malcolm Young, Brian Johnson", $track->getComposer());
        $this->assertEquals("343719", $track->getMilliseconds());
        $this->assertEquals("11170334", $track->getBytes());
        $this->assertEquals("0.99", $track->getUnitPrice());

    } // End testSelectTracks Method

    public function testSelectTrack()
    {

        $track = Tracks::selectTrack(1);

        $this->assertEquals(1, $track->getTrackID());
        $this->assertEquals("For Those About To Rock (We Salute You) MySQL", $track->getTrackName());
        $this->assertEquals("For Those About To Rock We Salute You", $track->getAlbum()->getAlbumName());
        $this->assertEquals("AC/DC", $track->getAlbum()->getArtist()->getArtistName());
        $this->assertEquals("MPEG audio file", $track->getMediaType()->getMediaType());
        $this->assertEquals("Rock", $track->getGenre()->getGenreName());
        $this->assertEquals("Angus Young, Malcolm Young, Brian Johnson", $track->getComposer());
        $this->assertEquals("343719", $track->getMilliseconds());
        $this->assertEquals("11170334", $track->getBytes());
        $this->assertEquals("0.99", $track->getUnitPrice());

    } // End testSelectTrack Method

}
