<?php

require("User.php");

class UserTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    } // End setUp Method

    protected function tearDown()
    {
        parent::tearDown();
    } // End tearDown Method

    public function testSetGetUserInfo()
    {

        $newUser = new User("Dan", "Password", "Salt");

        $this->assertEquals("Dan", $newUser->getUserName());
        $this->assertEquals("Password", $newUser->getPassword());
        $this->assertEquals("Salt", $newUser->getSalt());

        $newUser->setUserName("DanTest");
        $newUser->setPassword("PasswordTest");
        $newUser->setSalt("SaltTest");

        $this->assertEquals("DanTest", $newUser->getUserName());
        $this->assertEquals("PasswordTest", $newUser->getPassword());
        $this->assertEquals("SaltTest", $newUser->getSalt());

    } // End testSetGetUserInfo Method

    public function testSelectUser()
    {

        $userObj = User::selectUserFromName("Daniel");
        $this->assertEquals("Daniel", $userObj->getUserName());

        $userObj = User::selectUser("Daniel", "Password");
        $this->assertEquals(NULL, $userObj->getUserName());
        $this->assertEquals(NULL, $userObj->getPassword());
        $this->assertEquals(NULL, $userObj->getSalt());

    } // End testSelectUser Method

    public function testPassHash()
    {
        $userObj = User::selectUserFromName("Daniel");
        $hashPass = User::hashPassword("Password-01", $userObj->getSalt());
        $this->assertEquals($hashPass, $userObj->getPassword());

    } // End testPassHash Method

    public function testCreateUserAndRelatedInfo()
    {

        $salt = User::genSalt();

        $userName = $salt . "";
        $password = "";

        $password = User::hashPassword($password, $salt);

        $temp = User::insertUser($userName, $password, $salt);

        $this->assertEquals(True, $temp);

        $salt = User::genSalt();

    } // End testCreateUserAndRelatedInfo Method

} // End UserTest Class
