<?php

class MediaType
{
    private $mediaType;

    public function __construct($mediaType)
    {
        $this->mediaType = $mediaType;
    } // End Constructor Method

    public function getMediaType()
    {
        return $this->mediaType;
    } // End getMediaType Method

} // End MediaType Class

?>