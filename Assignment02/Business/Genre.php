<?php

class Genre
{
    private $genreName;

    public function __construct($genreName)
    {
        $this->genreName = $genreName;
    } // End Constructor Method

    public function getGenreName()
    {
        return $this->genreName;
    } // End getGenreName Method

} // End Genre Class

?>