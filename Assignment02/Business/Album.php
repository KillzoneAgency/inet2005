<?php

require_once 'Artist.php';

class Album
{
    private $albumName;
    private $artist;

    public function __construct($albumName, $artistName)
    {
        $this->albumName = $albumName;
        $this->artist = new Artist($artistName);
    } // End Constructor Method

    public function getAlbumName()
    {
        return $this->albumName;
    } // End getAlbumName Method

    public function getArtist()
    {
        return $this->artist;
    } // End getArtist Method

} // End Album Class

?>