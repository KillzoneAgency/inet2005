<?php

require_once '../Data/DataAccess.php';
require_once 'Album.php';
require_once 'Genre.php';
require_once 'MediaType.php';

class Tracks
{
    private $trackID;
    private $trackName;
    private $composer;
    private $milliseconds;
    private $bytes;
    private $unitPrice;

    private $album;
    private $mediaType;
    private $genre;

    public function __construct($trackID, $trackName)
    {
        $this->trackID = $trackID;
        $this->trackName = $trackName;
    } // End Constructor Method

    public function getMediaType()
    {
        return $this->mediaType;
    } // End getMediaType Method

    public function getAlbum()
    {
        return $this->album;
    } // End getAlbum Method

    public function getGenre()
    {
        return $this->genre;
    } // End getGenre Method

    public function getBytes()
    {
        return $this->bytes;
    } // End getBytes Method

    public function getComposer()
    {
        return $this->composer;
    } // End getComposer Method

    public function getMilliseconds()
    {
        return $this->milliseconds;
    } // End getMilliseconds Method

    public function getTrackID()
    {
        return $this->trackID;
    } // End getTrackID Method

    public function getTrackName()
    {
        return $this->trackName;
    } // End getTrackName Method

    public function getUnitPrice()
    {
        return $this->unitPrice;
    } // End getUnitPrice Method

    public static function selectTrack($id)
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectTrack($id);

        $row = $dataAccess->retrieveRecords();

        $currentTrack = new self($dataAccess->retrieveTrackID($row), $dataAccess->retrieveTrackName($row));
        $currentTrack->composer = $dataAccess->retrieveComposer($row);
        $currentTrack->milliseconds = $dataAccess->retrieveMilliseconds($row);
        $currentTrack->bytes = $dataAccess->retrieveBytes($row);
        $currentTrack->unitPrice = $dataAccess->retrieveUnitPrice($row);

        $albumObj = new Album($dataAccess->retrieveAlbumTitle($row), $dataAccess->retrieveArtistName($row));
        $mediaTypeObj = new MediaType($dataAccess->retrieveMediaType($row));
        $genreObj = new Genre($dataAccess->retrieveGenre($row));

        $currentTrack->album = $albumObj;
        $currentTrack->mediaType = $mediaTypeObj;
        $currentTrack->genre = $genreObj;

        $dataAccess->closeDB();

        return $currentTrack;

    } // End selectTracks Function

    public static function selectTracks()
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->selectTracks();

        $arrayOfTrackObjects = array();

        while($row = $dataAccess->retrieveRecords())
        {
            $currentTrack = new self($dataAccess->retrieveTrackID($row), $dataAccess->retrieveTrackName($row));
            $currentTrack->composer = $dataAccess->retrieveComposer($row);
            $currentTrack->milliseconds = $dataAccess->retrieveMilliseconds($row);
            $currentTrack->bytes = $dataAccess->retrieveBytes($row);
            $currentTrack->unitPrice = $dataAccess->retrieveUnitPrice($row);

            $albumObj = new Album($dataAccess->retrieveAlbumTitle($row), $dataAccess->retrieveArtistName($row));
            $mediaTypeObj = new MediaType($dataAccess->retrieveMediaType($row));
            $genreObj = new Genre($dataAccess->retrieveGenre($row));

            $currentTrack->album = $albumObj;
            $currentTrack->mediaType = $mediaTypeObj;
            $currentTrack->genre = $genreObj;

            $arrayOfTrackObjects[] = $currentTrack;
        } // End While Loop

        $dataAccess->closeDB();

        return $arrayOfTrackObjects;

    } // End selectTracks Function

/*    public static function search($query)
    {
        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        $dataAccess->searchTracks($query);

        while($row = $dataAccess->retrieveActors())
        {
            $currentActor = new self($dataAccess->retrieveActorFirstName($row), $dataAccess->retrieveActorLastName($row));
            $currentActor->actorID = $dataAccess->retrieveActorID($row);
            $currentActor->lastUpdate = $dataAccess->retrieveActorLastUpdate($row);
            $arrayOfActorObjects[] = $currentActor;
        }

        $dataAccess->closeDB();

        return $arrayOfActorObjects;
    } // End search Function*/

} // End Tracks Class

?>