<?php

require_once '../Data/DataAccess.php';

class User
{

    private $salt;
    private $userName;
    private $password;

    public function __construct($userName, $password, $salt)
    {
        $this->userName = $userName;
        $this->password = $password;
        $this->salt = $salt;
    } // End Constructor Method

    public static function insertUser($username, $password, $salt)
    {

        $temp = false;

        $dataAccess = DataAccess::getInstance();
        $dataAccess->connectToDB();

        $rowsAffected = $dataAccess->insertUser($username, $password, $salt);

        $dataAccess->closeDB();

        if ($rowsAffected > 0)
        {
            $temp = true;
        }

        return $temp;

    } // End insertUser Method

    public static function selectUserFromName($userName)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        // Selecting a User Based on their name (Unique)
        $dataAccess->selectUserFromName($userName);

        // Getting the results.
        $row = $dataAccess->retrieveRecords();

        // Creating a new user.
        $currentUser = new self($dataAccess->retrieveUserName($row), $dataAccess->retrievePassword($row), $dataAccess->retrieveSalt($row));

        $dataAccess->closeDB();

        return $currentUser;

    } // End selectUserFromName Method

    public static function selectUser($userName, $password)
    {

        $dataAccess = dataAccess::getInstance();
        $dataAccess->connectToDB();

        // Selecting a User
        $dataAccess->selectUser($userName, $password);

        // Getting the results.
        $row = $dataAccess->retrieveRecords();

        // Creating a new user.
        $currentUser = new self($dataAccess->retrieveUserName($row), $dataAccess->retrievePassword($row), $dataAccess->retrieveSalt($row));

        $dataAccess->closeDB();

        return $currentUser;

    } // End selectUser Method

    public static function hashPassword($password, $salt)
    {

        $hashInstructions = '$6$rounds=3000$' . $salt . '$';
        $hashedPasswordTemp = crypt($password, $hashInstructions);
        $hashedPassword = str_replace ($hashInstructions, "", $hashedPasswordTemp);

        return $hashedPassword;

    } // End hashPassword Method

    public static function genSalt()
    {
        return time();
    } // End genSalt Method

    public function setPassword($password)
    {
        $this->password = $password;
    } // End setPassword Method

    public function getPassword()
    {
        return $this->password;
    } // End getPassword Method

    public function setSalt($salt)
    {
        $this->salt = $salt;
    } // End setSalt Method

    public function getSalt()
    {
        return $this->salt;
    } // End getSalt Method

    public function setUserName($userName)
    {
        $this->userName = $userName;
    } // End setUserName Method

    public function getUserName()
    {
        return $this->userName;
    } // End getUserName Method

} // End User Class

?>