<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'DataAccess.php';
class DataAccessSQLServer extends DataAccess
{
    private $dbCon;
    private $result;

    // aDataAccess methods
    function connectToDB()
    {
           $this->dbCon;

           /* Specify the server and connection string attributes. */
           $serverName = "(local)\SQLEXPRESS";
           $uid = "Customer";
           $pwd = "Password-01";
           $connectionInfo = array( "UID"=>$uid,
                                    "PWD"=>$pwd,
                                    "Database"=>"Chinook");

           /* Connect using SQL Server Authentication. */
           $this->dbCon = sqlsrv_connect($serverName, $connectionInfo);

           if($this->dbCon === false )
           {
                die("Could not insert records into the database: " . print_r( sqlsrv_errors(), true));
           }


    }

    function closeDB()
    {
         sqlsrv_close($this->dbCon);
    }

    public function insertUser($userName, $hashedPassword, $salt)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);

        $insertStatement = "INSERT INTO webusers (UserName, Password, Salt) VALUES('";
        $insertStatement .= $userName;
        $insertStatement .= "', '";
        $insertStatement .= $hashedPassword;
        $insertStatement .= "', '";
        $insertStatement .= $salt;
        $insertStatement .= "');";

        $this->result = sqlsrv_query($this->dbCon, $insertStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_rows_affected($this->result);

    } // End insertUser Method

    public function selectUserFromName($userName)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);

        //$selectStatement = $this->dbCon->prepare("SELECT * FROM webusers WHERE UserName = ?;");
        //$selectStatement = $this->dbCon->obj_prepare_statement("SELECT * FROM webusers WHERE UserName = ?;");

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName';";

        //bindParam('?', $userName);

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectUserFromName Method

    public function selectUser($userName, $hashedPassword)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName' and Password ='$hashedPassword'";
        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectUser Method

    public function selectTrack($id)
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM track t ";
        $selectStatement .= "JOIN album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN genre g ON (t.GenreId = g.GenreId) ";
        $selectStatement .= "WHERE t.TrackId = ";
        $selectStatement .= $id;
        $selectStatement .= ";";

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectTrack Method

    public function selectTracks()
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM dbo.track t ";
        $selectStatement .= "JOIN dbo.album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN dbo.artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN dbo.mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN dbo.genre g ON (t.GenreId = g.GenreId);";
        //$selectStatement .= "ORDER BY t.TrackId;";

        $this->result = sqlsrv_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Chinook Database: " . print_r( sqlsrv_errors(), true));
        }

    } // End selectTracks Method

    public function retrieveRecords()
    {
        if(!$this->result)
        {
            die("No records to display: " . print_r( sqlsrv_errors(), true));
        }

        return sqlsrv_fetch_array($this->result, SQLSRV_FETCH_ASSOC);
    } // End retrieveRecords Method

    public function retrieveUserName($row)
    {
        return $row['UserName'];
    } // End retrieveUserName Method

    public function retrievePassword($row)
    {
        return $row['Password'];
    } // End retrievePassword Method

    public function retrieveSalt($row)
    {
        return $row['Salt'];
    } // End retrieveSalt Method

    public function retrieveTrackID($row)
    {
        return $row['TrackId'];
    } // End retrieveTrackID Method

    public function retrieveTrackName($row)
    {
        return $row['Name'];
    } // End retrieveTrackName Method

    public function retrieveComposer($row)
    {
        return $row['composer'];
    } // End retrieveTrackComposer Method

    public function retrieveMilliseconds($row)
    {
        return $row['Milliseconds'];
    } // End retrieveTrackMilliseconds Method

    public function retrieveBytes($row)
    {
        return $row['Bytes'];
    } // End retrieveTrackBytes Method

    public function retrieveUnitPrice($row)
    {
        return $row['UnitPrice'];
    } // End retrieveTrackUnitPrice Method

    public function retrieveAlbumTitle($row)
    {
        return $row['AlbumTitle'];
    } // End retrieveAlbumTitle Method

    public function retrieveArtistName($row)
    {
        return $row['ArtistName'];
    } // End retrieveArtistName Method

    public function retrieveMediaType($row)
    {
        return $row['MediaType'];
    } // End retrieveMediaType Method

    public function retrieveGenre($row)
    {
        return $row['Genre'];
    } // End retrieveGenre Method
}

?>
