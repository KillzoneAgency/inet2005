<?php

require_once '../Data/DataAccessMySQL.php';
require_once '../Data/DataAccessSQLServer.php';
require_once '../Data/DataAccessSQLite.php';

abstract class DataAccess
{
    private static $dataAccess;

    public static function getInstance()
    {
        // If there's no instance of DataAccess then create one.
        if(self::$dataAccess == null)
        {
            //self::$dataAccess = new DataAccessMySQL();
            self::$dataAccess = new DataAccessSQLServer();
            //self::$dataAccess = new DataAccessSQLite();
        }
        return self::$dataAccess;
    }

    public abstract function connectToDB();

    public abstract function closeDB();

    public abstract function insertUser($userName, $hashedPassword, $salt);

    public abstract function selectUserFromName($userName);

    public abstract function selectUser($userName, $hashedPassword);

    public abstract function selectTrack($id);

    public abstract function selectTracks();

    public abstract function retrieveRecords();

    public abstract function retrieveUserName($row);

    public abstract function retrievePassword($row);

    public abstract function retrieveSalt($row);

    public abstract function retrieveTrackID($row);

    public abstract function retrieveTrackName($row);

    public abstract function retrieveComposer($row);

    public abstract function retrieveMilliseconds($row);

    public abstract function retrieveBytes($row);

    public abstract function retrieveUnitPrice($row);

    public abstract function retrieveAlbumTitle($row);

    public abstract function retrieveArtistName($row);

    public abstract function retrieveMediaType($row);

    public abstract function retrieveGenre($row);

} // End DataAccess Class
?>