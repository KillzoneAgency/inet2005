<?php

require_once 'DataAccess.php';
class DataAccessSQLite extends DataAccess
{

    private $dbCon;
    private $result;

    public function connectToDB()
    {

        // Creating Connection
        $this->dbCon = @new SQLite3('../Data/SQLiteDatabase/Chinook_Sqlite_AutoIncrementPKs.sqlite');

        // Checking Connection for Errors
        if (!$this->dbCon)
        {
            die('Could not connect to the Chinook Database: ' . $this->dbCon->lastErrorMsg());
        }

    } //  End connectToDB Method

    public function closeDB()
    {
        $this->dbCon->close();
    } // End closeDB Method

    public function insertUser($userName, $hashedPassword, $salt)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);

        $userName = $this->dbCon->escapeString($userName);
        $hashedPassword = $this->dbCon->escapeString($hashedPassword);

        $insertStatement = "INSERT INTO webusers (UserName, Password, Salt) VALUES('";
        $insertStatement .= $userName;
        $insertStatement .= "', '";
        $insertStatement .= $hashedPassword;
        $insertStatement .= "', '";
        $insertStatement .= $salt;
        $insertStatement .= "');";

        $this->result = $this->dbCon->query($insertStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->lastErrorMsg());
        }

        return $this->dbCon->changes();

    } // End insertUser Method

    public function selectUserFromName($userName)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $userName = $this->dbCon->escapeString($userName);

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName';";

        $this->result = $this->dbCon->query($selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->lastErrorMsg());
        }

    } // End selectUserFromName Method

    public function selectUser($userName, $hashedPassword)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);
        $userName = $this->dbCon->escapeString($userName);
        $hashedPassword = $this->dbCon->escapeString($hashedPassword);

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName' and Password ='$hashedPassword'";

        $this->result = $this->dbCon->query($selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->lastErrorMsg());
        }

    } // End selectUser Method

    public function selectTrack($id)
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM track t ";
        $selectStatement .= "JOIN album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN genre g ON (t.GenreId = g.GenreId) ";
        $selectStatement .= "WHERE t.TrackId = ";
        $selectStatement .= $id;
        $selectStatement .= ";";

        $this->result = $this->dbCon->query($selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->lastErrorMsg());
        }

    } // End selectTrack Method

    public function selectTracks()
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM track t ";
        $selectStatement .= "JOIN album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN genre g ON (t.GenreId = g.GenreId);";

        $this->result = $this->dbCon->query($selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Chinook Database: " . $this->dbCon->lastErrorMsg());
        }

    } // End selectTracks Method

    public function retrieveRecords()
    {
        if(!$this->result)
        {
            die("No records to display: " . $this->dbCon->error);
        }

        return $this->result->fetchArray(SQLITE3_ASSOC);
    } // End retrieveTracks Method

    public function retrieveUserName($row)
    {
        return $row['UserName'];
    } // End retrieveUserName Method

    public function retrievePassword($row)
    {
        return $row['Password'];
    } // End retrievePassword Method

    public function retrieveSalt($row)
    {
        return $row['Salt'];
    } // End retrieveSalt Method

    public function retrieveTrackID($row)
    {
        return $row['TrackId'];
    } // End retrieveTrackID Method

    public function retrieveTrackName($row)
    {
        return $row['Name'];
    } // End retrieveTrackName Method

    public function retrieveComposer($row)
    {
        return $row['Composer'];
    } // End retrieveTrackComposer Method

    public function retrieveMilliseconds($row)
    {
        return $row['Milliseconds'];
    } // End retrieveTrackMilliseconds Method

    public function retrieveBytes($row)
    {
        return $row['Bytes'];
    } // End retrieveTrackBytes Method

    public function retrieveUnitPrice($row)
    {
        return $row['UnitPrice'];
    } // End retrieveTrackUnitPrice Method

    public function retrieveAlbumTitle($row)
    {
        return $row['AlbumTitle'];
    } // End retrieveAlbumTitle Method

    public function retrieveArtistName($row)
    {
        return $row['ArtistName'];
    } // End retrieveArtistName Method

    public function retrieveMediaType($row)
    {
        return $row['MediaType'];
    } // End retrieveMediaType Method

    public function retrieveGenre($row)
    {
        return $row['Genre'];
    } // End retrieveGenre Method

} // End DataAccessSQLite Class

?>