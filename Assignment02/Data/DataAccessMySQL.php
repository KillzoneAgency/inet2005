<?php

require_once 'DataAccess.php';
class DataAccessMySQL extends DataAccess
{

    private $dbCon;
    private $result;

    // aDataAccess methods
    public function connectToDB()
    {

        // Creating Connection
        $this->dbCon = mysqli_connect("localhost","Customer","Password-01","chinook");

        // Checking Connection for Errors
        if (!$this->dbCon)
        {
            die('Could not connect to the Chinook Database: ' . $this->dbCon->connect_errno);
        }

    } //  End connectToDB Method

    public function closeDB()
    {
        $this->dbCon->close();
    } // End closeDB Method

    public function insertUser($userName, $hashedPassword, $salt)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);
        $userName = mysqli_real_escape_string($this->dbCon, $userName);
        $hashedPassword = mysqli_real_escape_string($this->dbCon, $hashedPassword);

        $insertStatement = "INSERT INTO webusers (UserName, Password, Salt) VALUES('";
        $insertStatement .= $userName;
        $insertStatement .= "', '";
        $insertStatement .= $hashedPassword;
        $insertStatement .= "', '";
        $insertStatement .= $salt;
        $insertStatement .= "');";

        $this->result = mysqli_query($this->dbCon, $insertStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

        return $this->dbCon->affected_rows;

    } // End insertUser Method

    public function selectUserFromName($userName)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $userName = mysqli_real_escape_string($this->dbCon, $userName);

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName';";
        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectUserFromName Method

    public function selectUser($userName, $hashedPassword)
    {

        // To protect MySQL injection (more detail about MySQL injection)
        $userName = stripslashes($userName);
        $hashedPassword = stripslashes($hashedPassword);
        $userName = mysqli_real_escape_string($this->dbCon, $userName);
        $hashedPassword = mysqli_real_escape_string($this->dbCon, $hashedPassword);

        $selectStatement = "SELECT * FROM webusers WHERE UserName ='$userName' and Password ='$hashedPassword'";
        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectUser Method

    public function selectTrack($id)
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM track t ";
        $selectStatement .= "JOIN album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN genre g ON (t.GenreId = g.GenreId) ";
        $selectStatement .= "WHERE t.TrackId = ";
        $selectStatement .= $id;
        $selectStatement .= ";";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve record from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectTrack Method

    public function selectTracks()
    {

        $selectStatement = "SELECT t.TrackId, t.Name, t.composer, t.Milliseconds, t.Bytes, t.UnitPrice, ";
        $selectStatement .= "alb.Title AS 'AlbumTitle', art.Name AS 'ArtistName', m.Name AS 'MediaType', g.Name AS 'Genre' ";
        $selectStatement .= "FROM track t ";
        $selectStatement .= "JOIN album alb ON (t.AlbumId = alb.AlbumId) ";
        $selectStatement .= "JOIN artist art ON (alb.ArtistId = art.ArtistId) ";
        $selectStatement .= "JOIN mediatype m ON (t.MediaTypeId = m.MediaTypeId) ";
        $selectStatement .= "JOIN genre g ON (t.GenreId = g.GenreId);";
        //$selectStatement .= "ORDER BY t.TrackId;";

        $this->result = mysqli_query($this->dbCon,$selectStatement);

        if(!$this->result)
        {
            die("Could not retrieve records from the Chinook Database: " . $this->dbCon->error);
        }

    } // End selectTracks Method

    public function retrieveRecords()
    {
        if(!$this->result)
        {
            die("No records to display: " . $this->dbCon->error);
        }

        return mysqli_fetch_assoc($this->result);
    } // End retrieveTracks Method

    public function retrieveUserName($row)
    {
        return $row['UserName'];
    } // End retrieveUserName Method

    public function retrievePassword($row)
    {
        return $row['Password'];
    } // End retrievePassword Method

    public function retrieveSalt($row)
    {
        return $row['Salt'];
    } // End retrieveSalt Method

    public function retrieveTrackID($row)
    {
        return $row['TrackId'];
    } // End retrieveTrackID Method

    public function retrieveTrackName($row)
    {
        return $row['Name'];
    } // End retrieveTrackName Method

    public function retrieveComposer($row)
    {
        return $row['composer'];
    } // End retrieveTrackComposer Method

    public function retrieveMilliseconds($row)
    {
        return $row['Milliseconds'];
    } // End retrieveTrackMilliseconds Method

    public function retrieveBytes($row)
    {
        return $row['Bytes'];
    } // End retrieveTrackBytes Method

    public function retrieveUnitPrice($row)
    {
        return $row['UnitPrice'];
    } // End retrieveTrackUnitPrice Method

    public function retrieveAlbumTitle($row)
    {
        return $row['AlbumTitle'];
    } // End retrieveAlbumTitle Method

    public function retrieveArtistName($row)
    {
        return $row['ArtistName'];
    } // End retrieveArtistName Method

    public function retrieveMediaType($row)
    {
        return $row['MediaType'];
    } // End retrieveMediaType Method

    public function retrieveGenre($row)
    {
        return $row['Genre'];
    } // End retrieveGenre Method

} // End DataAccessMySQL Class

?>